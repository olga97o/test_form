module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/arui-feather/button/button.css":
/*!*****************************************************!*\
  !*** ./node_modules/arui-feather/button/button.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"button": "_2n6yvwGgFxGXcEP9aVlKZN",
	"button__addon": "_2zxzhpWLk0lcyisZGisnNf",
	"button__content": "_1GV3w65yvSxy_mTIDFJ6Ay",
	"button__text": "_3I31Ai7rx2PhEBRXzaWHwB",
	"button__icon": "UTAcUK7s-TW1QAfMSNJfN",
	"button_width_available": "_29fcoMTezxHMaXP4oRwCgx",
	"button_type_link": "_1lLtDscrVncRsoOLR0fZKc",
	"button_focused": "ZcqQJc0zVEP_N9We5QjQ7",
	"button_disabled": "qstFCt5sm4BU-48CVBUow",
	"button_view_action": "_2tJqPH4vhenSxzJcB1B-2F",
	"button_view_extra": "_1MY9bvSztfN102BAIQO7C",
	"button_view_rounded": "_39HakR5lTDe4eIUgPZwv3L",
	"button_size_s": "t2y1JR48lMsJCH0kb7M2c",
	"button_size_m": "_16q-crj-c2YYmZ0Suoa1Sx",
	"button_size_l": "_2wcrTd_vuv4i1F-AyMeAg1",
	"button_size_xl": "_2UhH02Q3bW6nJpaK7vBiJH"
};

/***/ }),

/***/ "./node_modules/arui-feather/button/button.js":
/*!****************************************************!*\
  !*** ./node_modules/arui-feather/button/button.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _dec, _dec2, _class, _desc, _value, _class2, _class3, _temp2; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                   * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                   * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _keyboardCode = __webpack_require__(/*! ../lib/keyboard-code */ "./node_modules/arui-feather/lib/keyboard-code.js");

var _keyboardCode2 = _interopRequireDefault(_keyboardCode);

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

/**
 * Компонент кнопки (да, она нажимается!).
 */
var Button = (_dec = (0, _cn2.default)('button'), _dec2 = (0, _performance2.default)(), _dec(_class = _dec2(_class = (_class2 = (_temp2 = _class3 = function (_React$Component) {
    _inherits(Button, _React$Component);

    function Button() {
        var _temp, _this, _ret;

        _classCallCheck(this, Button);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.state = {
            focused: false,
            hovered: false,
            pressed: false
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    /**
     * @type {HTMLButtonElement|HTMLSpanElement}
     */


    Button.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
        if (nextProps.disabled) {
            this.setState({
                hovered: false,
                focused: false
            });
        }
    };

    Button.prototype.render = function render(cn) {
        var _this2 = this;

        var isButton = this.props.tag !== 'span';

        var buttonProps = {
            ref: function ref(control) {
                _this2.control = control;
            },
            role: 'button',
            id: this.props.id,
            name: this.props.name,
            type: this.props.type,
            title: this.props.title,
            tabIndex: this.props.disabled ? '-1' : this.props.tabIndex,
            disabled: this.props.disabled,
            formNoValidate: isButton ? this.props.formNoValidate : null,
            className: cn({
                disabled: this.props.disabled,
                pseudo: this.props.pseudo,
                view: this.props.view,
                size: this.props.size,
                width: this.props.width,
                focused: this.props.focused !== undefined ? this.props.focused : this.state.focused,
                hovered: this.state.hovered,
                pressed: this.state.pressed,
                togglable: this.props.togglable,
                checked: this.props.checked
            }),
            onClick: this.handleClick,
            onFocus: this.handleFocus,
            onBlur: this.handleBlur,
            onMouseEnter: this.handleMouseEnter,
            onMouseLeave: this.handleMouseLeave,
            onMouseDown: this.handleMouseDown,
            onMouseUp: this.handleMouseUp,
            onMouseOut: this.handleMouseOut,
            onKeyDown: this.handleKeyDown,
            onKeyUp: this.handleKeyUp,
            'data-test-id': this.props['data-test-id']
        };

        var buttonContent = [this.props.leftAddons && _react2.default.createElement(
            'span',
            { key: 'left-addons', className: cn('addon') },
            this.props.leftAddons
        ), (this.props.children || this.props.text || this.props.icon) && _react2.default.createElement(
            'span',
            { key: 'content', className: cn('content') },
            this.props.icon && _react2.default.createElement(
                'span',
                { key: 'icon', className: cn('icon') },
                this.props.icon
            ),
            (this.props.children || this.props.text) && _react2.default.createElement(
                'span',
                { key: 'text', className: cn('text') },
                this.props.children || this.props.text
            )
        ), this.props.rightAddons && _react2.default.createElement(
            'span',
            { key: 'right-addons', className: cn('addon') },
            this.props.rightAddons
        )];

        return isButton ? _react2.default.createElement(
            'button',
            buttonProps,
            buttonContent
        ) : _react2.default.createElement(
            'span',
            buttonProps,
            buttonContent
        );
    };

    Button.prototype.handleClick = function handleClick(event) {
        if (this.props.onClick) {
            this.props.onClick(event);
        }
    };

    Button.prototype.handleFocus = function handleFocus(event) {
        if (this.state.pressed) return;

        this.setState({ focused: true });

        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    };

    Button.prototype.handleBlur = function handleBlur(event) {
        this.setState({ focused: false });

        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    };

    Button.prototype.handleMouseEnter = function handleMouseEnter(event) {
        if (!this.props.disabled) {
            this.setState({ hovered: true });
        }

        if (this.props.onMouseEnter) {
            this.props.onMouseEnter(event);
        }
    };

    Button.prototype.handleMouseLeave = function handleMouseLeave(event) {
        if (!this.props.disabled) {
            this.setState({ hovered: false });
        }

        if (this.props.onMouseLeave) {
            this.props.onMouseLeave(event);
        }
    };

    Button.prototype.handleMouseDown = function handleMouseDown(event) {
        if (!this.props.disabled) {
            this.setState({ pressed: true });
        }

        if (this.props.onMouseDown) {
            this.props.onMouseDown(event);
        }
    };

    Button.prototype.handleMouseUp = function handleMouseUp(event) {
        if (!this.props.disabled) {
            this.setState({ pressed: false });
        }

        if (this.props.onMouseUp) {
            this.props.onMouseUp(event);
        }
    };

    Button.prototype.handleMouseOut = function handleMouseOut(event) {
        if (!this.props.disabled) {
            this.setState({ pressed: false });
        }

        if (this.props.onMouseOut) {
            this.props.onMouseOut(event);
        }
    };

    Button.prototype.handleKeyDown = function handleKeyDown(event) {
        if ((event.which === _keyboardCode2.default.ENTER || event.which === _keyboardCode2.default.SPACE) && !this.props.disabled) {
            this.setState({ pressed: true });
        }

        if (this.props.onKeyDown) {
            this.props.onKeyDown(event);
        }
    };

    Button.prototype.handleKeyUp = function handleKeyUp(event) {
        if ((event.which === _keyboardCode2.default.ENTER || event.which === _keyboardCode2.default.SPACE) && !this.props.disabled) {
            this.setState({ pressed: false });
        }

        if (this.props.onKeyUp) {
            this.props.onKeyUp(event);
        }
    };

    /**
     * Возвращает корневой `HTMLElement` компонента.
     *
     * @public
     * @returns {HTMLElement}
     */


    Button.prototype.getNode = function getNode() {
        return this.control;
    };

    /**
     * Устанавливает фокус на поле ввода.
     *
     * @public
     */


    Button.prototype.focus = function focus() {
        this.control.focus();
    };

    /**
     * Убирает фокус с поля ввода.
     *
     * @public
     */


    Button.prototype.blur = function blur() {
        if (document.activeElement) {
            document.activeElement.blur();
        }
    };

    return Button;
}(_react2.default.Component), _class3.defaultProps = {
    type: 'button',
    tag: 'button',
    size: 'm',
    formNoValidate: false
}, _temp2), (_applyDecoratedDescriptor(_class2.prototype, 'handleClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleClick'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleFocus', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleFocus'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleBlur', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleBlur'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseEnter', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseEnter'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseLeave', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseLeave'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseDown', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseDown'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseUp', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseUp'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseOut', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseOut'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleKeyDown', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleKeyDown'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleKeyUp', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleKeyUp'), _class2.prototype)), _class2)) || _class) || _class);
Button.propTypes =  true ? {
    /** Текст кнопки */
    text: _propTypes2.default.node,
    /** Иконка кнопки */
    icon: _propTypes2.default.node,
    /** Список произвольных элементов в левом слоте */
    rightAddons: _propTypes2.default.node,
    /** Список произвольных элементов в правом слоте */
    leftAddons: _propTypes2.default.node,
    /** Тип кнопки */
    view: _propTypes2.default.oneOf(['default', 'action', 'extra', 'rounded']),
    /** Поведение кнопки */
    type: _propTypes2.default.oneOf(['button', 'reset', 'submit']),
    /** HTML элемент, которым будет компонент в DOM */
    tag: _propTypes2.default.oneOf(['button', 'span']),
    /** Управление шириной кнопки. При значении 'available' растягивает кнопку на ширину родителя */
    width: _propTypes2.default.oneOf(['default', 'available']),
    /** Размер компонента */
    size: _propTypes2.default.oneOf(['s', 'm', 'l', 'xl']),
    /** Управление возможности взаимодействия с компонентом */
    disabled: _propTypes2.default.bool,
    /** Отображение кнопки в состоянии фокуса */
    focused: _propTypes2.default.bool,
    /** Псевдо представление кнопки */
    pseudo: _propTypes2.default.bool,
    /** Идентификатор компонента в DOM */
    id: _propTypes2.default.string,
    /** Отключает валидацию полей формы, у которых есть атрибут pattern */
    formNoValidate: _propTypes2.default.bool,
    /** Имя компонента в DOM */
    name: _propTypes2.default.string,
    /** Текст всплывающей подсказки */
    title: _propTypes2.default.string,
    /** Последовательность перехода между контролами при нажатии на Tab */
    tabIndex: _propTypes2.default.number,
    /** Тип переключателя */
    togglable: _propTypes2.default.oneOf(['check', 'radio']),
    /** Отображение кнопки в отмеченном (зажатом) состоянии */
    checked: _propTypes2.default.bool,
    /** Дочерние элементы `Button` */
    children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]),
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string,
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /**
     * Обработчик клика по кнопке
     * @param {React.MouseEvent} event
     */
    onClick: _propTypes2.default.func,
    /**
     * Обработчик фокуса кнопки
     * @param {React.FocusEvent} event
     */
    onFocus: _propTypes2.default.func,
    /**
     * Обработчик снятия фокуса кнопки
     * @param {React.FocusEvent} event
     */
    onBlur: _propTypes2.default.func,
    /**
     * Обработчик события наведения курсора на кнопку
     * @param {React.MouseEvent} event
     */
    onMouseEnter: _propTypes2.default.func,
    /**
     * Обработчик события снятия курсора с кнопки
     * @param {React.MouseEvent} event
     */
    onMouseLeave: _propTypes2.default.func,
    /**
     * Обработчик события нажатия кнопки мыши в момент
     * @param {React.MouseEvent} event
     */
    onMouseDown: _propTypes2.default.func,
    /**
     * Обработчик события отжатия кнопки мыши в момент
     * @param {React.MouseEvent} event
     */
    onMouseUp: _propTypes2.default.func,
    /**
     * Обработчик события отведения курсора с кнопки
     * @param {React.MouseEvent} event
     */
    onMouseOut: _propTypes2.default.func,
    /**
     * Обработчик события нажатия на клавишу клавиатуры в момент, когда фокус находится на компоненте
     * @param {React.KeyboardEvent} event
     */
    onKeyDown: _propTypes2.default.func,
    /**
     * Обработчик события отжатия на клавишу клавиатуры в момент, когда фокус находится на компоненте
     * @param {React.KeyboardEvent} event
     */
    onKeyUp: _propTypes2.default.func
} : undefined;
exports.default = Button;
//# sourceMappingURL=button.js.map


/***/ }),

/***/ "./node_modules/arui-feather/button/button_theme_alfa-on-color.css":
/*!*************************************************************************!*\
  !*** ./node_modules/arui-feather/button/button_theme_alfa-on-color.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"button_theme_alfa-on-color": "_3hxu0PIkqPb88MuvP_cO7A",
	"button_hovered": "_38OFmCHlLr5aZZ4nb1BUi9",
	"button_focused": "_3UyOIFNvq2r3bBqGUvz6Ip",
	"button_pressed": "_2W2C4g5nfTpqGUAn1-Nw42",
	"button_checked": "_2T8_Vs7UsyFcrlTWMNrEAc",
	"button_view_action": "_2J4A3r4XbjGlnFEeBbqyR0",
	"button_view_extra": "HQ9QMwyA5fEpO22YTj9oV",
	"button_view_rounded": "_1evWsa-p8ICue2RXqc3ekI",
	"button_pseudo": "_2__C9gCzj2O6PsGx25xqw6",
	"button_disabled": "_1CwLzjKIwkZzkboI3znJbV",
	"button__text": "_2pywW04WpRjMYuIMdzqI3P"
};

/***/ }),

/***/ "./node_modules/arui-feather/button/button_theme_alfa-on-white.css":
/*!*************************************************************************!*\
  !*** ./node_modules/arui-feather/button/button_theme_alfa-on-white.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"button_theme_alfa-on-white": "QZUnf18byl4dNxXy84-0P",
	"button_hovered": "_2l2dGCrfhFTf9opRzwPROT",
	"button_focused": "omt-Rnr9JWJ1G4BQbf8YQ",
	"button_pressed": "_2L0MVmbN4kOJBsa9-thmAT",
	"button_checked": "_1XmswCShQQNcmY7u3wDSLz",
	"button_view_action": "_1wwq-ijbqCV2XqDtI7oV71",
	"button_view_extra": "_2c9zdgKoJIyVKa9gv2hb8o",
	"button_view_rounded": "_3zzOlNmLb1vSF1thPrrpaU",
	"button_disabled": "_3egpW6y0tM5afuC3M4LuGt",
	"button__text": "_3m6_RAzeJqoVrUmffqJGxA",
	"button_pseudo": "_26B0Ue2MaWBJy8Q62i8ioe"
};

/***/ }),

/***/ "./node_modules/arui-feather/button/index.js":
/*!***************************************************!*\
  !*** ./node_modules/arui-feather/button/index.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./button.css */ "./node_modules/arui-feather/button/button.css");

__webpack_require__(/*! ./button_theme_alfa-on-color.css */ "./node_modules/arui-feather/button/button_theme_alfa-on-color.css");

__webpack_require__(/*! ./button_theme_alfa-on-white.css */ "./node_modules/arui-feather/button/button_theme_alfa-on-white.css");

var _button = __webpack_require__(/*! ./button */ "./node_modules/arui-feather/button/button.js");

var _button2 = _interopRequireDefault(_button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

exports.default = _button2.default;
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/cn.js":
/*!*****************************************!*\
  !*** ./node_modules/arui-feather/cn.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _cnDecorator = __webpack_require__(/*! cn-decorator */ "cn-decorator");

var _cnDecorator2 = _interopRequireDefault(_cnDecorator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _cnDecorator2.default.create(['alfa-on-white', 'alfa-on-color']); /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                                     * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                                     * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=cn.js.map


/***/ }),

/***/ "./node_modules/arui-feather/dropdown/dropdown.js":
/*!********************************************************!*\
  !*** ./node_modules/arui-feather/dropdown/dropdown.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _dec, _dec2, _class, _desc, _value, _class2, _class3, _temp2; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                   * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                   * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _button = __webpack_require__(/*! ../button/button */ "./node_modules/arui-feather/button/button.js");

var _button2 = _interopRequireDefault(_button);

var _link = __webpack_require__(/*! ../link/link */ "./node_modules/arui-feather/link/link.js");

var _link2 = _interopRequireDefault(_link);

var _popup = __webpack_require__(/*! ../popup/popup */ "./node_modules/arui-feather/popup/popup.js");

var _popup2 = _interopRequireDefault(_popup);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

var _vars = __webpack_require__(/*! ../vars */ "./node_modules/arui-feather/vars.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

/**
 * Компонент «выпадашка»: ссылка или кнопка. По клику показывается Popup.
 */
var Dropdown = (_dec = (0, _cn2.default)('dropdown'), _dec2 = (0, _performance2.default)(), _dec(_class = _dec2(_class = (_class2 = (_temp2 = _class3 = function (_React$Component) {
    _inherits(Dropdown, _React$Component);

    function Dropdown() {
        var _temp, _this, _ret;

        _classCallCheck(this, Dropdown);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.state = {
            opened: false,
            switcherHovered: false,
            popupHovered: false
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    Dropdown.prototype.componentDidMount = function componentDidMount() {
        this.popup.setTarget(this.switcher.getNode());
    };

    Dropdown.prototype.render = function render(cn) {
        return _react2.default.createElement(
            'div',
            {
                className: cn(),
                id: this.props.id,
                'data-test-id': this.props['data-test-id']
            },
            this.renderSwitcher(cn),
            this.renderPopup(cn)
        );
    };

    Dropdown.prototype.renderSwitcher = function renderSwitcher(cn) {
        var content = this.props.children || this.props.switcherText;
        var opened = this.props.opened !== undefined ? this.props.opened : this.state.opened;

        return this.props.switcherType === 'button' ? this.renderSwitcherButton(cn, content, opened) : this.renderSwitcherLink(cn, content);
    };

    Dropdown.prototype.renderSwitcherButton = function renderSwitcherButton(cn, content, opened) {
        var _this2 = this;

        return _react2.default.createElement(
            _button2.default,
            {
                className: cn('switcher'),
                size: this.props.size,
                ref: function ref(switcher) {
                    _this2.switcher = switcher;
                },
                disabled: this.props.disabled,
                togglable: this.props.togglable,
                checked: this.props.togglable === 'check' && opened,
                onClick: !this.props.disabled ? this.handleSwitcherClick : undefined,
                onMouseEnter: this.handleSwitcherMouseEnter,
                onMouseLeave: this.handleSwitcherMouseLeave
            },
            content
        );
    };

    Dropdown.prototype.renderSwitcherLink = function renderSwitcherLink(cn, content) {
        var _this3 = this;

        return _react2.default.createElement(_link2.default, {
            className: cn('switcher'),
            size: this.props.size,
            ref: function ref(switcher) {
                _this3.switcher = switcher;
            },
            disabled: this.props.disabled,
            pseudo: true,
            text: content,
            onClick: !this.props.disabled ? this.handleSwitcherClick : undefined,
            onMouseEnter: this.handleSwitcherMouseEnter,
            onMouseLeave: this.handleSwitcherMouseLeave
        });
    };

    Dropdown.prototype.renderPopup = function renderPopup(cn) {
        var _this4 = this;

        var mainOffset = void 0;
        var opened = this.props.opened !== undefined ? this.props.opened : this.state.opened;

        if (this.props.popupProps === undefined || this.props.popupProps && this.props.popupProps.type !== 'tooltip') {
            switch (this.props.size) {
                case 's':
                case 'm':
                    mainOffset = _vars.POPUP_MAIN_OFFSET / 2;break;
                case 'l':
                case 'xl':
                    mainOffset = _vars.POPUP_MAIN_OFFSET;break;
            }
        }

        var popupProps = Object.assign({
            className: cn('popup'),
            size: this.props.size,
            mainOffset: mainOffset
        }, this.props.popupProps);

        return _react2.default.createElement(
            _popup2.default,
            _extends({}, popupProps, {
                ref: function ref(popup) {
                    _this4.popup = popup;
                },
                visible: !this.props.disabled && opened || this.props.mode === 'hover' && (this.state.switcherHovered || this.state.popupHovered),
                onMouseEnter: this.handlePopupMouseEnter,
                onMouseLeave: this.handlePopupMouseLeave,
                onClickOutside: this.handlePopupClickOutside
            }),
            this.props.popupContent
        );
    };

    Dropdown.prototype.handleSwitcherClick = function handleSwitcherClick() {
        var newOpenedStatusValue = this.props.opened !== undefined ? !this.props.opened : !this.state.opened;

        this.setState({
            opened: newOpenedStatusValue
        });

        if (this.props.onSwitcherClick) {
            this.props.onSwitcherClick(newOpenedStatusValue);
        }
    };

    Dropdown.prototype.handleSwitcherMouseEnter = function handleSwitcherMouseEnter(event) {
        this.setState({ switcherHovered: true });

        if (this.props.onSwitcherMouseEnter) {
            this.props.onSwitcherMouseEnter(event);
        }
    };

    Dropdown.prototype.handleSwitcherMouseLeave = function handleSwitcherMouseLeave(event) {
        this.setState({ switcherHovered: false });

        if (this.props.onSwitcherMouseLeave) {
            this.props.onSwitcherMouseLeave(event);
        }
    };

    Dropdown.prototype.handlePopupMouseEnter = function handlePopupMouseEnter(event) {
        this.setState({ popupHovered: true });

        if (this.props.onPopupMouseEnter) {
            this.props.onPopupMouseEnter(event);
        }
    };

    Dropdown.prototype.handlePopupMouseLeave = function handlePopupMouseLeave(event) {
        this.setState({ popupHovered: false });

        if (this.props.onPopupMouseLeave) {
            this.props.onPopupMouseLeave(event);
        }
    };

    Dropdown.prototype.handlePopupClickOutside = function handlePopupClickOutside(event) {
        this.setState({ opened: false });

        if (this.props.onPopupClickOutside) {
            this.props.onPopupClickOutside(event);
        }
    };

    return Dropdown;
}(_react2.default.Component), _class3.defaultProps = {
    switcherType: 'link',
    switcherText: 'Switcher',
    disabled: false,
    popupProps: {
        target: 'anchor'
    },
    size: 'm'
}, _temp2), (_applyDecoratedDescriptor(_class2.prototype, 'handleSwitcherClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleSwitcherClick'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleSwitcherMouseEnter', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleSwitcherMouseEnter'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleSwitcherMouseLeave', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleSwitcherMouseLeave'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handlePopupMouseEnter', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handlePopupMouseEnter'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handlePopupMouseLeave', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handlePopupMouseLeave'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handlePopupClickOutside', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handlePopupClickOutside'), _class2.prototype)), _class2)) || _class) || _class);
Dropdown.propTypes =  true ? {
    /** Тип компонента */
    switcherType: _propTypes2.default.oneOf(['link', 'button']),
    /** Текст кнопки компонента */
    switcherText: _propTypes2.default.node,
    /** Компонент [Popup](#!/Popup) */
    popupContent: _propTypes2.default.node,
    /** Свойства для компонента [Popup](#!/Popup) */
    popupProps: _propTypes2.default.shape({
        className: _propTypes2.default.string,
        type: _propTypes2.default.oneOf(['default', 'tooltip']),
        height: _propTypes2.default.oneOf(['default', 'available', 'adaptive']),
        directions: _propTypes2.default.arrayOf(_propTypes2.default.oneOf(['anchor', 'top-left', 'top-center', 'top-right', 'left-top', 'left-center', 'left-bottom', 'right-top', 'right-center', 'right-bottom', 'bottom-left', 'bottom-center', 'bottom-right'])),
        target: _propTypes2.default.oneOf(['anchor', 'position', 'screen']),
        mainOffset: _propTypes2.default.number,
        secondaryOffset: _propTypes2.default.number,
        fitContaiterOffset: _propTypes2.default.number,
        invalid: _propTypes2.default.bool,
        visible: _propTypes2.default.bool,
        padded: _propTypes2.default.bool,
        size: _propTypes2.default.oneOf(['s', 'm', 'l', 'xl']),
        theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
        onMouseEnter: _propTypes2.default.func,
        onMouseLeave: _propTypes2.default.func,
        onClickOutside: _propTypes2.default.func,
        minWidth: _propTypes2.default.number,
        maxWidth: _propTypes2.default.number
    }),
    /** Управление возможностью отображать попап при наведении курсора */
    mode: _propTypes2.default.oneOf(['hover', 'normal']),
    /** Управление возможностью открытия попапа */
    disabled: _propTypes2.default.bool,
    /** Управление состоянием открыт/закрыт попапа */
    opened: _propTypes2.default.bool,
    /** Только для switcherType='button'. Тип переключателя для кнопки, 'check' */
    togglable: _propTypes2.default.oneOf(['button', 'check']),
    /** Размер компонента */
    size: _propTypes2.default.oneOf(['s', 'm', 'l', 'xl']),
    /** Дочерние элементы `Dropdown` */
    children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]),
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /** Идентификатор компонента в DOM */
    id: _propTypes2.default.string,
    /**
     * Обработчик клика по кнопке компонента
     * @param {boolean} isOpened
     */
    onSwitcherClick: _propTypes2.default.func,
    /**
     * Обработчик события наведения курсора на кнопку компонента
     * @param {React.MouseEvent} event
     */
    onSwitcherMouseEnter: _propTypes2.default.func,
    /**
     * Обработчик события снятия курсора с кнопки компонента
     * @param {React.MouseEvent} event
     */
    onSwitcherMouseLeave: _propTypes2.default.func,
    /**
     * Обработчик события наведения курсора на попап
     * @param {React.MouseEvent} event
     */
    onPopupMouseEnter: _propTypes2.default.func,
    /**
     * Обработчик события снятия курсора с попапа
     * @param {React.MouseEvent} event
     */
    onPopupMouseLeave: _propTypes2.default.func,
    /**
     * Обработчик события клика попапа за пределами попапа
     * @param {React.MouseEvent} event
     */
    onPopupClickOutside: _propTypes2.default.func,
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = Dropdown;
//# sourceMappingURL=dropdown.js.map


/***/ }),

/***/ "./node_modules/arui-feather/dropdown/index.js":
/*!*****************************************************!*\
  !*** ./node_modules/arui-feather/dropdown/index.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ../button */ "./node_modules/arui-feather/button/index.js");

__webpack_require__(/*! ../link */ "./node_modules/arui-feather/link/index.js");

__webpack_require__(/*! ../popup */ "./node_modules/arui-feather/popup/index.js");

var _dropdown = __webpack_require__(/*! ./dropdown */ "./node_modules/arui-feather/dropdown/dropdown.js");

var _dropdown2 = _interopRequireDefault(_dropdown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

exports.default = _dropdown2.default;
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/form/form.css":
/*!*************************************************!*\
  !*** ./node_modules/arui-feather/form/form.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"form": "_28IM8FuKg-GeoNvaDMly_V",
	"form__footer": "_23dmAQRky31qQ3T8O__tle",
	"form_view_line": "_2WLancKF9vKj2ozg1CFgn4",
	"form_size_s": "_2G9y6G76RYBctU6Ar1SKGA",
	"form_size_m": "_2qyU5KxdDyZI_cF6EQVS0i",
	"form_size_l": "_3ucN6XnUFqHDaQrTcikRS",
	"form_size_xl": "vn6Wh8X-koLuHB4S0AoXQ"
};

/***/ }),

/***/ "./node_modules/arui-feather/form/form.js":
/*!************************************************!*\
  !*** ./node_modules/arui-feather/form/form.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _dec, _dec2, _class, _desc, _value, _class2, _class3, _temp; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                  * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

/**
 * Компонент формы.
 */
var Form = (_dec = (0, _cn2.default)('form'), _dec2 = (0, _performance2.default)(), _dec(_class = _dec2(_class = (_class2 = (_temp = _class3 = function (_React$Component) {
    _inherits(Form, _React$Component);

    function Form() {
        _classCallCheck(this, Form);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    Form.prototype.render = function render(cn) {
        return _react2.default.createElement(
            'form',
            {
                action: this.props.action,
                encType: this.props.enctype,
                method: this.props.method,
                noValidate: this.props.noValidate,
                autoComplete: this.props.autocomplete === false ? 'off' : 'on',
                className: cn({
                    size: this.props.size,
                    view: this.props.view
                }),
                id: this.props.id,
                name: this.props.name,
                onSubmit: this.handleSubmit,
                'data-test-id': this.props['data-test-id']
            },
            this.props.children,
            this.props.footer && _react2.default.createElement(
                'div',
                { className: cn('footer') },
                this.props.footer
            )
        );
    };

    Form.prototype.handleSubmit = function handleSubmit(event) {
        event.preventDefault();

        if (this.props.onSubmit) {
            this.props.onSubmit(event);
        }
    };

    return Form;
}(_react2.default.Component), _class3.defaultProps = {
    action: '/',
    enctype: 'application/x-www-form-urlencoded',
    method: 'post',
    size: 'm',
    noValidate: false,
    autocomplete: true
}, _temp), (_applyDecoratedDescriptor(_class2.prototype, 'handleSubmit', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleSubmit'), _class2.prototype)), _class2)) || _class) || _class);
Form.propTypes =  true ? {
    /** Способ кодирования данных формы при их отправке */
    enctype: _propTypes2.default.oneOf(['application/x-www-form-urlencoded', 'multipart/form-data', 'text/plain']),
    /** Адрес отправки данных на сервер */
    action: _propTypes2.default.string,
    /** Метод запроса */
    method: _propTypes2.default.oneOf(['post', 'get']),
    /** Тип формы */
    view: _propTypes2.default.oneOf(['line', 'normal']),
    /** Размер компонента */
    size: _propTypes2.default.oneOf(['s', 'm', 'l', 'xl']),
    /** Футер для формы */
    footer: _propTypes2.default.node,
    /** Управление встроенным в браузер механизмом валидации формы */
    noValidate: _propTypes2.default.bool,
    /** Управление автозаполнением формы */
    autocomplete: _propTypes2.default.bool,
    /** Дочерние элементы формы */
    children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]),
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /** Идентификатор компонента в DOM */
    id: _propTypes2.default.string,
    /** Имя компонента в DOM */
    name: _propTypes2.default.string,
    /**
     * Обработчик отправки формы
     * @param {React.FormEvent} event
     */
    onSubmit: _propTypes2.default.func,
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = Form;
//# sourceMappingURL=form.js.map


/***/ }),

/***/ "./node_modules/arui-feather/form/index.js":
/*!*************************************************!*\
  !*** ./node_modules/arui-feather/form/index.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./form.css */ "./node_modules/arui-feather/form/form.css");

var _form = __webpack_require__(/*! ./form */ "./node_modules/arui-feather/form/form.js");

var _form2 = _interopRequireDefault(_form);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

exports.default = _form2.default;
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon-button/icon-button.css":
/*!***************************************************************!*\
  !*** ./node_modules/arui-feather/icon-button/icon-button.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"icon-button": "_3eK7QASAlTXrfE98Iwe1mh",
	"icon-button_hovered": "_24FV1395yfYo30YSotipeb",
	"icon-button_focused": "_1ay7B53y1zX1bQximRqX4n",
	"icon-button_disabled": "_2_V8lo4nDnHvj5OekxFmRh",
	"icon-button_size_s": "_1TY3bsJgHvo_81g2wJ7Kdi",
	"icon-button_size_m": "gBfA2mFsvTtt__cPpH-z8",
	"icon-button_size_l": "_2DdVxT6_OkgDUp8qJbEqnu",
	"icon-button_size_xl": "_3T4d3feysjCNII2jd1eI_g"
};

/***/ }),

/***/ "./node_modules/arui-feather/icon-button/icon-button.js":
/*!**************************************************************!*\
  !*** ./node_modules/arui-feather/icon-button/icon-button.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _dec, _class; /* This Source Code Form is subject to the terms of the Mozilla Public
                   * License, v. 2.0. If a copy of the MPL was not distributed with this
                   * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _button = __webpack_require__(/*! ../button/button */ "./node_modules/arui-feather/button/button.js");

var _button2 = _interopRequireDefault(_button);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

/**
 * Компонент кнопки с иконкой.
 *
 * @extends Button
 */
var IconButton = (_dec = (0, _cn2.default)('icon-button'), _dec(_class = function (_Button) {
  _inherits(IconButton, _Button);

  function IconButton() {
    _classCallCheck(this, IconButton);

    return _possibleConstructorReturn(this, _Button.apply(this, arguments));
  }

  return IconButton;
}(_button2.default)) || _class);
exports.default = IconButton;
//# sourceMappingURL=icon-button.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon-button/index.js":
/*!********************************************************!*\
  !*** ./node_modules/arui-feather/icon-button/index.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./icon-button.css */ "./node_modules/arui-feather/icon-button/icon-button.css");

var _iconButton = __webpack_require__(/*! ./icon-button */ "./node_modules/arui-feather/icon-button/icon-button.js");

var _iconButton2 = _interopRequireDefault(_iconButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

exports.default = _iconButton2.default;
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/icon.css":
/*!*************************************************!*\
  !*** ./node_modules/arui-feather/icon/icon.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"icon": "_1_vd-rUZ8g4cWdusYq150H",
	"icon_size_xs": "_39thURn0E-2RjDFKwgNwdX",
	"icon_size_s": "_3Gyb77V-AnqPWXiI7Cz6JC",
	"icon_size_m": "_3SKaXT1phIqnQOKiAtYHlI",
	"icon_size_l": "_2D1Z37c0TqdN2pHEqVsgU5",
	"icon_size_xl": "_36tZp2vbxqRh4BW_HitBZr",
	"icon_size_xxl": "_2ymzdjL9LSoxzFbT6Tumif",
	"icon_size_xxxl": "_2aVzIoFLCVra9qsOurRek-"
};

/***/ }),

/***/ "./node_modules/arui-feather/icon/icon.js":
/*!************************************************!*\
  !*** ./node_modules/arui-feather/icon/icon.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _dec, _dec2, _class, _class2, _temp; /* This Source Code Form is subject to the terms of the Mozilla Public
                                          * License, v. 2.0. If a copy of the MPL was not distributed with this
                                          * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

/**
 * Базовый компонент иконки. Содержит в себе только необходимые для компонентов иконки.
 */
var Icon = (_dec = (0, _cn2.default)('icon'), _dec2 = (0, _performance2.default)(), _dec(_class = _dec2(_class = (_temp = _class2 = function (_React$Component) {
    _inherits(Icon, _React$Component);

    function Icon() {
        _classCallCheck(this, Icon);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    Icon.prototype.render = function render(cn) {
        var mods = { size: this.props.size };

        if (this.props.name) {
            mods.name = this.props.name;
        }

        if (this.props.colored) {
            mods.colored = true;
        }

        return _react2.default.createElement('span', {
            className: cn(mods),
            id: this.props.id,
            'data-test-id': this.props['data-test-id']
        });
    };

    return Icon;
}(_react2.default.Component), _class2.defaultProps = {
    size: 'm'
}, _temp)) || _class) || _class);
Icon.propTypes =  true ? {
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /** Управление цветностью иконки */
    colored: _propTypes2.default.bool,
    /** Идентификатор компонента в DOM */
    id: _propTypes2.default.string,
    /** Название иконки */
    name: _propTypes2.default.string,
    /** Размер иконки */
    size: _propTypes2.default.oneOf(['xs', 's', 'm', 'l', 'xl', 'xxl', 'xxxl']),
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = Icon;
//# sourceMappingURL=icon.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/index.js":
/*!*************************************************!*\
  !*** ./node_modules/arui-feather/icon/index.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./icon.css */ "./node_modules/arui-feather/icon/icon.css");

var _icon = __webpack_require__(/*! ./icon */ "./node_modules/arui-feather/icon/icon.js");

var _icon2 = _interopRequireDefault(_icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

exports.default = _icon2.default;
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/arrow-down/arrow-down.css":
/*!*********************************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/arrow-down/arrow-down.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"icon_size_l": "Q8k4RqQhBCFIGoSNnUfFN",
	"icon_name_arrow-down": "_1A9cnKUvR85DE_wJTysFD3",
	"icon_theme_alfa-on-white": "_2OT_ARnRcLfOP-ewxA-6OH",
	"icon_theme_alfa-on-color": "YUv7KALWNWH3DcXQeRI5K",
	"icon_size_m": "_2wtagG9gIw-y3XuMzey6_S",
	"icon_size_s": "_1_TIMP5TnQeAbas9cgF6MP",
	"icon_size_xl": "_25wkhenNNaFT3TQCt-1OjV",
	"icon_size_xxl": "_3Z1-JLC0pF-Fo497KSZrVB"
};

/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/arrow-down/arrow-down.js":
/*!********************************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/arrow-down/arrow-down.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _icon = __webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _icon2 = _interopRequireDefault(_icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); } /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var IconArrowDown = function (_React$Component) {
    _inherits(IconArrowDown, _React$Component);

    function IconArrowDown() {
        _classCallCheck(this, IconArrowDown);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    IconArrowDown.prototype.render = function render() {
        return _react2.default.createElement(_icon2.default, _extends({}, this.props, {
            name: 'arrow-down'
        }));
    };

    return IconArrowDown;
}(_react2.default.Component);

IconArrowDown.propTypes =  true ? _icon2.default.propTypes : undefined;
exports.default = IconArrowDown;
//# sourceMappingURL=arrow-down.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/arrow-down/index.js":
/*!***************************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/arrow-down/index.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./arrow-down.css */ "./node_modules/arui-feather/icon/ui/arrow-down/arrow-down.css");

__webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _arrowDown = __webpack_require__(/*! ./arrow-down */ "./node_modules/arui-feather/icon/ui/arrow-down/arrow-down.js");

var _arrowDown2 = _interopRequireDefault(_arrowDown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _arrowDown2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                        * License, v. 2.0. If a copy of the MPL was not distributed with this
                                        * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/arrow-up/arrow-up.css":
/*!*****************************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/arrow-up/arrow-up.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"icon_size_l": "_2RKXo5cKilIk-FY5f2e3Vi",
	"icon_name_arrow-up": "_20FxoGvFLx0duZ0oYlR6ys",
	"icon_theme_alfa-on-white": "_2_IomDeLYT_bl5natfsc1h",
	"icon_theme_alfa-on-color": "_3lzisxWphn4-QhGE7eGhN2",
	"icon_size_m": "_4fwsL9kSwGhxNot4NIlNn",
	"icon_size_s": "_3HS2qlEKiM9xkwrAiUxW5p",
	"icon_size_xl": "_1u5WxH3oTX4EAmb6b8xpPy",
	"icon_size_xxl": "qR1RCNy8rbvO-qw7HpB4D"
};

/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/arrow-up/arrow-up.js":
/*!****************************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/arrow-up/arrow-up.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _icon = __webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _icon2 = _interopRequireDefault(_icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); } /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var IconArrowUp = function (_React$Component) {
    _inherits(IconArrowUp, _React$Component);

    function IconArrowUp() {
        _classCallCheck(this, IconArrowUp);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    IconArrowUp.prototype.render = function render() {
        return _react2.default.createElement(_icon2.default, _extends({}, this.props, {
            name: 'arrow-up'
        }));
    };

    return IconArrowUp;
}(_react2.default.Component);

IconArrowUp.propTypes =  true ? _icon2.default.propTypes : undefined;
exports.default = IconArrowUp;
//# sourceMappingURL=arrow-up.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/arrow-up/index.js":
/*!*************************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/arrow-up/index.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./arrow-up.css */ "./node_modules/arui-feather/icon/ui/arrow-up/arrow-up.css");

__webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _arrowUp = __webpack_require__(/*! ./arrow-up */ "./node_modules/arui-feather/icon/ui/arrow-up/arrow-up.js");

var _arrowUp2 = _interopRequireDefault(_arrowUp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _arrowUp2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                      * License, v. 2.0. If a copy of the MPL was not distributed with this
                                      * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/check/check.css":
/*!***********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/check/check.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"icon_size_l": "_5uxkUcXS5YgXmCWuD3rbX",
	"icon_name_check": "_3nRKZ0NRVvfYorKa5dN35b",
	"icon_theme_alfa-on-white": "_2Tt-Z3anrzJ906wcll5HPO",
	"icon_theme_alfa-on-color": "O4fxFX8MG09Jo8vWkUZPu",
	"icon_size_m": "_2d3XHjd0ct9CQzkqNAzunb",
	"icon_size_s": "_2t7jJZOlkHY6TYmAzH7Ra1",
	"icon_size_xl": "_23bowAPPfg_KeA6OUhyLRD",
	"icon_size_xxl": "_1vBp3Ky1o3zw5ZcJUSQecA"
};

/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/check/check.js":
/*!**********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/check/check.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _icon = __webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _icon2 = _interopRequireDefault(_icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); } /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var IconCheck = function (_React$Component) {
    _inherits(IconCheck, _React$Component);

    function IconCheck() {
        _classCallCheck(this, IconCheck);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    IconCheck.prototype.render = function render() {
        return _react2.default.createElement(_icon2.default, _extends({}, this.props, {
            name: 'check'
        }));
    };

    return IconCheck;
}(_react2.default.Component);

IconCheck.propTypes =  true ? _icon2.default.propTypes : undefined;
exports.default = IconCheck;
//# sourceMappingURL=check.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/check/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/check/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./check.css */ "./node_modules/arui-feather/icon/ui/check/check.css");

__webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _check = __webpack_require__(/*! ./check */ "./node_modules/arui-feather/icon/ui/check/check.js");

var _check2 = _interopRequireDefault(_check);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _check2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                    * License, v. 2.0. If a copy of the MPL was not distributed with this
                                    * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/close/close.css":
/*!***********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/close/close.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"icon_size_l": "_1LLdmHpPvKSeADLM7mVcdf",
	"icon_name_close": "_3v7DwmSWjXUPty_U_RVhGP",
	"icon_theme_alfa-on-white": "_3CDMWufZQ7tNKdvAzT7HxW",
	"icon_theme_alfa-on-color": "_3B0MSGhDEeuHZk-F-o2eAY",
	"icon_size_m": "_31N_wBnHdnAQ01Yhb9bWbG",
	"icon_size_s": "_627ycssuPfI-hmxpMuI4h",
	"icon_size_xl": "ycrOb22V0N_bEbp_sn4St",
	"icon_size_xs": "H_doSqRjuMuAqab2bLDdY"
};

/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/close/close.js":
/*!**********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/close/close.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _icon = __webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _icon2 = _interopRequireDefault(_icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); } /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var IconClose = function (_React$Component) {
    _inherits(IconClose, _React$Component);

    function IconClose() {
        _classCallCheck(this, IconClose);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    IconClose.prototype.render = function render() {
        return _react2.default.createElement(_icon2.default, _extends({}, this.props, {
            name: 'close'
        }));
    };

    return IconClose;
}(_react2.default.Component);

IconClose.propTypes =  true ? _icon2.default.propTypes : undefined;
exports.default = IconClose;
//# sourceMappingURL=close.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/close/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/close/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./close.css */ "./node_modules/arui-feather/icon/ui/close/close.css");

__webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _close = __webpack_require__(/*! ./close */ "./node_modules/arui-feather/icon/ui/close/close.js");

var _close2 = _interopRequireDefault(_close);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _close2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                    * License, v. 2.0. If a copy of the MPL was not distributed with this
                                    * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/error/error.css":
/*!***********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/error/error.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"icon_size_l": "_2sh3hDezgrFUW8lO-TGYvJ",
	"icon_name_error": "_1u3dWVJG_-XgkYB0cozMSQ",
	"icon_theme_alfa-on-white": "_9Nwy5JHviNKE0MsILhbC9",
	"icon_colored": "Z5ga8WPX7tNWECQ8KZvRv",
	"icon_theme_alfa-on-color": "_1Zzlz5WqFUt89pIVkdzTdV",
	"icon_size_m": "IZcdDq6KNH1dFdG-8T96Z",
	"icon_size_s": "_2TGSgWWesTfRqaKse6L_Kg",
	"icon_size_xl": "_200XMncLlazjkm-WUjzwW8",
	"icon_size_xxl": "_2i6DU6j_xH8Wesmf31I93S"
};

/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/error/error.js":
/*!**********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/error/error.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _icon = __webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _icon2 = _interopRequireDefault(_icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); } /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var IconError = function (_React$Component) {
    _inherits(IconError, _React$Component);

    function IconError() {
        _classCallCheck(this, IconError);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    IconError.prototype.render = function render() {
        return _react2.default.createElement(_icon2.default, _extends({}, this.props, {
            name: 'error'
        }));
    };

    return IconError;
}(_react2.default.Component);

IconError.propTypes =  true ? _icon2.default.propTypes : undefined;
exports.default = IconError;
//# sourceMappingURL=error.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/error/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/error/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./error.css */ "./node_modules/arui-feather/icon/ui/error/error.css");

__webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _error = __webpack_require__(/*! ./error */ "./node_modules/arui-feather/icon/ui/error/error.js");

var _error2 = _interopRequireDefault(_error);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _error2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                    * License, v. 2.0. If a copy of the MPL was not distributed with this
                                    * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/fail/fail.css":
/*!*********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/fail/fail.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"icon_size_l": "_2snhXfyHSzibr88KHDEdNR",
	"icon_name_fail": "yAdx2INlWW0tquwsit7vo",
	"icon_theme_alfa-on-white": "bPysY9M36AZ9pX-54UG9s",
	"icon_theme_alfa-on-color": "_2V-unfsIYJXEWltwqQAyp6",
	"icon_size_m": "_2qlEaqzqVzBnNsvO2kc3z8",
	"icon_size_s": "_2-nLPjKnZNfpty7EhsvlLA",
	"icon_size_xl": "xJ6jZ5TZX0UaQc-A-9Dl0",
	"icon_size_xxl": "_2IvH0Xi2zHEoAc6jXHVVtH"
};

/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/fail/fail.js":
/*!********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/fail/fail.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _icon = __webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _icon2 = _interopRequireDefault(_icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); } /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var IconFail = function (_React$Component) {
    _inherits(IconFail, _React$Component);

    function IconFail() {
        _classCallCheck(this, IconFail);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    IconFail.prototype.render = function render() {
        return _react2.default.createElement(_icon2.default, _extends({}, this.props, {
            name: 'fail'
        }));
    };

    return IconFail;
}(_react2.default.Component);

IconFail.propTypes =  true ? _icon2.default.propTypes : undefined;
exports.default = IconFail;
//# sourceMappingURL=fail.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/fail/index.js":
/*!*********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/fail/index.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./fail.css */ "./node_modules/arui-feather/icon/ui/fail/fail.css");

__webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _fail = __webpack_require__(/*! ./fail */ "./node_modules/arui-feather/icon/ui/fail/fail.js");

var _fail2 = _interopRequireDefault(_fail);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _fail2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                   * License, v. 2.0. If a copy of the MPL was not distributed with this
                                   * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/ok/index.js":
/*!*******************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/ok/index.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./ok.css */ "./node_modules/arui-feather/icon/ui/ok/ok.css");

__webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _ok = __webpack_require__(/*! ./ok */ "./node_modules/arui-feather/icon/ui/ok/ok.js");

var _ok2 = _interopRequireDefault(_ok);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _ok2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                 * License, v. 2.0. If a copy of the MPL was not distributed with this
                                 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/ok/ok.css":
/*!*****************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/ok/ok.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"icon_size_l": "Xg5kypZ8RjbMRSwcOmLmS",
	"icon_name_ok": "_2kO9KyWkpNelryhdIjFX7V",
	"icon_theme_alfa-on-white": "_3uagK1xxgukgc6ZQEofgVG",
	"icon_colored": "_19UK7wnJ_b1y9XzOPHlPeA",
	"icon_theme_alfa-on-color": "_2sRAUWQiaKDIDYdbpHofTS",
	"icon_size_m": "_2JJQafQnkjiyiK2n7HnM5S",
	"icon_size_s": "_22xHQzJe0Ul0-hZfR4pZnY",
	"icon_size_xl": "_2GDsBuve6iL9BQXlxfHHxL",
	"icon_size_xxl": "_2rOKyMMSE8Fsd_JB7oNtce",
	"icon_size_xxxl": "_1fC-kDISzcDnMs2ELgsmdd"
};

/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/ok/ok.js":
/*!****************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/ok/ok.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _icon = __webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _icon2 = _interopRequireDefault(_icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); } /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var IconOk = function (_React$Component) {
    _inherits(IconOk, _React$Component);

    function IconOk() {
        _classCallCheck(this, IconOk);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    IconOk.prototype.render = function render() {
        return _react2.default.createElement(_icon2.default, _extends({}, this.props, {
            name: 'ok'
        }));
    };

    return IconOk;
}(_react2.default.Component);

IconOk.propTypes =  true ? _icon2.default.propTypes : undefined;
exports.default = IconOk;
//# sourceMappingURL=ok.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/tick/index.js":
/*!*********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/tick/index.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./tick.css */ "./node_modules/arui-feather/icon/ui/tick/tick.css");

__webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _tick = __webpack_require__(/*! ./tick */ "./node_modules/arui-feather/icon/ui/tick/tick.js");

var _tick2 = _interopRequireDefault(_tick);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _tick2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                   * License, v. 2.0. If a copy of the MPL was not distributed with this
                                   * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/tick/tick.css":
/*!*********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/tick/tick.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"icon_size_l": "_28BO8n-OCTH-K4veffvTmo",
	"icon_name_tick": "_2y6_tEdyDcodvi3Y10koXO",
	"icon_theme_alfa-on-white": "_3rrcbmndxw3_7AMe3NneHJ",
	"icon_theme_alfa-on-color": "_2XvIDAp7NtBNVuCaetu1Im",
	"icon_size_m": "g6BRFpzke_IS_ZhJ0BBQg",
	"icon_size_s": "_2HTqM9KTeKBIYp7pveMHcG",
	"icon_size_xl": "_3ZJdWINfy5mAEN36W8vFpH",
	"icon_size_xs": "TMPm3qTp-rnw9PAiqcZo8",
	"icon_size_xxl": "_3BA6zCUMVVchK2c6JLKJMe"
};

/***/ }),

/***/ "./node_modules/arui-feather/icon/ui/tick/tick.js":
/*!********************************************************!*\
  !*** ./node_modules/arui-feather/icon/ui/tick/tick.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _icon = __webpack_require__(/*! ../../../icon */ "./node_modules/arui-feather/icon/index.js");

var _icon2 = _interopRequireDefault(_icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); } /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var IconTick = function (_React$Component) {
    _inherits(IconTick, _React$Component);

    function IconTick() {
        _classCallCheck(this, IconTick);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    IconTick.prototype.render = function render() {
        return _react2.default.createElement(_icon2.default, _extends({}, this.props, {
            name: 'tick'
        }));
    };

    return IconTick;
}(_react2.default.Component);

IconTick.propTypes =  true ? _icon2.default.propTypes : undefined;
exports.default = IconTick;
//# sourceMappingURL=tick.js.map


/***/ }),

/***/ "./node_modules/arui-feather/input/index.js":
/*!**************************************************!*\
  !*** ./node_modules/arui-feather/input/index.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ../icon/ui/close */ "./node_modules/arui-feather/icon/ui/close/index.js");

__webpack_require__(/*! ../icon-button */ "./node_modules/arui-feather/icon-button/index.js");

__webpack_require__(/*! ../masked-input */ "./node_modules/arui-feather/masked-input/index.js");

__webpack_require__(/*! ./input_theme_alfa-on-color.css */ "./node_modules/arui-feather/input/input_theme_alfa-on-color.css");

__webpack_require__(/*! ./input_theme_alfa-on-white.css */ "./node_modules/arui-feather/input/input_theme_alfa-on-white.css");

__webpack_require__(/*! ./input.css */ "./node_modules/arui-feather/input/input.css");

var _input = __webpack_require__(/*! ./input */ "./node_modules/arui-feather/input/input.js");

var _input2 = _interopRequireDefault(_input);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _input2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                    * License, v. 2.0. If a copy of the MPL was not distributed with this
                                    * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/input/input.css":
/*!***************************************************!*\
  !*** ./node_modules/arui-feather/input/input.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"input": "_2BpFCTP9BKOQtQqx-beWxr",
	"input__inner": "_2j6K6ulm7uO3kWTCoxN24D",
	"input__box": "ZrfiguCc1nw44iQHTwUhR",
	"input__top": "_1I9HA9V0eREAJPfpX8Phvy",
	"input__sub": "_3WUdCsoYnpUlEzn2T090Nw",
	"input__control": "SyUAtj6LLZxYn_sK01NWw",
	"input_has-label": "_18_V1aEp_xlLK1rX09Qsmd",
	"input_focused": "_2liOU9DVE_MaumpHIvvXKO",
	"input_has-clear": "_17thwIbznxGt35Vxejlj9v",
	"input__clear": "_15mkw95M06Zy11S_UhK7ZU",
	"input_has-icon": "_1Sv7lkefwYxcCGxKjlvTS6",
	"input__icon": "_3vvHp0c7D9K0P91WvRXb_A",
	"input_width_available": "_16zKy-aQOTpt3k4ddld6d_",
	"input_has-addons": "SpOqaIkx6kZdQbK7Kx4AP",
	"input__addons_left": "Pmmlr_W6INoCuJrnvl6RN",
	"input__addons": "_2Yc8_wg4C0gJNnUpQozegs",
	"input_type_hidden": "_2-j2SP8LNSCs5K3hG5FIbz",
	"input_disabled": "_1GHfLvzQfXRdIP6pDcRWN",
	"input_has-value": "_2TWk625UShOd8vl2V-7qui",
	"input_type_password": "yMB_Lddjz8orhssmQWyn6",
	"input_size_s": "_3aX6pDUojpjrs0bgbhpsE2",
	"input_size_m": "_1Ig3krMrozUDdf3_AQaQRN",
	"input_size_l": "xsMKDEdOsw6V-aNFSmGoB",
	"input_size_xl": "_3ewA7vN9A5qwLS-g9w5t6b",
	"input_view_filled": "_2LdQBUeBleqwaWqTGvDWZk",
	"input_has-left-addons": "PrQ2nB4orOx8OBjN0VZ-2",
	"input_view_filled__sub": "oLeAlarm0ecvhbX1RoyA_",
	"input_invalid": "FVy4Ivw0K34yjyTspFJqg"
};

/***/ }),

/***/ "./node_modules/arui-feather/input/input.js":
/*!**************************************************!*\
  !*** ./node_modules/arui-feather/input/input.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _dec, _dec2, _class, _desc, _value, _class2, _class3, _temp2; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                   * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                   * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* eslint-disable max-len */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _close = __webpack_require__(/*! ../icon/ui/close */ "./node_modules/arui-feather/icon/ui/close/index.js");

var _close2 = _interopRequireDefault(_close);

var _iconButton = __webpack_require__(/*! ../icon-button/icon-button */ "./node_modules/arui-feather/icon-button/icon-button.js");

var _iconButton2 = _interopRequireDefault(_iconButton);

var _maskedInput = __webpack_require__(/*! ../masked-input/masked-input */ "./node_modules/arui-feather/masked-input/masked-input.js");

var _maskedInput2 = _interopRequireDefault(_maskedInput);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

var _scrollTo2 = __webpack_require__(/*! ../lib/scroll-to */ "./node_modules/arui-feather/lib/scroll-to.js");

var _scrollTo3 = _interopRequireDefault(_scrollTo2);

var _vars = __webpack_require__(/*! ../vars */ "./node_modules/arui-feather/vars.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

/**
 * Компонент текстового поля ввода.
 */
var Input = (_dec = (0, _cn2.default)('input', _maskedInput2.default), _dec2 = (0, _performance2.default)(), _dec(_class = _dec2(_class = (_class2 = (_temp2 = _class3 = function (_React$Component) {
    _inherits(Input, _React$Component);

    function Input() {
        var _temp, _this, _ret;

        _classCallCheck(this, Input);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.state = {
            focused: false,
            error: _this.props.error || null,
            value: _this.props.defaultValue || ''
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    /**
     * @type {HTMLSpanElement}
     */


    /**
     * @type {HTMLSpanElement}
     */


    /**
     * @type {HTMLInputElement}
     */


    Input.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
        this.setState({
            error: nextProps.error
        });
    };

    Input.prototype.render = function render(cn, MaskedInput) {
        var _this2 = this;

        var hasAddons = !!this.props.rightAddons || !!this.props.leftAddons;
        var hasLeftAddons = !!this.props.leftAddons;
        var value = this.props.value !== undefined ? this.props.value : this.state.value;
        var focused = this.getFocused();

        return _react2.default.createElement(
            'span',
            {
                className: cn({
                    type: this.props.type,
                    view: this.props.view,
                    disabled: this.props.disabled,
                    focused: focused,
                    size: this.props.size,
                    width: this.props.width,
                    'has-addons': hasAddons,
                    'has-left-addons': hasLeftAddons,
                    'has-clear': !!this.props.clear,
                    'has-icon': !!this.props.icon,
                    'has-label': !!this.props.label,
                    'has-value': !!value,
                    invalid: !!this.state.error
                }),
                ref: function ref(root) {
                    _this2.root = root;
                }
            },
            _react2.default.createElement(
                'span',
                { className: cn('inner') },
                !!this.props.label && _react2.default.createElement(
                    'span',
                    { className: cn('top') },
                    this.props.label
                ),
                this.renderContent(cn, MaskedInput),
                (this.state.error || this.props.hint) && _react2.default.createElement(
                    'span',
                    { className: cn('sub') },
                    this.state.error || this.props.hint
                )
            )
        );
    };

    Input.prototype.renderContent = function renderContent(cn, MaskedInput) {
        var _this3 = this;

        var isMaskedInput = this.props.mask !== undefined;
        var value = this.props.value !== undefined ? this.props.value : this.state.value;

        var inputProps = {
            className: cn('control'),
            type: this.props.type,
            view: this.props.view,
            formNoValidate: this.props.formNoValidate,
            autoComplete: this.getAutoCompleteValue(),
            disabled: this.props.disabled || this.props.disabledAttr,
            maxLength: this.props.maxLength,
            id: this.props.id,
            name: this.props.name,
            value: value,
            tabIndex: this.props.tabIndex,
            placeholder: this.props.placeholder,
            pattern: this.props.pattern,
            ref: function ref(control) {
                _this3.control = control;
            },
            title: this.props.title,
            onChange: this.handleChange,
            onFocus: this.handleFocus,
            onClick: this.handleClick,
            onBlur: this.handleBlur,
            onKeyDown: this.handleKeyDown,
            onKeyUp: this.handleKeyUp,
            onPaste: this.handlePaste,
            onTouchStart: this.handleTouchStart,
            onTouchEnd: this.handleTouchEnd,
            onTouchMove: this.handleTouchMove,
            onTouchCancel: this.handleTouchCancel
        };

        return _react2.default.createElement(
            'span',
            {
                className: cn('box'),
                key: 'input-wrapper',
                ref: function ref(box) {
                    _this3.box = box;
                }
            },
            this.props.leftAddons && _react2.default.createElement(
                'span',
                { className: cn('addons', { left: true }), key: 'left-addons' },
                this.props.leftAddons
            ),
            !isMaskedInput ? _react2.default.createElement('input', inputProps) : _react2.default.createElement(MaskedInput, _extends({}, inputProps, {
                mask: this.props.mask,
                formatCharacters: this.props.maskFormatCharacters,
                onProcessInputEvent: this.props.onProcessMaskInputEvent,
                useWhitespaces: this.props.useWhitespacesInMask
            })),
            this.props.clear && value && _react2.default.createElement(
                _iconButton2.default,
                {
                    className: cn('clear'),
                    size: this.props.size,
                    tabIndex: -1,
                    onClick: this.handleClearClick
                },
                _react2.default.createElement(_close2.default, {
                    size: this.props.size
                })
            ),
            this.props.icon && _react2.default.createElement(
                'span',
                { className: cn('icon') },
                this.props.icon
            ),
            this.props.rightAddons && _react2.default.createElement(
                'span',
                { className: cn('addons', { right: true }), key: 'right-addons' },
                this.props.rightAddons
            )
        );
    };

    Input.prototype.handleFocus = function handleFocus(event) {
        this.setState({ focused: true });
        this.enableMouseWheel();
        this.resetError();

        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    };

    Input.prototype.handleClick = function handleClick(event) {
        if (this.props.onClick) {
            this.props.onClick(event);
        }
    };

    Input.prototype.handleBlur = function handleBlur(event) {
        this.setState({ focused: false });
        this.disableMouseWheel();

        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    };

    Input.prototype.handleChange = function handleChange(event) {
        this.changeValue(event.target.value);
    };

    Input.prototype.handleClearClick = function handleClearClick(event) {
        this.changeValue('');

        if (this.props.onClearClick) {
            this.props.onClearClick(event);
        }

        this.focus();
    };

    Input.prototype.handleKeyDown = function handleKeyDown(event) {
        if (this.props.onKeyDown) {
            this.props.onKeyDown(event);
        }
    };

    Input.prototype.handleKeyUp = function handleKeyUp(event) {
        if (this.props.onKeyUp) {
            this.props.onKeyUp(event);
        }
    };

    Input.prototype.handlePaste = function handlePaste(event) {
        if (this.props.onPaste) {
            this.props.onPaste(event);
        }
    };

    Input.prototype.handleTouchStart = function handleTouchStart(event) {
        if (this.props.onTouchStart) {
            this.props.onTouchStart(event);
        }
    };

    Input.prototype.handleTouchEnd = function handleTouchEnd(event) {
        if (this.props.onTouchEnd) {
            this.props.onTouchEnd(event);
        }
    };

    Input.prototype.handleTouchMove = function handleTouchMove(event) {
        if (this.props.onTouchMove) {
            this.props.onTouchMove(event);
        }
    };

    Input.prototype.handleTouchCancel = function handleTouchCancel(event) {
        if (this.props.onTouchCancel) {
            this.props.onTouchCancel(event);
        }
    };

    Input.prototype.getAutoCompleteValue = function getAutoCompleteValue() {
        if (typeof this.props.autocomplete === 'string') {
            return this.props.autocomplete;
        }

        return this.props.autocomplete === false ? 'off' : 'on';
    };

    /**
     * Разблокирует возможность скролла в поле ввода
     *
     * @public
     * @returns {void}
     */


    Input.prototype.enableMouseWheel = function enableMouseWheel() {
        var input = this.control instanceof _maskedInput2.default ? this.control.input : this.control;

        if (input) {
            input.onwheel = function () {
                return true;
            };
        }
    };

    /**
     * Блокирует возможность скролла в поле ввода
     *
     * @public
     * @returns {void}
     */


    Input.prototype.disableMouseWheel = function disableMouseWheel() {
        var input = this.control instanceof _maskedInput2.default ? this.control.getControl() : this.control;

        if (input) {
            input.onwheel = function () {
                return false;
            };
        }
    };

    /**
     * Возвращает корневой `HTMLElement` компонента.
     *
     * @public
     * @returns {HTMLElement}
     */


    Input.prototype.getNode = function getNode() {
        return this.root;
    };

    /**
     * Возвращает ссылку на инстанс контейнера для контрола.
     *
     * @public
     * @returns {HTMLSpanElement}
     */


    Input.prototype.getBoxNode = function getBoxNode() {
        return this.box;
    };

    /**
     * Возвращает ссылку на HTMLElement инпута.
     *
     * @public
     * @returns {HTMLInputElement}
     */


    Input.prototype.getControl = function getControl() {
        if (this.props.mask !== undefined) {
            return this.control.getControl();
        }

        return this.control;
    };

    /**
     * Возвращает ссылку на инстанс MaskedInput.
     * Если маска не была установлена, возвращает null.
     *
     * @public
     * @returns {MaskedInput|null}
     */


    Input.prototype.getMaskedInputInstance = function getMaskedInputInstance() {
        if (this.props.mask !== undefined) {
            return this.control;
        }

        return null;
    };

    /**
     * Устанавливает фокус на поле ввода.
     *
     * @public
     */


    Input.prototype.focus = function focus() {
        this.control.focus();
        this.setSelectionRange(this.getControl().value.length);
    };

    /**
     * Убирает фокус с поля ввода.
     *
     * @public
     */


    Input.prototype.blur = function blur() {
        if (document.activeElement) {
            document.activeElement.blur();
        }
    };

    /**
     * Скроллит страницу до поля ввода.
     *
     * @public
     */


    Input.prototype.scrollTo = function scrollTo() {
        (0, _scrollTo3.default)({
            targetY: this.root.getBoundingClientRect().top + window.pageYOffset - _vars.SCROLL_TO_CORRECTION
        });
    };

    /**
     * Устанавливает начальное и конечное положение выделения текста в элементе.
     *
     * @public
     * @param {Number} start=0 Индекс первого выделенного символа.
     * @param {Number} end=value.length Индекс символа после последнего выделенного символа.
     */


    Input.prototype.setSelectionRange = function setSelectionRange() {
        var start = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var end = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.getControl().value.length;

        if (this.props.type !== 'email') {
            this.getControl().setSelectionRange(start, end);
        }
    };

    /**
     * Изменяет текущение значение поля ввода и генерирует событие об этом.
     *
     * @param {String} value Новое значение
     */


    Input.prototype.changeValue = function changeValue(value) {
        if (this.props.value === undefined) {
            this.setState({ value: value });
        }

        if (this.props.onChange) {
            this.props.onChange(value);
        }
    };

    /**
     * Возвращает состояние фокуса.
     *
     * @returns {Boolean}
     */


    Input.prototype.getFocused = function getFocused() {
        return this.props.focused !== undefined ? this.props.focused : this.state.focused;
    };

    /**
     * Сбрасывает состояние ошибки.
     *
     * @returns {void}
     */


    Input.prototype.resetError = function resetError() {
        if (this.props.resetError) {
            this.setState({
                error: null
            });
        }
    };

    return Input;
}(_react2.default.Component), _class3.defaultProps = {
    formNoValidate: false,
    size: 'm',
    type: 'text',
    view: 'default',
    resetError: true
}, _temp2), (_applyDecoratedDescriptor(_class2.prototype, 'handleFocus', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleFocus'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleClick'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleBlur', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleBlur'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleChange', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleChange'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleClearClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleClearClick'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleKeyDown', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleKeyDown'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleKeyUp', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleKeyUp'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handlePaste', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handlePaste'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleTouchStart', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleTouchStart'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleTouchEnd', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleTouchEnd'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleTouchMove', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleTouchMove'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleTouchCancel', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleTouchCancel'), _class2.prototype)), _class2)) || _class) || _class);
Input.propTypes =  true ? {
    /**
     * Тип поля.
     * Внимание, тип 'number' не умеет работать с масками, в том числе с 'selectionStart' и 'selectionEnd'.
     * Подробнее: <a href="http://w3c.github.io/html/sec-forms.html#does-not-apply" target="_blank">http://w3c.github.io/html/sec-forms.html#does-not-apply</a>
     */
    type: _propTypes2.default.oneOf(['number', 'card', 'email', 'file', 'hidden', 'money', 'password', 'tel', 'text']),
    /** Тип инпута (filled только на белом фоне в размере m) */
    view: _propTypes2.default.oneOf(['default', 'filled']),
    /** Управление возможностью компонента занимать всю ширину родителя */
    width: _propTypes2.default.oneOf(['default', 'available']),
    /**
     * Управление автозаполнением компонента. В случае передачи `true` или `false` подставляет `on` или `off`.
     * Строка подставляется как есть.
     */
    autocomplete: _propTypes2.default.oneOfType([_propTypes2.default.bool, _propTypes2.default.string]),
    /** Управление возможностью изменения атрибута компонента, установка соответствующего класса-модификатора для оформления */
    disabled: _propTypes2.default.bool,
    /** Управление возможностью изменения атрибута компонента (без установки класса-модификатора для оформления) */
    disabledAttr: _propTypes2.default.bool,
    /** Управление возможностью изменения класса-модификатора компонента */
    focused: _propTypes2.default.bool,
    /** Максимальное число символов */
    maxLength: _propTypes2.default.number,
    /** Иконка компонента */
    icon: _propTypes2.default.node,
    /** Управление наличием крестика, сбрасывающего значение 'value' */
    clear: _propTypes2.default.bool,
    /** Уникальный идентификатор блока */
    id: _propTypes2.default.string,
    /** Уникальное имя блока */
    name: _propTypes2.default.string,
    /** Содержимое поля ввода */
    value: _propTypes2.default.string,
    /** Содержимое поля ввода, указанное по умолчанию */
    defaultValue: _propTypes2.default.string,
    /** Последовательность перехода между контролами при нажатии на Tab */
    tabIndex: _propTypes2.default.number,
    /** Определяет маску для ввода значений. <a href="https://github.com/insin/inputmask-core#pattern" target="_blank">Шаблон маски</a> */
    mask: _propTypes2.default.string,
    /** Позволяет использовать пробелы в маске */
    useWhitespacesInMask: _propTypes2.default.bool,
    /** Кастомные форматтеры символов маски, использует формат formatCharacters из `inputmask-core` */
    maskFormatCharacters: _propTypes2.default.objectOf(_propTypes2.default.shape({
        validate: _propTypes2.default.func.isRequired,
        transform: _propTypes2.default.func
    })),
    /** Стандартное ствойство HTMLInputElement 'pattern'. Может быть использовано для показа корректной клавиатуры на мобильных устройствах. */
    pattern: _propTypes2.default.string,
    /** Управление встроенной проверкой данных введённых пользователем в поле на корректность */
    formNoValidate: _propTypes2.default.bool,
    /** Добавление дополнительных элементов к инпуту слева */
    leftAddons: _propTypes2.default.node,
    /** Добавление дополнительных элементов к инпуту справа */
    rightAddons: _propTypes2.default.node,
    /** Лейбл для поля */
    label: _propTypes2.default.node,
    /** Подсказка в поле */
    placeholder: _propTypes2.default.string,
    /** Подсказка под полем */
    hint: _propTypes2.default.node,
    /** Отображение ошибки */
    error: _propTypes2.default.node,
    /** Сброс ошибки при установке фокуса */
    resetError: _propTypes2.default.bool,
    /** Размер компонента */
    size: _propTypes2.default.oneOf(['s', 'm', 'l', 'xl']),
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /** Тултип, который появляется при наведении  */
    title: _propTypes2.default.string,
    /**
     * Обработчик изменения значения 'value'
     * @param {string} value
     */
    onChange: _propTypes2.default.func,
    /**
     * Обработчик фокуса поля
     * @param {React.FocusEvent} event
     */
    onFocus: _propTypes2.default.func,
    /**
     * Обработчик клика по полю
     * @param {React.MouseEvent} event
     */
    onClick: _propTypes2.default.func,
    /**
     * Обработчик снятия фокуса с поля
     * @param {React.FocusEvent} event
     */
    onBlur: _propTypes2.default.func,
    /**
     * Обработчик клика по крестику сбрасываещему значение 'value'
     * @param {React.MouseEvent} event
     */
    onClearClick: _propTypes2.default.func,
    /**
     * Обработчик события нажатия на клавишу клавиатуры в момент, когда фокус находится на компоненте
     * @param {React.KeyboardEvent} event
     */
    onKeyDown: _propTypes2.default.func,
    /**
     * Обработчик события отжатия на клавишу клавиатуры в момент, когда фокус находится на компоненте
     * @param {React.KeyboardEvent} event
     */
    onKeyUp: _propTypes2.default.func,
    /**
     * Обработчик события вставки текста в поле
     * @param {React.ClipboardEvent} event
     */
    onPaste: _propTypes2.default.func,
    /**
     * Обработчик события касания по полю
     * @param {React.TouchEvent} event
     */
    onTouchStart: _propTypes2.default.func,
    /**
     * Обработчик события прекращения касания по полю
     * @param {React.TouchEvent} event
     */
    onTouchEnd: _propTypes2.default.func,
    /**
     * Обработчик события перемещения при касании по полю
     * @param {React.TouchEvent} event
     */
    onTouchMove: _propTypes2.default.func,
    /**
     * Обработчик события прерывания касания по полю
     * @param {React.TouchEvent} event
     */
    onTouchCancel: _propTypes2.default.func,
    /**
     * Обработчик, вызываемый перед началом ввода в маскированное поле
     * @param {React.ChangeEvent} event
     */
    onProcessMaskInputEvent: _propTypes2.default.func,
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = Input;
//# sourceMappingURL=input.js.map


/***/ }),

/***/ "./node_modules/arui-feather/input/input_theme_alfa-on-color.css":
/*!***********************************************************************!*\
  !*** ./node_modules/arui-feather/input/input_theme_alfa-on-color.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"input_theme_alfa-on-color": "_27YJgaVOr8b4uXPEtspBcF",
	"input__box": "_194GO9TtytVx4C6dGgzkBK",
	"input__control": "_2P4hI4Ql5sbKDvhPwM2LyW",
	"input__top": "_1LyCUjxBW-vHIhJVtgTXK3",
	"input__sub": "lKb3ZNmZigadGmG4EGXDS",
	"input_has-label": "zbrF2GGgLWyVM-JjllrdB",
	"input_focused": "vAgd7_sVBFTl0FB6R030R",
	"input_has-icon": "_3UvC-wixDQ7s-fOOHgMH4I",
	"input__icon": "_2BjSZhInxvndb1NGFhnv34",
	"input_disabled": "_2b65EpzwH57D5tJIoM15qE",
	"input_invalid": "_16biL9tqdwYcUSNzIFjWgX"
};

/***/ }),

/***/ "./node_modules/arui-feather/input/input_theme_alfa-on-white.css":
/*!***********************************************************************!*\
  !*** ./node_modules/arui-feather/input/input_theme_alfa-on-white.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"input_theme_alfa-on-white": "KWPLZmpC4Ux1ql1jykx-k",
	"input__box": "_8Rjoo4-Y2n3lNuRrJeQ79",
	"input__control": "_1syMqZHt_H2-1K6kq90Ts2",
	"input__top": "_2J57LLCA4mPlCSpbzUWjJb",
	"input__sub": "_3UNKtr1XotvErdLxT6gEML",
	"input_has-label": "RY37XF3MliRb-P7L69O-T",
	"input_focused": "_1qQtIgmFpkFCeY2OGq80tr",
	"input_has-icon": "vXkTDfafuUnhDDAaqL9nI",
	"input__icon": "vdIiBiN904QReAsjGoVRS",
	"input_disabled": "_1u1AF7cyynn6PKrFnpxqJ4",
	"input_invalid": "_8cEkSsurHR3NcGaMRsBAB"
};

/***/ }),

/***/ "./node_modules/arui-feather/lib/easings.js":
/*!**************************************************!*\
  !*** ./node_modules/arui-feather/lib/easings.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _bezierEasing = __webpack_require__(/*! bezier-easing */ "bezier-easing");

var _bezierEasing2 = _interopRequireDefault(_bezierEasing);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    easeInSine: (0, _bezierEasing2.default)(0.47, 0, 0.745, 0.715),
    easeOutSine: (0, _bezierEasing2.default)(0.39, 0.575, 0.565, 1),
    easeInOutSine: (0, _bezierEasing2.default)(0.445, 0.05, 0.55, 0.95),
    easeInQuad: (0, _bezierEasing2.default)(0.55, 0.085, 0.68, 0.53),
    easeOutQuad: (0, _bezierEasing2.default)(0.25, 0.46, 0.45, 0.94),
    easeInOutQuad: (0, _bezierEasing2.default)(0.455, 0.03, 0.515, 0.955),
    easeInCubic: (0, _bezierEasing2.default)(0.55, 0.055, 0.675, 0.19),
    easeOutCubic: (0, _bezierEasing2.default)(0.215, 0.61, 0.355, 1),
    easeInOutCubic: (0, _bezierEasing2.default)(0.645, 0.045, 0.355, 1),
    easeInQuart: (0, _bezierEasing2.default)(0.895, 0.03, 0.685, 0.22),
    easeOutQuart: (0, _bezierEasing2.default)(0.165, 0.84, 0.44, 1),
    easeInOutQuart: (0, _bezierEasing2.default)(0.77, 0, 0.175, 1),
    easeInQuint: (0, _bezierEasing2.default)(0.755, 0.05, 0.855, 0.06),
    easeOutQuint: (0, _bezierEasing2.default)(0.23, 1, 0.32, 1),
    easeInOutQuint: (0, _bezierEasing2.default)(0.86, 0, 0.07, 1),
    easeInExpo: (0, _bezierEasing2.default)(0.95, 0.05, 0.795, 0.035),
    easeOutExpo: (0, _bezierEasing2.default)(0.19, 1, 0.22, 1),
    easeInOutExpo: (0, _bezierEasing2.default)(1, 0, 0, 1),
    easeInCirc: (0, _bezierEasing2.default)(0.6, 0.04, 0.98, 0.335),
    easeOutCirc: (0, _bezierEasing2.default)(0.075, 0.82, 0.165, 1),
    easeInOutCirc: (0, _bezierEasing2.default)(0.785, 0.135, 0.15, 0.86),
    easeInBack: (0, _bezierEasing2.default)(0.6, -0.28, 0.735, 0.045),
    easeOutBack: (0, _bezierEasing2.default)(0.175, 0.885, 0.32, 1.275),
    easeInOutBack: (0, _bezierEasing2.default)(0.68, -0.55, 0.265, 1.55)
}; /* This Source Code Form is subject to the terms of the Mozilla Public
    * License, v. 2.0. If a copy of the MPL was not distributed with this
    * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=easings.js.map


/***/ }),

/***/ "./node_modules/arui-feather/lib/keyboard-code.js":
/*!********************************************************!*\
  !*** ./node_modules/arui-feather/lib/keyboard-code.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

exports.default = {
    SPACE: 32,
    BACKSPACE: 8,
    TAB: 9,
    ENTER: 13,
    SHIFT: 16,
    CTRL: 17,
    ALT: 18,
    PAUSE_BREAK: 19,
    CAPS_LOCK: 20,
    ESCAPE: 27,
    PAGE_UP: 33,
    PAGE_DOWN: 34,
    END: 35,
    HOME: 36,
    LEFT_ARROW: 37,
    UP_ARROW: 38,
    RIGHT_ARROW: 39,
    DOWN_ARROW: 40,
    INSERT: 45,
    DELETE: 46,
    NUMBER_0: 48,
    NUMBER_1: 49,
    NUMBER_2: 50,
    NUMBER_3: 51,
    NUMBER_4: 52,
    NUMBER_5: 53,
    NUMBER_6: 54,
    NUMBER_7: 55,
    NUMBER_8: 56,
    NUMBER_9: 57,
    LETTER_A: 65,
    LETTER_B: 66,
    LETTER_C: 67,
    LETTER_D: 68,
    LETTER_E: 69,
    LETTER_F: 70,
    LETTER_G: 71,
    LETTER_H: 72,
    LETTER_I: 73,
    LETTER_J: 74,
    LETTER_K: 75,
    LETTER_L: 76,
    LETTER_M: 77,
    LETTER_N: 78,
    LETTER_O: 79,
    LETTER_P: 80,
    LETTER_Q: 81,
    LETTER_R: 82,
    LETTER_S: 83,
    LETTER_T: 84,
    LETTER_U: 85,
    LETTER_V: 86,
    LETTER_W: 87,
    LETTER_X: 88,
    LETTER_Y: 89,
    LETTER_Z: 90,
    LEFT_WINDOW_KEY: 91,
    RIGHT_WINDOW_KEY: 92,
    SELECT_KEY: 93,
    NUMPAD_0: 96,
    NUMPAD_1: 97,
    NUMPAD_2: 98,
    NUMPAD_3: 99,
    NUMPAD_4: 100,
    NUMPAD_5: 101,
    NUMPAD_6: 102,
    NUMPAD_7: 103,
    NUMPAD_8: 104,
    NUMPAD_9: 105,
    MULTIPLY: 106,
    ADD: 107,
    SUBTRACT: 109,
    DECIMALPOINT: 110,
    DIVIDE: 111,
    F1: 112,
    F2: 113,
    F3: 114,
    F4: 115,
    F5: 116,
    F6: 117,
    F7: 118,
    F8: 119,
    F9: 120,
    F10: 121,
    F11: 122,
    F12: 123,
    NUM_LOCK: 144,
    SCROLL_LOCK: 145,
    SEMI_COLON: 186,
    EQUAL_SIGN: 187,
    COMMA: 188,
    DASH: 189,
    PERIOD: 190,
    FORWARD_SLASH: 191,
    GRAVE_ACCENT: 192,
    OPEN_BRACKET: 219,
    BACK_SLASH: 220,
    CLOSE_BRACKET: 221,
    SINGLE_QUOTE: 222
};
//# sourceMappingURL=keyboard-code.js.map


/***/ }),

/***/ "./node_modules/arui-feather/lib/match-media.js":
/*!******************************************************!*\
  !*** ./node_modules/arui-feather/lib/match-media.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.getMatchMedia = getMatchMedia;
exports.releaseMatchMedia = releaseMatchMedia;

var _mq = __webpack_require__(/*! ../mq/mq.json */ "./node_modules/arui-feather/mq/mq.json");

var _mq2 = _interopRequireDefault(_mq);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pool = {};
var refCounters = {};

/**
 * Возвращает MediaQueryList для заданного media-выражения.
 *
 * @param {String} queryProp media выражение или кастомные запросы из `src/mq/mq.json`, например `--small`.
 * @returns {MediaQueryList}
 */
function getMatchMedia(queryProp) {
    var query = _mq2.default[queryProp] || queryProp;

    if (!pool[query]) {
        pool[query] = window.matchMedia(query);
        refCounters[query] = 1;
    } else {
        refCounters[query] += 1;
    }

    return pool[query];
}

/**
 * Удаляет MediaQueryList.
 *
 * @param {String} queryProp media выражение
 */
function releaseMatchMedia(queryProp) {
    var query = _mq2.default[queryProp] || queryProp;

    refCounters[query] -= 1;

    if (pool[query] && refCounters[query] === 0) {
        delete pool[query];
        delete refCounters[query];
    }
}
//# sourceMappingURL=match-media.js.map


/***/ }),

/***/ "./node_modules/arui-feather/lib/prop-types.js":
/*!*****************************************************!*\
  !*** ./node_modules/arui-feather/lib/prop-types.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.deprecated = deprecated;
exports.deprecatedType = deprecatedType;
exports.createMappingPropValidator = createMappingPropValidator;
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Добавляет к type-checker для propType валидации метод isRequired.
 *
 * @param {Function} validate Оригинальный метод для валидации
 * @returns {Function}
 */
function createChainableTypeChecker(validate) {
    function checkType(isRequired, props, propName, componentName, location) {
        componentName = componentName || '';
        if (props[propName] === null || props[propName] === undefined) {
            if (isRequired) {
                return new Error('Required prop `' + propName + '` was not specified in `' + componentName + '`.');
            }
            return null;
        }

        return validate(props, propName, componentName, location);
    }

    var chainedCheckType = checkType.bind(null, false);
    chainedCheckType.isRequired = checkType.bind(null, true);

    return chainedCheckType;
}

/**
 * Проверяет, является ли заданный prop инстансом HTMLElement.
 *
 * @param {Object} props Пропы компонента
 * @param {String} propName Имя пропса для валидации
 * @param {String} componentName Имя компонента
 * @returns {Error|null}
 */
function propTypeIsHtmlElement(props, propName, componentName) {
    if (!(props[propName] instanceof (typeof HTMLElement === 'undefined' ? {} : HTMLElement))) {
        return new Error('Invalid prop `' + propName + '` supplied to `' + componentName + '`.\n            Expected valid HTMLElement object, ' + _typeof(props[propName]) + ' given.');
    }
    return null;
}

/**
 * Враппер для prop-type валидаторов, позволяющий помечать prop как устаревший, сохраняя проверку типа.
 *
 * @param {Function} propType Оригинальный propType валидатор.
 * @param {String} message Дополнительное сообщение.
 * @returns {function(...[*]): *}
 */
function deprecated(propType, message) {
    var warned = false;
    return function () {
        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        var props = args[0],
            propName = args[1],
            componentName = args[2];

        var prop = props[propName];
        if (prop !== undefined && prop !== null && !warned) {
            warned = true;
            if (true) {
                // eslint-disable-next-line no-console
                console.warn('Property \'' + propName + '\' of \'' + componentName + '\' is deprecated. ' + message);
            }
        }
        return propType.call.apply(propType, [this].concat(args));
    };
}

/**
 * Враппер для prop-type валидатора, позволяющий пометить один из типов как устаревший.
 * В случае, если заданный prop будет соответствовать старому валидатору, но не будет
 * соответствовать новому - пользователь будет об этом предупрежден.
 *
 * @param {Function} oldType Валидатор для старого типа.
 * @param {Function} newType Валидатор для нового типа.
 * @param {String} message Дополнительное сообщение
 * @returns {Function}
 */
function deprecatedType(oldType, newType, message) {
    var warned = false;

    return function () {
        for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
            args[_key2] = arguments[_key2];
        }

        var propName = args[1],
            componentName = args[2];

        var oldResult = oldType.call.apply(oldType, [this].concat(args));
        var newResult = newType.call.apply(newType, [this].concat(args));
        if ( true && !oldResult && !warned && newResult) {
            warned = true;
            // eslint-disable-next-line no-console
            console.warn('Given type of \'' + propName + '\' of \'' + componentName + '\' is deprecated. ' + message);
        }
        return newResult;
    };
}

var HtmlElement = exports.HtmlElement = createChainableTypeChecker(propTypeIsHtmlElement);

function createMappingPropValidator(validationMapping, controllingPropName) {
    return function validateProp(props, propName, componentName) {
        var controllingPropValue = props[controllingPropName];
        var controlledPropValue = props[propName];

        var propsDefined = controllingPropValue && controlledPropValue;
        var availableOptions = validationMapping[controllingPropValue];
        if (!propsDefined || !Array.isArray(availableOptions)) {
            return null;
        }

        var isValidProp = availableOptions.indexOf(props[propName]) !== -1;
        if (!isValidProp) {
            return new Error('Invalid prop \'' + propName + '\' supplied to ' + componentName + '. \n                Expected one of ' + availableOptions + ' for prop \'' + controllingPropName + '\' equal to ' + controllingPropValue);
        }

        return null;
    };
}
//# sourceMappingURL=prop-types.js.map


/***/ }),

/***/ "./node_modules/arui-feather/lib/scroll-to.js":
/*!****************************************************!*\
  !*** ./node_modules/arui-feather/lib/scroll-to.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = scrollTo;

var _easings = __webpack_require__(/*! ./easings */ "./node_modules/arui-feather/lib/easings.js");

var _easings2 = _interopRequireDefault(_easings);

var _vars = __webpack_require__(/*! ../vars */ "./node_modules/arui-feather/vars.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Скроллит по элементу или странице.
 * В настоящее время доступно перемещение только по оси Y.
 *
 * @param {Options} options Список переданных опций
 * @param {Number} options.targetY Цель по оси Y
 * @param {HTMLElement} [options.container] Элемент в котором скроллим
 * @param {Number} [options.duration=0] Продолжительность анимации в миллесекундах
 * @param {String} [options.easing='easeInOutSine'] Название функции плавности для анимации
 * @returns {Promise}
 */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

function scrollTo(options) {
    var targetY = options.targetY,
        container = options.container,
        _options$duration = options.duration,
        duration = _options$duration === undefined ? 0 : _options$duration,
        _options$easing = options.easing,
        easing = _options$easing === undefined ? _vars.SCROLL_TO_EASING : _options$easing;


    var scrollY = container ? container.scrollTop : window.pageYOffset;
    var startTime = window.performance.now();

    if (duration < 0) {
        throw new Error('Incorrect duration in options');
    }

    if (!_easings2.default[easing]) {
        throw new Error('Incorrect easing in options');
    }

    var easingFunc = _easings2.default[easing];

    return new Promise(function (resolve) {
        function scrollToTarget(y) {
            if (container) {
                container.scrollTop = y;
            } else {
                window.scrollTo(0, y);
            }
        }

        function loop(timestamp) {
            var currentTime = Math.abs(timestamp - startTime);
            var t = currentTime / duration;
            var val = easingFunc(t);
            var currentTargetY = scrollY + (targetY - scrollY) * val;

            if (t < 1) {
                window.requestAnimationFrame(loop);
                scrollToTarget(currentTargetY);
            } else {
                scrollToTarget(targetY);
                resolve();
            }
        }

        if (duration === 0) {
            scrollToTarget(targetY);
            resolve();
        } else {
            loop(window.performance.now());
        }
    });
}
//# sourceMappingURL=scroll-to.js.map


/***/ }),

/***/ "./node_modules/arui-feather/lib/window.js":
/*!*************************************************!*\
  !*** ./node_modules/arui-feather/lib/window.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.isEventOusideBounds = undefined;
exports.isNodeOutsideElement = isNodeOutsideElement;
exports.isEventOutsideClientBounds = isEventOutsideClientBounds;

var _deprecatedDecorator = __webpack_require__(/*! deprecated-decorator */ "deprecated-decorator");

var _deprecatedDecorator2 = _interopRequireDefault(_deprecatedDecorator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Check that mouse was outside element.
 *
 * @param {Object} event - MouseEvent
 * @param {Element} element - Element to check bounds
 * @returns {Boolean}
 */
var isEventOusideBounds = exports.isEventOusideBounds = (0, _deprecatedDecorator2.default)({
  name: 'isEventOusideBounds',
  alternative: 'isEventOutsideClientBounds',
  version: '12.0.1'
}, function isEventOusideBounds(event, element) {
  var rect = element.getBoundingClientRect();
  return event.pageX < rect.left || event.pageX > rect.right || event.pageY < rect.top || event.pageY > rect.bottom;
});

/**
 * Check that node is outside given element.
 *
 * @param {Node} node Node to search
 * @param {Node} element Element that shouldn't contain node
 * @returns {Boolean}
 */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
function isNodeOutsideElement(node, element) {
  return !(element.contains(node) || element === node);
}

/**
 * Check that mouse was outside element.
 *
 * @param {Object} event - MouseEvent
 * @param {React.Element} element - Element to check bounds
 * @returns {Boolean}
 */
function isEventOutsideClientBounds(event, element) {
  var rect = element.getBoundingClientRect();
  return event.clientX < rect.left || event.clientX > rect.right || event.clientY < rect.top || event.clientY > rect.bottom;
}
//# sourceMappingURL=window.js.map


/***/ }),

/***/ "./node_modules/arui-feather/link/index.js":
/*!*************************************************!*\
  !*** ./node_modules/arui-feather/link/index.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./link.css */ "./node_modules/arui-feather/link/link.css");

var _link = __webpack_require__(/*! ./link */ "./node_modules/arui-feather/link/link.js");

var _link2 = _interopRequireDefault(_link);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

exports.default = _link2.default;
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/link/link.css":
/*!*************************************************!*\
  !*** ./node_modules/arui-feather/link/link.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"link": "_2ftxzjDA76_zpsv3J6caDe",
	"link_flex": "CRjKb_FEXC8rYTSK0U4Ws",
	"link__text-container": "vqbIatLzF6gPtHDyy19aO",
	"link__text": "_24KMjc0TFi7woawIgf5rJB",
	"link__icon": "_3_EYfP5scqbpWRTDLtR7j9",
	"link__icon_left": "_39RAOsnC-riIcyFSm-M61r",
	"link_size_xs": "_3qjVzvMvCvJzRUAZkhnuow",
	"link_size_s": "_3nkbDXgwsDFJBooIaS5uZ3",
	"link_size_m": "_3donFNkNP1odJwYr3puErv",
	"link_size_l": "_1Ardxt3kKEqtuxPnSVkf5j",
	"link_size_xl": "_16qbSsXXvS4X3sgo-xjdF_",
	"link_pseudo": "_3v5TS1Wk532uu58kmUavrY",
	"link_disabled": "_2m5bxmQJSUZi5RzRb9ebON",
	"link_checked": "_1bd_OSIGRCW8X8xPz59MTg",
	"link_theme_alfa-on-color": "_4c7BwGi6fIdTYFeyfzPro",
	"link_theme_alfa-on-white": "_37z7P0lefVbTDT837rndtj"
};

/***/ }),

/***/ "./node_modules/arui-feather/link/link.js":
/*!************************************************!*\
  !*** ./node_modules/arui-feather/link/link.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _dec, _dec2, _class, _desc, _value, _class2, _class3, _temp2; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                   * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                   * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

/**
 * Компонент ссылки.
 */
var Link = (_dec = (0, _cn2.default)('link'), _dec2 = (0, _performance2.default)(), _dec(_class = _dec2(_class = (_class2 = (_temp2 = _class3 = function (_React$Component) {
    _inherits(Link, _React$Component);

    function Link() {
        var _temp, _this, _ret;

        _classCallCheck(this, Link);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.state = {
            hovered: false,
            focused: false
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    Link.prototype.render = function render(cn) {
        var _this2 = this;

        var linkElement = this.props.checked || this.props.disabled ? 'span' : 'a';
        var iconPosition = this.props.iconPosition;


        var linkProps = {
            ref: function ref(root) {
                _this2.root = root;
            },
            download: this.props.download,
            className: cn({
                disabled: this.props.disabled,
                checked: this.props.checked,
                pseudo: this.props.pseudo,
                size: this.props.size,
                focused: this.state.focused,
                hovered: this.state.hovered,
                flex: this.props.icon && iconPosition === 'left'
            }),
            id: this.props.id,
            tabIndex: this.props.tabIndex,
            onClick: this.handleClick,
            onFocus: this.handleFocus,
            onBlur: this.handleBlur,
            onMouseEnter: this.handleMouseEnter,
            onMouseLeave: this.handleMouseLeave,
            'data-test-id': this.props['data-test-id']
        };

        if (this.props.target === '_blank') {
            linkProps.rel = 'noreferrer noopener';
        }

        if (!this.props.checked && !this.props.disabled) {
            linkProps.href = this.props.url;
            linkProps.target = this.props.target;
        }

        var linkContent = [this.props.children];
        var iconTemplate = this.props.icon && _react2.default.createElement(
            'span',
            { key: 'icon', className: cn('icon', { left: iconPosition === 'left' }) },
            this.props.icon
        );

        var textTemplate = this.props.text && _react2.default.createElement(
            'span',
            { key: 'text', className: cn('text-container') },
            _react2.default.createElement(
                'span',
                { className: cn('text') },
                this.props.text
            )
        );

        if (iconPosition === 'left') {
            linkContent.push(iconTemplate, textTemplate);
        }
        if (iconPosition === 'right') {
            linkContent.push(textTemplate, iconTemplate);
        }

        return _react2.default.createElement(linkElement, linkProps, linkContent);
    };

    Link.prototype.handleClick = function handleClick(event) {
        if (this.props.pseudo) {
            event.preventDefault();
        }
        if (!this.props.disabled && this.props.onClick) {
            this.props.onClick(event);
        }

        if (this.props.disabled && this.props.onDisabledClick) {
            this.props.onDisabledClick(event);
        }
    };

    Link.prototype.handleFocus = function handleFocus(event) {
        this.setState({ focused: true });

        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    };

    Link.prototype.handleBlur = function handleBlur(event) {
        this.setState({ focused: false });

        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    };

    Link.prototype.handleMouseEnter = function handleMouseEnter(event) {
        this.setState({ hovered: true });

        if (this.props.onMouseEnter) {
            this.props.onMouseEnter(event);
        }
    };

    Link.prototype.handleMouseLeave = function handleMouseLeave(event) {
        this.setState({ hovered: false });

        if (this.props.onMouseLeave) {
            this.props.onMouseLeave(event);
        }
    };

    /**
     * Возвращает корневой `HTMLElement` компонента.
     *
     * @public
     * @returns {HTMLElement}
     */


    Link.prototype.getNode = function getNode() {
        return this.root;
    };

    /**
     * Ставит фокус на ссылку.
     *
     * @public
     */


    Link.prototype.focus = function focus() {
        this.root.focus();
    };

    /**
     * Убирает фокус с ссылки.
     *
     * @public
     */


    Link.prototype.blur = function blur() {
        if (document.activeElement) {
            document.activeElement.blur();
        }
    };

    return Link;
}(_react2.default.Component), _class3.defaultProps = {
    iconPosition: 'left',
    size: 'm',
    url: '#',
    tabIndex: 0,
    disabled: false,
    checked: false,
    pseudo: false
}, _temp2), (_applyDecoratedDescriptor(_class2.prototype, 'handleClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleClick'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleFocus', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleFocus'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleBlur', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleBlur'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseEnter', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseEnter'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseLeave', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseLeave'), _class2.prototype)), _class2)) || _class) || _class);
Link.propTypes =  true ? {
    /** Иконка ссылки */
    icon: _propTypes2.default.node,
    /** Позиционирование иконки в ссылке */
    iconPosition: _propTypes2.default.oneOf(['left', 'right']),
    /** Текст ссылки */
    text: _propTypes2.default.node,
    /** href ссылки */
    url: _propTypes2.default.string,
    /** Указание на загрузку, вместо открытия и указание имени файла  */
    download: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.bool]),
    /** target ссылки */
    target: _propTypes2.default.oneOf(['_self', '_blank', '_parent', '_top']),
    /** Последовательность перехода между контролами при нажатии на Tab */
    tabIndex: _propTypes2.default.number,
    /** Управление возможностью клика по ссылке */
    disabled: _propTypes2.default.bool,
    /** Управление состоянием ссылки выбран/не выбран */
    checked: _propTypes2.default.bool,
    /** Псевдо-ссылка (border-bottom: dashed) */
    pseudo: _propTypes2.default.bool,
    /** Размер компонента */
    size: _propTypes2.default.oneOf(['xs', 's', 'm', 'l', 'xl']),
    /** Дочерние элементы `Link` */
    children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]),
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /** Идентификатор компонента в DOM */
    id: _propTypes2.default.string,
    /**
     * Обработчик клика но ссылке
     * @param {React.MouseEvent} event
     */
    onClick: _propTypes2.default.func,
    /**
     * Обработчик фокуса компонента
     * @param {React.FocusEvent} event
     */
    onFocus: _propTypes2.default.func,
    /**
     * Обработчик снятия фокуса компонента
     * @param {React.FocusEvent} event
     */
    onBlur: _propTypes2.default.func,
    /**
     * Обработчик события наведения курсора на ссылку
     * @param {React.MouseEvent} event
     */
    onMouseEnter: _propTypes2.default.func,
    /**
     * Обработчик события снятия курсора с ссылки
     * @param {React.MouseEvent} event
     */
    onMouseLeave: _propTypes2.default.func,
    /**
     * Обработчик клика по disabled ссылке
     */
    onDisabledClick: _propTypes2.default.func,
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = Link;
//# sourceMappingURL=link.js.map


/***/ }),

/***/ "./node_modules/arui-feather/masked-input/index.js":
/*!*********************************************************!*\
  !*** ./node_modules/arui-feather/masked-input/index.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

var _maskedInput = __webpack_require__(/*! ./masked-input */ "./node_modules/arui-feather/masked-input/masked-input.js");

var _maskedInput2 = _interopRequireDefault(_maskedInput);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _maskedInput2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                          * License, v. 2.0. If a copy of the MPL was not distributed with this
                                          * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/masked-input/mask.js":
/*!********************************************************!*\
  !*** ./node_modules/arui-feather/masked-input/mask.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _inputmaskCore = __webpack_require__(/*! inputmask-core */ "inputmask-core");

var _inputmaskCore2 = _interopRequireDefault(_inputmaskCore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } } /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                                                                                                           * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                                                                                                           * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

// Стандартный плейсхолдер буквы равный по ширине цифровому символу.
var PLACEHOLDER_CHAR = '\u2007';

// Стандартные formatCharacters из inputmask-core.
var DIGIT_RE = /^\d$/;
var LETTER_RE = /^[A-Za-z]$/;
var ALPHANNUMERIC_RE = /^[\dA-Za-z]$/;

/**
 * @typedef {Object} CharFormatter
 * @property {Function} validate Предикат-фильтр
 * @property {Function} [transform] Действия с символом
 */

/**
 * @typedef {Object.<string, CharFormatter>} FormatCharacters
 */

/**
 * @type FormatCharacters
 */
var DEFAULT_FORMAT_CHARACTERS = {
    '*': {
        validate: function validate(char) {
            return ALPHANNUMERIC_RE.test(char);
        }
    },
    1: {
        validate: function validate(char) {
            return DIGIT_RE.test(char);
        }
    },
    a: {
        validate: function validate(char) {
            return LETTER_RE.test(char);
        }
    },
    A: {
        validate: function validate(char) {
            return LETTER_RE.test(char);
        },
        transform: function transform(char) {
            return char.toUpperCase();
        }
    },
    '#': {
        validate: function validate(char) {
            return ALPHANNUMERIC_RE.test(char);
        },
        transform: function transform(char) {
            return char.toUpperCase();
        }
    }
};

/**
 * Класс маски. Позволяет форматировать строку по заданной маске.
 *
 * @class
 * @param {String} mask Маска в формате: https://github.com/insin/inputmask-core
 * @param {FormatCharacters} [formatCharacters] Форматтеры маски в формате `inputmask-core`
 */

var Mask = function () {

    /**
     * @type {InputMask.Pattern}
     */


    /**
     * Индекс первого редактируемого символа.
     *
     * @public
     * @type {Number}
     */
    function Mask(mask, formatCharacters, useWhitespaces) {
        _classCallCheck(this, Mask);

        this.pattern = new _inputmaskCore2.default.Pattern(mask, Object.assign({}, DEFAULT_FORMAT_CHARACTERS, formatCharacters), PLACEHOLDER_CHAR);

        this.length = this.pattern.length;
        this.firstEditableIndex = this.pattern.firstEditableIndex;
        this.lastEditableIndex = this.pattern.lastEditableIndex;
        this.useWhitespaces = useWhitespaces || false;
    }

    /**
     * Проверяет является ли символ в заданном индексе редактируемым.
     *
     * @param {Number} index Индекс символа.
     * @returns {Boolean}
     */


    /**
     * Признак что пробелы удалять не надо
     *
     * @type {Boolean}
     */


    /**
     * Индекс последнего редактируемого символа.
     *
     * @public
     * @type {Number}
     */

    /**
     * Длина маски.
     *
     * @public
     * @type {Number}
     */


    Mask.prototype.isEditableIndex = function isEditableIndex(index) {
        return this.pattern.isEditableIndex(index);
    };

    /**
     * Форматирует значение введенное в поле ввода по маске.
     *
     * @param {String} value Неформатированное значение из поля ввода.
     * @returns {String}
     */


    Mask.prototype.format = function format(value) {
        var formattedValue = '';

        var cleanValue = this.useWhitespaces ? value : value.replace(/\s+/g, '');
        var cleanValueLength = cleanValue.length;
        var cleanValueIndex = 0;
        var cleanValueChar = void 0;

        var patternIndex = 0;
        var patternChar = void 0;
        var patternLength = this.pattern.length;

        while (patternIndex < patternLength && cleanValueIndex < cleanValueLength) {
            if (!this.pattern.isEditableIndex(patternIndex)) {
                patternChar = this.pattern.pattern[patternIndex];
                formattedValue += patternChar;
                patternIndex += 1;
                if (cleanValue.charAt(cleanValueIndex) === patternChar) {
                    cleanValueIndex += 1;
                }
            } else {
                // eslint-disable-next-line no-cond-assign
                while ((cleanValueChar = cleanValue.charAt(cleanValueIndex)) !== '') {
                    if (this.pattern.isValidAtIndex(cleanValueChar, patternIndex)) {
                        formattedValue += this.pattern.transform(cleanValueChar, patternIndex);
                        patternIndex += 1;
                        cleanValueIndex += 1;
                        break;
                    } else {
                        cleanValueIndex += 1;
                    }
                }
            }
        }

        return formattedValue;
    };

    return Mask;
}();

exports.default = Mask;
//# sourceMappingURL=mask.js.map


/***/ }),

/***/ "./node_modules/arui-feather/masked-input/masked-input.js":
/*!****************************************************************!*\
  !*** ./node_modules/arui-feather/masked-input/masked-input.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _desc, _value, _class; /* This Source Code Form is subject to the terms of the Mozilla Public
                            * License, v. 2.0. If a copy of the MPL was not distributed with this
                            * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* eslint react/prop-types: 0 */

exports.getAndroidVersion = getAndroidVersion;

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _mask = __webpack_require__(/*! ./mask */ "./node_modules/arui-feather/masked-input/mask.js");

var _mask2 = _interopRequireDefault(_mask);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

// В эту проверку попадают IE9 и IE10, которые не могут корректно работать с кареткой на событии `input`.
var IS_IE9_10 = typeof window !== 'undefined' && !!window.ActiveXObject;

var IS_ANDROID = typeof window !== 'undefined' && /(android)/i.test(window.navigator.userAgent);

/**
 * Возвращает версию андроида в формате "4.2.1" или false, если не аднроид.
 *
 * @returns {String|false}
 */
function getAndroidVersion() {
    if (!/android/.test(navigator.userAgent.toLowerCase())) {
        return false;
    }

    var userAgent = navigator.userAgent.toLowerCase();
    var match = userAgent.match(/android\s([\d.]*)/);

    return match ? match[1] : false;
}

// Для IE11 вместо `onChange`, используем событие `onInput`, для правильной работы copy/paste
// Issue на ошибку в React: https://github.com/facebook/react/issues/7211
// Детектим IE11: `Object.hasOwnProperty.call(window, 'ActiveXObject') && !window.ActiveXObject;`
var IS_IE11 = typeof window !== 'undefined' && Object.hasOwnProperty.call(window, 'ActiveXObject') && !window.ActiveXObject;

// Типы операции, которые пользователь может производить с текстовым полем.
var operationType = {
    ADD: 0,
    DELETE: 1,
    REPLACE: 2
};

/**
 * Возвращает количество не редактируемых символов в строке, в соответствии с указанной маской.
 *
 * @param {String} str Анализируемая строка
 * @param {Mask} mask Маска
 * @returns {Number}
 */
var getSeparatorsAmount = function getSeparatorsAmount(str, mask) {
    return str.split('').reduce(function (amount, char, index) {
        if (mask.isEditableIndex(index)) {
            return amount;
        }
        return amount + 1;
    }, 0);
};

/**
 * Компонент поля ввода с поддержкой масок.
 * Расширяет стандартный <input /> React-а.
 */
var MaskedInput = (_class = function (_React$Component) {
    _inherits(MaskedInput, _React$Component);

    function MaskedInput() {
        var _temp, _this, _ret;

        _classCallCheck(this, MaskedInput);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.value = '', _this.beforeInputSelection = { start: 0, end: 0 }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    /**
     * @type {HTMLInputElement}
     */


    /**
     * @type {String}
     */


    /**
     * @type {Mask}
     */


    /**
     * @type {Mask}
     */


    /**
     * @type {FormatCharacters}
     */


    /**
     * @type {String}
     */


    /**
     * @type {Number}
     */


    /**
     * @type {{ start: Number, end: Number }}
     */


    MaskedInput.prototype.componentWillMount = function componentWillMount() {
        this.setMask(this.props.mask, this.props.formatCharacters, this.props.useWhitespaces);
        this.beforeChangeMask = this.mask;
        this.value = this.mask.format(this.props.value || '');
    };

    MaskedInput.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
        var reformatValue = false;

        this.beforeChangeMask = this.mask;

        if (this.props.mask !== nextProps.mask || this.props.formatCharacters !== nextProps.formatCharacters) {
            this.setMask(nextProps.mask, nextProps.formatCharacters);
            reformatValue = true;
        }

        if (reformatValue || this.props.value !== nextProps.value) {
            this.value = this.mask.format(nextProps.value || '');
        }
    };

    MaskedInput.prototype.componentWillUnmount = function componentWillUnmount() {
        if (this.caretFixTimeout) {
            clearTimeout(this.caretFixTimeout);
            this.caretFixTimeout = null;
        }
    };

    MaskedInput.prototype.render = function render() {
        var _this2 = this;

        var props = Object.assign({}, this.props);
        var length = props.maxLength !== undefined ? props.maxLength : this.mask.length;

        delete props.mask;
        delete props.formatCharacters;
        delete props.onProcessInputEvent;
        delete props.useWhitespaces;

        return _react2.default.createElement('input', _extends({}, props, {
            ref: function ref(_ref) {
                _this2.input = _ref;
            },
            maxLength: length,
            value: this.value,
            onBeforeInput: this.handleBeforeInput,
            onInput: this.handleInput,
            onChange: this.handleChange
        }));
    };

    MaskedInput.prototype.handleBeforeInput = function handleBeforeInput(event) {
        this.beforeInputSelection = {
            start: this.input.selectionStart,
            end: this.input.selectionEnd
        };

        if (this.props.onBeforeInput) {
            this.props.onBeforeInput(event);
        }
    };

    /**
     * Обрабатывает событие «input».
     *
     * @param {React.ChangeEvent<HTMLInput>} event The Event object
     */


    MaskedInput.prototype.handleInput = function handleInput(event) {
        var processedEvent = IS_IE9_10 ? event : this.processInputEvent(event);

        if (this.props.onInput) {
            this.props.onInput(processedEvent);
        }

        if (IS_IE11) {
            if (this.props.onChange) {
                this.props.onChange(processedEvent);
            }
        }
    };

    /**
     * Обрабатывает событие «change».
     *
     * @param {React.ChangeEvent<HTMLInput>} event The Event object
     */


    MaskedInput.prototype.handleChange = function handleChange(event) {
        if (IS_IE11 || !this.props.onChange) {
            return;
        }

        var processedInput = IS_IE9_10 ? this.processInputEvent(event) : event;

        if (this.props.onChange) {
            this.props.onChange(processedInput);
        }
    };

    /**
     * Устанавливает фокус на поле ввода.
     *
     * @public
     */


    MaskedInput.prototype.focus = function focus() {
        this.input.focus();
    };

    /**
     * Снимает фокус с поля ввода.
     *
     * @public
     */


    MaskedInput.prototype.blur = function blur() {
        this.input.blur();
    };

    /**
     * Возвращает ссылку на HTMLElement инпута.
     *
     * @public
     * @returns {HTMLInputElement}
     */


    MaskedInput.prototype.getControl = function getControl() {
        return this.input;
    };

    /**
     * Синхронно обновляет маску на поле ввода.
     *
     * @public
     * @param {String} newMask Новая маска
     * @param {FormatCharacters} [formatCharacters] Форматтер маски
     * @param {Boolean} useWhitespaces использовать в маске пробелы
     */


    MaskedInput.prototype.setMask = function setMask(newMask, formatCharacters, useWhitespaces) {
        if (this.maskPattern !== newMask || this.formatCharacters !== formatCharacters) {
            this.mask = new _mask2.default(newMask, formatCharacters, useWhitespaces);
            this.maskPattern = newMask;
            this.formatCharacters = formatCharacters;
        }
    };

    MaskedInput.prototype.processInputEvent = function processInputEvent(event) {
        if (this.props.onProcessInputEvent) {
            this.props.onProcessInputEvent(event);
        }

        var prevSelection = this.input.selectionStart;
        var newValue = event.target.value;

        var currentValue = this.value;
        var formattedValue = this.mask.format(newValue);
        this.value = formattedValue;
        event.target.value = formattedValue;

        // Если изменение поля ввода произошло не в конце ввода,
        // то необходимо починить стандартное поведение Реакта и
        // вернуть каретку к последнему изменению.
        if (prevSelection <= currentValue.length) {
            var newSelection = prevSelection;

            // Определяем тип операции, который был произведен над текстовым полем.
            var opType = newValue.length >= currentValue.length ? operationType.ADD : operationType.DELETE;

            // На пользовательском инпуте было выделение перед операцией,
            // значит могла быть операция или удаления или замены.
            var beforeInputSelectionLength = this.beforeInputSelection.end - this.beforeInputSelection.start;
            if (beforeInputSelectionLength >= 1) {
                if (newValue.length !== currentValue.length - beforeInputSelectionLength) {
                    opType = operationType.REPLACE;
                }
            }

            var beforeChangeSeparatorsAmount = getSeparatorsAmount(currentValue.slice(0, prevSelection), this.beforeChangeMask);

            var afterChangeSeparatorsAmount = getSeparatorsAmount(formattedValue.slice(0, newSelection), this.mask);

            // Двигаем каретку вправо, если слева от каретки добавились не редактируемые символы
            var shouldShiftCaret = beforeChangeSeparatorsAmount < afterChangeSeparatorsAmount && (opType === operationType.ADD || opType === operationType.REPLACE) && this.beforeChangeMask.isEditableIndex(this.beforeInputSelection.start) && this.mask.isEditableIndex(newSelection);

            // Двигаем каретку влево, если слева от каретки добавились не редактируемые символы
            var shouldUnshiftCaret = beforeChangeSeparatorsAmount > afterChangeSeparatorsAmount && opType === operationType.DELETE && this.mask.isEditableIndex(newSelection - 1) && newSelection > 0;

            if (shouldUnshiftCaret || shouldShiftCaret) {
                newSelection += afterChangeSeparatorsAmount - beforeChangeSeparatorsAmount;
            }

            // Для операции добавления и замены, если мы стояли на нередактируемом символе,
            // то добавляем сдвиг до ближайшего редактируемого.
            if (opType === operationType.ADD || opType === operationType.REPLACE) {
                var index = this.beforeInputSelection.start;
                while (!this.beforeChangeMask.isEditableIndex(index) && index < formattedValue.length) {
                    index += 1;
                }
                newSelection += index - this.beforeInputSelection.start;
            }

            // Если вдруг попали на нередактируемый символ маски,
            // то подвигаем курсом до ближайшего редактируемого.
            if (opType === operationType.ADD || opType === operationType.REPLACE) {
                while (!this.mask.isEditableIndex(newSelection) && newSelection < formattedValue.length) {
                    newSelection += 1;
                }
            } else if (opType === operationType.DELETE) {
                while (!this.mask.isEditableIndex(newSelection - 1) && newSelection > 0) {
                    newSelection -= 1;
                }
            }

            // Положение каретки с учетом первого и последнего редактируемого символа маски.
            var clampedSection = this.clampSelection(newSelection);

            // Фикс бага смещения каретки в браузере на андроидах Jelly Bean (c 4.1 по 4.3)
            var offsetSection = opType === operationType.ADD && IS_ANDROID && parseFloat(getAndroidVersion(), 10) < 4.4 ? 1 : 0;

            this.setInputSelection(clampedSection + offsetSection);
        } else if (IS_ANDROID) {
            // Если изменение поля произошло в конце ввода.
            // Android chrome имеет дефект с автоматической установкой каретки
            // при использовании клавиатуры отличной от type="text".
            this.setInputSelectionByTimeout(event.target.selectionStart);
        }

        return event;
    };

    /**
     * Возвращает положение каретки с учетом первого и последнего редактируемого символа маски.
     *
     * @param {Number} selection Положение каретки
     * @returns {Number}
     */


    MaskedInput.prototype.clampSelection = function clampSelection(selection) {
        if (selection < this.mask.firstEditableIndex) {
            return this.mask.firstEditableIndex;
        }

        // +1 разрешает каретке становится в конец поля ввода при необходимости.
        if (selection > this.mask.lastEditableIndex + 1) {
            return this.mask.lastEditableIndex + 1;
        }

        return selection;
    };

    /**
     * Устанавливает каретку поля ввода в новую позицию.
     *
     * @param {Number} selection Новое положение каретки
     */


    MaskedInput.prototype.setInputSelection = function setInputSelection(selection) {
        this.input.selectionStart = selection;
        this.input.selectionEnd = selection;

        // IE10 не умеет синхронно в событие `change` переставлять каретку.
        // Android chrome имеет дефект с автоматической установкой каретки
        // при использовании клавиатуры отличной от type="text".
        if (IS_IE9_10 || IS_ANDROID) {
            this.setInputSelectionByTimeout(selection);
        }
    };

    /**
     * Устанавливает каретку поля ввода в заданную позицию асинхронно.
     *
     * Во-избежание дефекта с установкой каретки, наблюдаемом в мобильных браузерах, а так же
     * браузерах IE9-10, установка происходит асинхронно, с минимальной задержкой,
     * с помощью [setTimeout] обертки.
     *
     * @param {Number} selection Положение каретки
     */


    MaskedInput.prototype.setInputSelectionByTimeout = function setInputSelectionByTimeout(selection) {
        var _this3 = this;

        if (this.caretFixTimeout) {
            clearTimeout(this.caretFixTimeout);
            this.caretFixTimeout = null;
        }

        this.caretFixTimeout = setTimeout(function () {
            _this3.caretFixTimeout = null;
            _this3.input.selectionStart = selection;
            _this3.input.selectionEnd = selection;
        }, 0);
    };

    return MaskedInput;
}(_react2.default.Component), (_applyDecoratedDescriptor(_class.prototype, 'handleBeforeInput', [_autobind2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'handleBeforeInput'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'handleInput', [_autobind2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'handleInput'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'handleChange', [_autobind2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'handleChange'), _class.prototype)), _class);
MaskedInput.propTypes =  true ? {
    /** Маска для поля ввода, использует формат https://github.com/insin/inputmask-core */
    mask: _propTypes2.default.string.isRequired,
    /** Кастомные форматтеры символов маски, использует формат formatCharacters из `inputmask-core` */
    formatCharacters: _propTypes2.default.objectOf(_propTypes2.default.shape({
        validate: _propTypes2.default.func.isRequired,
        transform: _propTypes2.default.func
    })),
    /** Максимальное число символов */
    maxLength: _propTypes2.default.number,
    /**
     * Обработчик, вызываемый перед началом ввода в поле
     * @param {React.ChangeEvent} event
     */
    onProcessInputEvent: _propTypes2.default.func,
    /** Признак что пробелы удалять не надо */
    useWhitespaces: _propTypes2.default.bool,
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = MaskedInput;
//# sourceMappingURL=masked-input.js.map


/***/ }),

/***/ "./node_modules/arui-feather/menu-item/index.js":
/*!******************************************************!*\
  !*** ./node_modules/arui-feather/menu-item/index.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ../dropdown */ "./node_modules/arui-feather/dropdown/index.js");

__webpack_require__(/*! ../icon */ "./node_modules/arui-feather/icon/index.js");

__webpack_require__(/*! ../link */ "./node_modules/arui-feather/link/index.js");

__webpack_require__(/*! ./menu-item.css */ "./node_modules/arui-feather/menu-item/menu-item.css");

var _menuItem = __webpack_require__(/*! ./menu-item */ "./node_modules/arui-feather/menu-item/menu-item.js");

var _menuItem2 = _interopRequireDefault(_menuItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _menuItem2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                       * License, v. 2.0. If a copy of the MPL was not distributed with this
                                       * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/menu-item/menu-item.css":
/*!***********************************************************!*\
  !*** ./node_modules/arui-feather/menu-item/menu-item.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"menu-item": "RFSo9r_4RiqdT91Eyft5K",
	"menu-item_disabled": "_3y-OZVPHWuA2h5LrWmY6BE",
	"menu-item_checked": "_2PpsbcOi44LAxfrn4RXORD",
	"menu-item_hidden": "_133cLszitLWIHdPgCp6lrc",
	"menu-item_type_link": "_1dP-xsLUq9yRRYUsayfJxx",
	"menu-item__link": "_2eSFj3CQ0_dKCu_NeOA_xI"
};

/***/ }),

/***/ "./node_modules/arui-feather/menu-item/menu-item.js":
/*!**********************************************************!*\
  !*** ./node_modules/arui-feather/menu-item/menu-item.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _dec, _dec2, _class, _desc, _value, _class2, _class3, _temp2; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                   * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                   * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _dropdown = __webpack_require__(/*! ../dropdown/dropdown */ "./node_modules/arui-feather/dropdown/dropdown.js");

var _dropdown2 = _interopRequireDefault(_dropdown);

var _link = __webpack_require__(/*! ../link/link */ "./node_modules/arui-feather/link/link.js");

var _link2 = _interopRequireDefault(_link);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

/**
 * Компонент элемента меню. Как правило, используется совместно с `Menu`.
 */
var MenuItem = (_dec = (0, _cn2.default)('menu-item'), _dec2 = (0, _performance2.default)(), _dec(_class = _dec2(_class = (_class2 = (_temp2 = _class3 = function (_React$Component) {
    _inherits(MenuItem, _React$Component);

    function MenuItem() {
        var _temp, _this, _ret;

        _classCallCheck(this, MenuItem);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.state = {
            hovered: false,
            focused: false
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    MenuItem.prototype.render = function render(cn) {
        var _this2 = this;

        var content = this.props.children || this.props.value;
        var itemElement = void 0;
        var menuItemProps = {
            ref: function ref(root) {
                _this2.root = root;
            },
            'data-test-id': this.props['data-test-id']
        };

        switch (this.props.type) {
            case 'dropdown':
                itemElement = _react2.default.createElement(_dropdown2.default, {
                    ref: function ref(control) {
                        _this2.control = control;
                    },
                    className: cn('control') + ' ' + cn('dropdown'),
                    size: this.props.size,
                    theme: this.props.theme,
                    opened: this.state.hovered,
                    switcherType: 'link',
                    switcherText: content,
                    popupContent: this.props.popup,
                    popupProps: {
                        directions: ['bottom-left'],
                        target: 'anchor'
                    },
                    mode: 'hover',
                    onSwitcherClick: this.handleClick,
                    onSwitcherMouseEnter: this.handleMouseEnter,
                    onSwitcherMouseLeave: this.handleMouseLeave
                });
                break;
            case 'block':
                menuItemProps = {
                    ref: function ref(control) {
                        _this2.root = control;
                        _this2.control = control;
                    },
                    onClick: this.handleClick,
                    onMouseEnter: this.handleMouseEnter,
                    onMouseLeave: this.handleMouseLeave
                };
                itemElement = _react2.default.createElement(
                    'span',
                    { className: cn('control') },
                    content
                );

                break;
            case 'link':
            default:
                itemElement = _react2.default.createElement(
                    _link2.default,
                    {
                        ref: function ref(control) {
                            _this2.control = control;
                        },
                        className: cn('control') + ' ' + cn('link'),
                        size: this.props.size,
                        theme: this.props.theme,
                        pseudo: this.props.view === 'pseudo',
                        disabled: this.props.disabled,
                        checked: this.props.checked,
                        text: this.props.view !== 'plain' && content,
                        url: this.props.url,
                        target: this.props.target,
                        onClick: this.handleClick,
                        onFocus: this.handleFocus,
                        onBlur: this.handleBlur,
                        onMouseEnter: this.handleMouseEnter,
                        onMouseLeave: this.handleMouseLeave
                    },
                    this.props.view === 'plain' && content
                );
                break;
        }

        return _react2.default.createElement(
            'div',
            _extends({
                className: cn({
                    disabled: this.props.disabled,
                    checked: this.props.checked,
                    hidden: this.props.hidden,
                    type: this.props.type,
                    hovered: this.props.hovered === undefined ? this.state.hovered : this.props.hovered,
                    focused: this.state.focused
                })
            }, menuItemProps),
            itemElement
        );
    };

    MenuItem.prototype.handleClick = function handleClick(event) {
        if (this.props.disabled) {
            event.preventDefault();
            return;
        }

        if (this.props.onClick) {
            this.props.onClick(event);
        }
    };

    MenuItem.prototype.handleFocus = function handleFocus(event) {
        this.setState({ focused: true });

        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    };

    MenuItem.prototype.handleBlur = function handleBlur(event) {
        this.setState({ focused: false });

        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    };

    MenuItem.prototype.handleMouseEnter = function handleMouseEnter(event) {
        this.setState({ hovered: true });

        if (this.props.onMouseEnter) {
            this.props.onMouseEnter(event);
        }
    };

    MenuItem.prototype.handleMouseLeave = function handleMouseLeave(event) {
        this.setState({ hovered: false });

        if (this.props.onMouseLeave) {
            this.props.onMouseLeave(event);
        }
    };

    /**
     * Возвращает корневой `HTMLElement` компонента.
     *
     * @public
     * @returns {HTMLElement}
     */


    MenuItem.prototype.getNode = function getNode() {
        return this.root;
    };

    /**
     * Устанавливает фокус на элементе меню.
     *
     * @public
     */


    MenuItem.prototype.focus = function focus() {
        this.control.focus();
    };

    /**
     * Убирает фокус с элемента меню.
     *
     * @public
     */


    MenuItem.prototype.blur = function blur() {
        if (document.activeElement) {
            document.activeElement.blur();
        }
    };

    return MenuItem;
}(_react2.default.Component), _class3.defaultProps = {
    type: 'link',
    view: 'plain'
}, _temp2), (_applyDecoratedDescriptor(_class2.prototype, 'handleClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleClick'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleFocus', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleFocus'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleBlur', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleBlur'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseEnter', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseEnter'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseLeave', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseLeave'), _class2.prototype)), _class2)) || _class) || _class);
MenuItem.propTypes =  true ? {
    /** Тип элемента меню */
    type: _propTypes2.default.oneOf(['link', 'dropdown', 'block']),
    /** Тип ссылки, для компонента с type='link' */
    view: _propTypes2.default.oneOf(['plain', 'underlined', 'pseudo']),
    /** href ссылки, для компонента с type='link' */
    url: _propTypes2.default.string,
    /** target для ссылки */
    target: _propTypes2.default.oneOf(['_self', '_blank', '_parent', '_top']),
    /** Уникальное значение элемента. Для использования в Menu */
    value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
    /** Попап для компонента с type='dropdown' */
    popup: _propTypes2.default.node,
    /** Управление возможностью выбирать данный компонент */
    disabled: _propTypes2.default.bool,
    /** Управление состоянием выбран/не выбран компонента */
    checked: _propTypes2.default.bool,
    /** Управление видимостью компонента */
    hidden: _propTypes2.default.bool,
    /** Управление визуальным выделением компонента */
    hovered: _propTypes2.default.bool,
    /** Размер компонента */
    size: _propTypes2.default.oneOf(['s', 'm', 'l', 'xl']),
    /** Дочерние элементы `MenuItem` */
    children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]),
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /**
     * Только для type='link', обработчик клика по компоненту
     * @param {React.MouseEvent} event
     */
    onClick: _propTypes2.default.func,
    /**
     * Обработчик фокуса компонента
     * @param {React.FocusEvent} event
     */
    onFocus: _propTypes2.default.func,
    /**
     * Обработчик снятия фокуса компонента
     * @param {React.FocusEvent} event
     */
    onBlur: _propTypes2.default.func,
    /**
     * Обработчик события наведения курсора на элемент меню
     * @param {React.MouseEvent} event
     */
    onMouseEnter: _propTypes2.default.func,
    /**
     * Обработчик события снятия курсора с элемента меню
     * @param {React.MouseEvent} event
     */
    onMouseLeave: _propTypes2.default.func,
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = MenuItem;
//# sourceMappingURL=menu-item.js.map


/***/ }),

/***/ "./node_modules/arui-feather/menu/index.js":
/*!*************************************************!*\
  !*** ./node_modules/arui-feather/menu/index.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ../icon/ui/check */ "./node_modules/arui-feather/icon/ui/check/index.js");

__webpack_require__(/*! ../menu-item */ "./node_modules/arui-feather/menu-item/index.js");

__webpack_require__(/*! ./menu.css */ "./node_modules/arui-feather/menu/menu.css");

__webpack_require__(/*! ./menu_theme_alfa-on-white.css */ "./node_modules/arui-feather/menu/menu_theme_alfa-on-white.css");

__webpack_require__(/*! ./menu_theme_alfa-on-color.css */ "./node_modules/arui-feather/menu/menu_theme_alfa-on-color.css");

var _menu = __webpack_require__(/*! ./menu */ "./node_modules/arui-feather/menu/menu.js");

var _menu2 = _interopRequireDefault(_menu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

exports.default = _menu2.default;
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/menu/menu.css":
/*!*************************************************!*\
  !*** ./node_modules/arui-feather/menu/menu.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"menu": "_2uDPWI5D_VA6FiUIzjgSBM",
	"menu_view_horizontal": "jfMzj2u-FWVToqBr6uVk3",
	"menu-item": "_2YV6MIJvWAlHnFC2aLYGVR",
	"menu-item_type_link": "_2ss1BUkEvOrZsIj1sF65yL",
	"menu_mode_check": "XubmgSRqTm-LBFQlhqETo",
	"menu-item_checked": "_9hKhsRNFY8mWnL1Ui2dIw",
	"icon": "_3M0k7wS88E69Z96vP8C4r7",
	"menu_mode_radio": "_2ZH6u-kq84oezJA-DsfucM",
	"menu_mode_radio-check": "_1vH45HldDK7VxUeZWq-Ir_",
	"menu_size_s": "_1dZ3aPof4MswSzmvBjPZi4",
	"menu__group-title": "dX1aWQuD56veEvq2YoFIo",
	"menu__group": "_39nITPZVj27QNrlhxPoGjw",
	"menu_size_m": "rSOXTo3c8P2RzukDKSHH6",
	"menu_size_l": "_1mHfmTrXAYuNdIKwcQ7-PT",
	"menu_size_xl": "Zhho0Xy6pX8WXRDbmEFeW",
	"menu_group-view_line": "T49wjDtWVAhkko-LU-leO"
};

/***/ }),

/***/ "./node_modules/arui-feather/menu/menu.js":
/*!************************************************!*\
  !*** ./node_modules/arui-feather/menu/menu.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _dec, _dec2, _class, _desc, _value, _class2, _class3, _temp2; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                   * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                   * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* eslint jsx-a11y/no-static-element-interactions: 0 */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _reactStyleProptype = __webpack_require__(/*! react-style-proptype */ "react-style-proptype");

var _reactStyleProptype2 = _interopRequireDefault(_reactStyleProptype);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _tick = __webpack_require__(/*! ../icon/ui/tick */ "./node_modules/arui-feather/icon/ui/tick/index.js");

var _tick2 = _interopRequireDefault(_tick);

var _menuItem = __webpack_require__(/*! ../menu-item/menu-item */ "./node_modules/arui-feather/menu-item/menu-item.js");

var _menuItem2 = _interopRequireDefault(_menuItem);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _window = __webpack_require__(/*! ../lib/window */ "./node_modules/arui-feather/lib/window.js");

var _keyboardCode = __webpack_require__(/*! ../lib/keyboard-code */ "./node_modules/arui-feather/lib/keyboard-code.js");

var _keyboardCode2 = _interopRequireDefault(_keyboardCode);

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

/**
 * Компонент меню.
 */
var Menu = (_dec = (0, _cn2.default)('menu'), _dec2 = (0, _performance2.default)(true), _dec(_class = _dec2(_class = (_class2 = (_temp2 = _class3 = function (_React$Component) {
    _inherits(Menu, _React$Component);

    function Menu() {
        var _temp, _this, _ret;

        _classCallCheck(this, Menu);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.state = {
            highlightedItem: null,
            checkedItems: [],
            hovered: false
        }, _this.menuItemList = [], _this.blurTimeoutId = null, _temp), _possibleConstructorReturn(_this, _ret);
    }

    Menu.prototype.componentDidMount = function componentDidMount() {
        if (!!this.props.content && this.props.content.length > 0 && (!this.props.checkedItems || this.props.checkedItems.length === 0) && this.props.mode === 'radio') {
            var firstItem = this.getFirstItem(this.props.content);

            this.changeCheckedItems([firstItem.value]);
        }
    };

    Menu.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
        if (this.props.mode !== 'check' && this.state.checkedItems[0] && nextProps.checkedItems[0] !== this.state.checkedItems[0]) {
            var highlightedItem = void 0;

            this.menuItemList.forEach(function (item, index, menuItemList) {
                if (item.ref === nextProps.checkedItems[0]) {
                    highlightedItem = menuItemList[index].instance;
                }
            });

            if (this.props.onHighlightItem) {
                this.props.onHighlightItem(highlightedItem);
            }
        }
    };

    Menu.prototype.componentWillUpdate = function componentWillUpdate() {
        this.menuItemList = [];
    };

    Menu.prototype.componentWillUnmount = function componentWillUnmount() {
        if (this.blurTimeoutId) {
            clearTimeout(this.blurTimeoutId);
            this.blurTimeoutId = null;
        }
    };

    Menu.prototype.render = function render(cn) {
        var _this2 = this;

        /* eslint-disable jsx-a11y/no-noninteractive-tabindex */
        return _react2.default.createElement(
            'div',
            {
                ref: function ref(root) {
                    _this2.root = root;
                },
                style: this.props.style,
                className: cn({
                    size: this.props.size,
                    view: this.props.view,
                    'group-view': this.props.groupView,
                    hovered: this.state.hovered,
                    disabled: this.props.disabled,
                    mode: this.props.mode
                }),
                id: this.props.id,
                tabIndex: 0,
                onMouseEnter: this.handleMouseEnter,
                onMouseLeave: this.handleMouseLeave,
                onKeyDown: this.handleKeyDown,
                onKeyUp: this.handleKeyUp,
                onFocus: this.handleFocus,
                onBlur: this.handleBlur,
                'data-test-id': this.props['data-test-id']
            },
            !!this.props.content && this.renderMenuItemList(cn, this.props.content)
        );
        /* eslint-enable jsx-a11y/no-noninteractive-tabindex */
    };

    Menu.prototype.renderMenuItemList = function renderMenuItemList(cn, content) {
        var _this3 = this;

        var result = [];
        var groupKey = 0;

        content.forEach(function (item) {
            if (item.type === 'group') {
                result.push(_react2.default.createElement(
                    'div',
                    {
                        className: cn('group'),
                        key: 'group_' + groupKey
                    },
                    !!item.title && _react2.default.createElement(
                        'div',
                        { className: cn('group-title') },
                        item.title
                    ),
                    _this3.renderMenuItemList(cn, item.content)
                ));
                groupKey += 1;
            } else {
                result.push(_this3.renderMenuItem(item));
            }
        });

        return result;
    };

    Menu.prototype.renderMenuItem = function renderMenuItem(item) {
        var _this4 = this;

        var itemProps = item.props || {};
        var isItemChecked = this.getIndexInCheckedItemsList(item.value) !== -1;
        var isItemDisabled = this.props.disabled || itemProps.disabled;
        var clickHandler = this.props.mode === 'basic' ? itemProps.onClick : function () {
            return _this4.handleMenuItemClick(item);
        };
        var menuItem = {
            item: item,
            ref: item.value
        };
        var menuItemProps = Object.assign({}, itemProps, {
            disabled: isItemDisabled,
            value: item.value,
            size: this.props.size || itemProps.size,
            onClick: !isItemDisabled ? clickHandler : undefined
        });
        var highlightedItem = this.props.highlightedItem === undefined ? this.state.highlightedItem : this.props.highlightedItem;
        var iconSize = void 0;

        switch (this.props.size) {
            case 's':case 'm':
                iconSize = 's';break;
            case 'l':case 'xl':
                iconSize = 'm';break;
        }

        this.menuItemList.push(menuItem);

        return _react2.default.createElement(
            _menuItem2.default,
            _extends({}, menuItemProps, {
                ref: function ref(instance) {
                    menuItem.instance = instance;
                },
                key: item.key || item.value,
                checked: isItemChecked,
                type: this.props.mode !== 'basic' ? 'block' : itemProps.type,
                onMouseEnter: function onMouseEnter() {
                    return _this4.handleMenuItemMouseEnter(menuItem);
                },
                onMouseLeave: this.handleMenuItemMouseLeave,
                hovered: highlightedItem && highlightedItem.ref === menuItem.ref
            }),
            this.props.mode === 'check' && isItemChecked && _react2.default.createElement(_tick2.default, {
                size: iconSize
            }),
            item.content
        );
    };

    Menu.prototype.handleMenuItemClick = function handleMenuItemClick(item) {
        this.setNewCheckedItems(item);

        if (this.props.onItemClick) {
            this.props.onItemClick(item);
        }
    };

    Menu.prototype.handleMouseEnter = function handleMouseEnter(event) {
        this.setState({ hovered: true });

        if (this.props.onMouseEnter) {
            this.props.onMouseEnter(event);
        }
    };

    Menu.prototype.handleMouseLeave = function handleMouseLeave(event) {
        this.setState({ hovered: false });

        if (this.props.onMouseLeave) {
            this.props.onMouseLeave(event);
        }
    };

    Menu.prototype.handleKeyUp = function handleKeyUp(event) {
        if (this.props.onKeyUp) {
            this.props.onKeyUp(event);
        }
    };

    Menu.prototype.handleKeyDown = function handleKeyDown(event) {
        var _this5 = this;

        var highlightedItem = null;
        var highlightedMenuItem = null;
        var menuIteListLength = this.menuItemList.length;

        switch (event.which) {
            case _keyboardCode2.default.DOWN_ARROW:
                {
                    event.preventDefault();

                    if (this.state.highlightedItem) {
                        this.menuItemList.forEach(function (item, index, menuItemList) {
                            if (item.ref === _this5.state.highlightedItem.ref) {
                                if (index + 1 === menuIteListLength) {
                                    highlightedItem = menuItemList[0];
                                } else {
                                    highlightedItem = menuItemList[index + 1];
                                }
                            }
                        });
                    } else {
                        var _menuItemList = this.menuItemList;
                        highlightedItem = _menuItemList[0];
                    }

                    this.setState({
                        highlightedItem: highlightedItem
                    });

                    if (this.props.onHighlightItem) {
                        this.props.onHighlightItem(highlightedItem);
                    }

                    break;
                }
            case _keyboardCode2.default.UP_ARROW:
                {
                    event.preventDefault();

                    if (this.state.highlightedItem) {
                        this.menuItemList.forEach(function (item, index, menuItemList) {
                            if (item.ref === _this5.state.highlightedItem.ref) {
                                if (index - 1 < 0) {
                                    highlightedItem = menuItemList[menuIteListLength - 1];
                                } else {
                                    highlightedItem = menuItemList[index - 1];
                                }
                            }
                        });
                    } else {
                        highlightedItem = this.menuItemList[menuIteListLength - 1];
                    }

                    this.setState({
                        highlightedItem: highlightedItem
                    });

                    if (this.props.onHighlightItem) {
                        this.props.onHighlightItem(highlightedItem);
                    }

                    break;
                }
            case _keyboardCode2.default.ENTER:
            case _keyboardCode2.default.SPACE:
                {
                    event.preventDefault();

                    highlightedItem = this.props.highlightedItem === undefined ? this.state.highlightedItem : this.props.highlightedItem;

                    if (highlightedItem) {
                        this.setNewCheckedItems(highlightedItem.item);
                    }

                    break;
                }
        }

        if (highlightedItem) {
            highlightedMenuItem = highlightedItem.instance;
        }

        if (this.props.onKeyDown) {
            this.props.onKeyDown(event, highlightedMenuItem);
        }
    };

    Menu.prototype.handleFocus = function handleFocus(event) {
        if (this.blurTimeoutId) {
            clearTimeout(this.blurTimeoutId);
            this.blurTimeoutId = null;
        }

        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    };

    Menu.prototype.handleBlur = function handleBlur(event) {
        var _this6 = this;

        event.persist();
        if (this.blurTimeoutId) {
            clearTimeout(this.blurTimeoutId);
        }

        this.blurTimeoutId = setTimeout(function () {
            if ((0, _window.isNodeOutsideElement)(document.activeElement, _this6.root) && _this6.props.onBlur) {
                _this6.props.onBlur(event);
            }
            _this6.blurTimeoutId = null;
        }, 0);
    };

    Menu.prototype.handleMenuItemMouseEnter = function handleMenuItemMouseEnter(menuItem) {
        this.setState({
            highlightedItem: menuItem
        });

        if (this.props.onHighlightItem) {
            this.props.onHighlightItem(menuItem);
        }
    };

    Menu.prototype.handleMenuItemMouseLeave = function handleMenuItemMouseLeave() {
        this.setState({
            highlightedItem: null
        });

        if (this.props.onHighlightItem) {
            this.props.onHighlightItem(null);
        }
    };

    /**
     * Возвращает корневой `HTMLElement` компонента.
     *
     * @public
     * @returns {HTMLElement}
     */


    Menu.prototype.getNode = function getNode() {
        return this.root;
    };

    /**
     * Устанавливает фокус на меню.
     *
     * @public
     */


    Menu.prototype.focus = function focus() {
        this.root.focus();

        if (this.props.autoFocusFirstItem) {
            var highlightedItem = this.menuItemList[0];
            this.setState({
                highlightedItem: highlightedItem
            });

            if (this.props.onHighlightItem) {
                this.props.onHighlightItem(highlightedItem);
            }
        }
    };

    /**
     * Убирает фокус с меню.
     *
     * @public
     */


    Menu.prototype.blur = function blur() {
        if (document.activeElement) {
            document.activeElement.blur();
        }
    };

    Menu.prototype.setNewCheckedItems = function setNewCheckedItems(item) {
        var value = item.value;

        var checkedItems = this.props.checkedItems !== undefined ? Array.from(this.props.checkedItems) : Array.from(this.state.checkedItems);
        var indexInCheckedItemsList = this.getIndexInCheckedItemsList(value);

        switch (this.props.mode) {
            case 'check':
                if (indexInCheckedItemsList === -1) {
                    checkedItems.push(value);
                } else {
                    checkedItems.splice(indexInCheckedItemsList, 1);
                }
                break;
            case 'radio':
                if (indexInCheckedItemsList === -1) {
                    checkedItems = [value];
                }
                break;
            case 'radio-check':
                if (indexInCheckedItemsList === -1) {
                    checkedItems = [value];
                } else {
                    checkedItems = [];
                }
                break;
            default:
                break;
        }

        this.changeCheckedItems(checkedItems);
        this.focus();
    };

    /**
     * Изменяет выбранные значения.
     *
     * @param {Array.<String|Number>} checkedItems Список выбранных значений
     */


    Menu.prototype.changeCheckedItems = function changeCheckedItems(checkedItems) {
        this.setState({
            checkedItems: checkedItems
        });

        if (this.props.onItemCheck) {
            this.props.onItemCheck(checkedItems);
        }
    };

    Menu.prototype.getIndexInCheckedItemsList = function getIndexInCheckedItemsList(value) {
        var checkedItems = this.props.checkedItems ? this.props.checkedItems : this.state.checkedItems;
        return checkedItems.indexOf(value);
    };

    Menu.prototype.getFirstItem = function getFirstItem(content) {
        var firstItem = content[0];

        return firstItem.type === 'group' ? this.getFirstItem(firstItem.content) : firstItem;
    };

    return Menu;
}(_react2.default.Component), _class3.defaultProps = {
    size: 'm',
    mode: 'basic',
    groupView: 'default',
    autoFocusFirstItem: false
}, _temp2), (_applyDecoratedDescriptor(_class2.prototype, 'handleMenuItemClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMenuItemClick'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseEnter', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseEnter'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseLeave', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseLeave'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleKeyUp', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleKeyUp'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleKeyDown', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleKeyDown'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleFocus', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleFocus'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleBlur', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleBlur'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMenuItemMouseLeave', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMenuItemMouseLeave'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'getIndexInCheckedItemsList', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'getIndexInCheckedItemsList'), _class2.prototype)), _class2)) || _class) || _class);
Menu.propTypes =  true ? {
    /** Тип расположения меню: 'horizontal' */
    view: _propTypes2.default.string,
    /** Размещение заголовка групп: обычное или в одну строку с первым элементом группы */
    groupView: _propTypes2.default.oneOf(['default', 'line']),
    /** Тип списка вариантов меню */
    mode: _propTypes2.default.oneOf(['basic', 'check', 'radio', 'radio-check']),
    /** Управление возможностью изменения значения */
    disabled: _propTypes2.default.bool,
    /** Управление состоянием фокуса элемента */
    focused: _propTypes2.default.bool,
    /** Управление автоматическим фокусом на первом элементе при вызове публичного метода focus */
    autoFocusFirstItem: _propTypes2.default.bool,
    /** Элемент меню, на котором стоит выделение */
    highlightedItem: _propTypes2.default.shape({
        /** Уникальный идентификатор */
        ref: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]),
        /** Элемент списка типа ContentItem */
        item: _propTypes2.default.any
    }),
    /** Список объектов ContentItem */
    content: _propTypes2.default.arrayOf(_propTypes2.default.shape({
        /** Тип элемента */
        type: _propTypes2.default.oneOf(['item', 'group']),
        /** Только для type='item', свойство для компонента [MenuItem](#!/MenuItem) */
        value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
        /** Содержание элемента */
        content: _propTypes2.default.oneOfType([_propTypes2.default.node, _propTypes2.default.array]),
        /** Только для type='item': свойства для компонента [MenuItem](#!/MenuItem) */
        props: _propTypes2.default.object
    })),
    /** Список значений выбранных элементов */
    checkedItems: _propTypes2.default.arrayOf(_propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number])),
    /** Размер компонента */
    size: _propTypes2.default.oneOf(['s', 'm', 'l', 'xl']),
    /** Объект со стилями */
    style: _reactStyleProptype2.default,
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /** Идентификатор компонента в DOM */
    id: _propTypes2.default.string,
    /**
     * Обработчик клика по варианту меню
     * @param item
     */
    onItemClick: _propTypes2.default.func,
    /**
     * Обработчик выбора варианта меню
     * @param {Array<string|number>} checkedItems
     */
    onItemCheck: _propTypes2.default.func,
    /**
     * Обработчик события наведения курсора на меню
     * @param {React.MouseEvent} event
     */
    onMouseEnter: _propTypes2.default.func,
    /**
     * Обработчик события снятия курсора с меню
     * @param {React.MouseEvent} event
     */
    onMouseLeave: _propTypes2.default.func,
    /**
     * Обработчик события нажатия на клавишу клавиатуры в момент, когда фокус находится на компоненте
     * @param {React.KeyboardEvent} event
     */
    onKeyDown: _propTypes2.default.func,
    /**
     * Обработчик события отжатия на клавишу клавиатуры в момент, когда фокус находится на компоненте
     * @param {React.KeyboardEvent} event
     */
    onKeyUp: _propTypes2.default.func,
    /**
     * Обработчик фокуса
     * @param {React.FocusEvent} event
     */
    onFocus: _propTypes2.default.func,
    /**
     * Обработчик снятия фокуса
     * @param {React.FocusEvent} event
     */
    onBlur: _propTypes2.default.func,
    /**
     * Обработчик события выделения элемента меню, принимает на вход переменную типа HighlightedItem
     * @param highlightedItem
     */
    onHighlightItem: _propTypes2.default.func,
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = Menu;
//# sourceMappingURL=menu.js.map


/***/ }),

/***/ "./node_modules/arui-feather/menu/menu_theme_alfa-on-color.css":
/*!*********************************************************************!*\
  !*** ./node_modules/arui-feather/menu/menu_theme_alfa-on-color.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"menu_theme_alfa-on-color": "_2cuKOKb_vSdrzdVrlKXW5C",
	"menu-item": "_2FCgDzXXGXQ0Jf0nLZQYGw",
	"menu-item_hovered": "_3YDWidNlD5tZN6XMRG5pyP",
	"menu-item_disabled": "mlsF_9IRDnSZF4zBS86dh",
	"menu-item_type_link": "_8V9lwXLtFn5rbbEBp9lPV",
	"menu-item_type_dropdown": "_3fX4a4wqNcRi7DKf6kfFaj",
	"icon": "_3nsk63DX__pADJzWRoy938",
	"menu_mode_radio": "_2nhg7BNp5qWJV5IK88gZGU",
	"menu-item_checked": "e_xYkaIpA7PUQqM3ZexQW",
	"menu_mode_radio-check": "_2ps8VF01bnMEvpIP0oNfyY",
	"menu_mode_check": "_2f8ZvQP1WbcpjgUzoZpVEV",
	"menu__group-title": "_1WuVirHozwvLhGnsnG1r7Z"
};

/***/ }),

/***/ "./node_modules/arui-feather/menu/menu_theme_alfa-on-white.css":
/*!*********************************************************************!*\
  !*** ./node_modules/arui-feather/menu/menu_theme_alfa-on-white.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"menu_theme_alfa-on-white": "_1YAiUlBUsBoXfYWf7bMcry",
	"menu-item": "_3fLLq_q1EbJxA8cKpqcGnT",
	"menu-item_hovered": "_23iAkDBgE2loPbLlrsPGXH",
	"menu-item_disabled": "_7m3F5z_Ji2ks3EX6mpNzV",
	"menu-item_type_link": "_3c1wb6OqXy3RDQTV9KIQc2",
	"menu-item_type_dropdown": "_2tyunLZzJNTEnoG7oLkA1X",
	"icon": "_2ZrcNGbL7EOC6yPHrsGD9V",
	"menu_mode_radio": "_1Wts7dh1XmvsDSWXoz08z3",
	"menu-item_checked": "J2p_Iq4NmGWRUwpi4bVkA",
	"menu_mode_radio-check": "_34ARhZLfEh2uT9dxYFidB_",
	"menu_mode_check": "_2ft4kxYcwculyrUI0-ZeUA",
	"menu__group-title": "_3PhuiU9dj5mKQqfmBM_seN"
};

/***/ }),

/***/ "./node_modules/arui-feather/modernizr.js":
/*!************************************************!*\
  !*** ./node_modules/arui-feather/modernizr.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

;(function (window) {
  if (window) {
    /*! modernizr 3.3.1 (Custom Build) | MIT *
    * http://modernizr.com/download/?-inputtypes-pointerevents-touchevents !*/
    !function (e, t, n) {
      function o(e, t) {
        return (typeof e === "undefined" ? "undefined" : _typeof(e)) === t;
      }function i() {
        var e, t, n, i, s, r, a;for (var d in l) {
          if (l.hasOwnProperty(d)) {
            if (e = [], t = l[d], t.name && (e.push(t.name.toLowerCase()), t.options && t.options.aliases && t.options.aliases.length)) for (n = 0; n < t.options.aliases.length; n++) {
              e.push(t.options.aliases[n].toLowerCase());
            }for (i = o(t.fn, "function") ? t.fn() : t.fn, s = 0; s < e.length; s++) {
              r = e[s], a = r.split("."), 1 === a.length ? Modernizr[a[0]] = i : (!Modernizr[a[0]] || Modernizr[a[0]] instanceof Boolean || (Modernizr[a[0]] = new Boolean(Modernizr[a[0]])), Modernizr[a[0]][a[1]] = i), u.push((i ? "" : "no-") + a.join("-"));
            }
          }
        }
      }function s() {
        return "function" != typeof t.createElement ? t.createElement(arguments[0]) : p ? t.createElementNS.call(t, "http://www.w3.org/2000/svg", arguments[0]) : t.createElement.apply(t, arguments);
      }function r() {
        var e = t.body;return e || (e = s(p ? "svg" : "body"), e.fake = !0), e;
      }function a(e, n, o, i) {
        var a,
            l,
            d,
            u,
            p = "modernizr",
            c = s("div"),
            h = r();if (parseInt(o, 10)) for (; o--;) {
          d = s("div"), d.id = i ? i[o] : p + (o + 1), c.appendChild(d);
        }return a = s("style"), a.type = "text/css", a.id = "s" + p, (h.fake ? h : c).appendChild(a), h.appendChild(c), a.styleSheet ? a.styleSheet.cssText = e : a.appendChild(t.createTextNode(e)), c.id = p, h.fake && (h.style.background = "", h.style.overflow = "hidden", u = f.style.overflow, f.style.overflow = "hidden", f.appendChild(h)), l = n(c, e), h.fake ? (h.parentNode.removeChild(h), f.style.overflow = u, f.offsetHeight) : c.parentNode.removeChild(c), !!l;
      }var l = [],
          d = { _version: "3.3.1", _config: { classPrefix: "", enableClasses: !0, enableJSClass: !0, usePrefixes: !0 }, _q: [], on: function on(e, t) {
          var n = this;setTimeout(function () {
            t(n[e]);
          }, 0);
        }, addTest: function addTest(e, t, n) {
          l.push({ name: e, fn: t, options: n });
        }, addAsyncTest: function addAsyncTest(e) {
          l.push({ name: null, fn: e });
        } },
          Modernizr = function Modernizr() {};Modernizr.prototype = d, Modernizr = new Modernizr();var u = [],
          f = t.documentElement,
          p = "svg" === f.nodeName.toLowerCase(),
          c = s("input"),
          h = "search tel url email datetime date month week time datetime-local number range color".split(" "),
          m = {};Modernizr.inputtypes = function (e) {
        for (var o, i, s, r = e.length, a = "1)", l = 0; r > l; l++) {
          c.setAttribute("type", o = e[l]), s = "text" !== c.type && "style" in c, s && (c.value = a, c.style.cssText = "position:absolute;visibility:hidden;", /^range$/.test(o) && c.style.WebkitAppearance !== n ? (f.appendChild(c), i = t.defaultView, s = i.getComputedStyle && "textfield" !== i.getComputedStyle(c, null).WebkitAppearance && 0 !== c.offsetHeight, f.removeChild(c)) : /^(search|tel)$/.test(o) || (s = /^(url|email)$/.test(o) ? c.checkValidity && c.checkValidity() === !1 : c.value != a)), m[e[l]] = !!s;
        }return m;
      }(h);var v = "Moz O ms Webkit",
          y = d._config.usePrefixes ? v.toLowerCase().split(" ") : [];d._domPrefixes = y;var g = function () {
        function e(e, t) {
          var i;return e ? (t && "string" != typeof t || (t = s(t || "div")), e = "on" + e, i = e in t, !i && o && (t.setAttribute || (t = s("div")), t.setAttribute(e, ""), i = "function" == typeof t[e], t[e] !== n && (t[e] = n), t.removeAttribute(e)), i) : !1;
        }var o = !("onblur" in t.documentElement);return e;
      }();d.hasEvent = g, Modernizr.addTest("pointerevents", function () {
        var e = !1,
            t = y.length;for (e = Modernizr.hasEvent("pointerdown"); t-- && !e;) {
          g(y[t] + "pointerdown") && (e = !0);
        }return e;
      });var w = d._config.usePrefixes ? " -webkit- -moz- -o- -ms- ".split(" ") : [];d._prefixes = w;var b = d.testStyles = a;Modernizr.addTest("touchevents", function () {
        var n;if ("ontouchstart" in e || e.DocumentTouch && t instanceof DocumentTouch) n = !0;else {
          var o = ["@media (", w.join("touch-enabled),("), "heartz", ")", "{#modernizr{top:9px;position:absolute}}"].join("");b(o, function (e) {
            n = 9 === e.offsetTop;
          });
        }return n;
      }), i(), delete d.addTest, delete d.addAsyncTest;for (var C = 0; C < Modernizr._q.length; C++) {
        Modernizr._q[C]();
      }e.Modernizr = Modernizr;
    }(window, document);
    module.exports = window.Modernizr;
  } else {
    module.exports = {};
  }
})(typeof window !== 'undefined' ? window : false);
//# sourceMappingURL=modernizr.js.map


/***/ }),

/***/ "./node_modules/arui-feather/mq/index.js":
/*!***********************************************!*\
  !*** ./node_modules/arui-feather/mq/index.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

var _mq = __webpack_require__(/*! ./mq */ "./node_modules/arui-feather/mq/mq.js");

var _mq2 = _interopRequireDefault(_mq);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _mq2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                 * License, v. 2.0. If a copy of the MPL was not distributed with this
                                 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/mq/mq.js":
/*!********************************************!*\
  !*** ./node_modules/arui-feather/mq/mq.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _desc, _value, _class; /* This Source Code Form is subject to the terms of the Mozilla Public
                            * License, v. 2.0. If a copy of the MPL was not distributed with this
                            * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _modernizr = __webpack_require__(/*! ../modernizr */ "./node_modules/arui-feather/modernizr.js");

var _modernizr2 = _interopRequireDefault(_modernizr);

var _matchMedia = __webpack_require__(/*! ../lib/match-media */ "./node_modules/arui-feather/lib/match-media.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

var IS_BROWSER = typeof window !== 'undefined';
var SUPPORTS_TOUCH = IS_BROWSER && (_modernizr2.default.pointerevents || _modernizr2.default.touchevents);

/**
 * Компонент, имплементирующий поддержку медиа запросов в шаблонах.
 * Рендерит внутренние компоненты/разметку исходя из соответствия условиям запроса.
 * Для `query` используется window.matchMedia с полифиллом для IE9.
 * Можно использовать кастомные запросы из `src/mq/mq.json`, например `--small`.
 * Пока браузеры не поддерживают CSS4 Media Queries, поддержка тач-событий определяется через `touch`.
 */
var Mq = (_class = function (_React$Component) {
    _inherits(Mq, _React$Component);

    function Mq() {
        var _temp, _this, _ret;

        _classCallCheck(this, Mq);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.state = {
            isMatched: false
        }, _this.mql = null, _temp), _possibleConstructorReturn(_this, _ret);
    }

    Mq.prototype.componentDidMount = function componentDidMount() {
        this.mql = (0, _matchMedia.getMatchMedia)(this.props.query);
        this.mql.addListener(this.handleMatch);
        this.handleMatch(this.mql);
    };

    Mq.prototype.componentWillUnmount = function componentWillUnmount() {
        (0, _matchMedia.releaseMatchMedia)(this.props.query);
        this.mql.removeListener(this.handleMatch);
        this.mql = null;
    };

    Mq.prototype.render = function render() {
        if (!this.props.children || !IS_BROWSER || !this.state.isMatched) return false;

        return this.props.children;
    };

    /**
     * @param {Object} [mql] MediaQueryList или MediaQueryListEvent
     */


    Mq.prototype.handleMatch = function handleMatch(mql) {
        var _this2 = this;

        var queryPass = true;
        var touchPass = true;

        if (this.props.query) {
            queryPass = mql.matches;
        }
        if (this.props.touch) {
            touchPass = SUPPORTS_TOUCH;
        } else if (this.props.touch === false) {
            touchPass = !SUPPORTS_TOUCH;
        }

        this.setState({
            isMatched: queryPass && touchPass
        }, function () {
            if (_this2.props.onMatchChange) {
                _this2.props.onMatchChange(_this2.state.isMatched);
            }
        });
    };

    return Mq;
}(_react2.default.Component), (_applyDecoratedDescriptor(_class.prototype, 'handleMatch', [_autobind2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'handleMatch'), _class.prototype)), _class);
Mq.propTypes =  true ? {
    /** Медиа запрос */
    query: _propTypes2.default.string,
    /** Запрос на поддержку тач-событий */
    touch: _propTypes2.default.bool,
    /** Дочерние элементы `Mq` */
    children: _propTypes2.default.node,
    /**
     * Обработчик изменений в совпадении запросов
     * @param {boolean} isMatched
     */
    onMatchChange: _propTypes2.default.func
} : undefined;
exports.default = Mq;
//# sourceMappingURL=mq.js.map


/***/ }),

/***/ "./node_modules/arui-feather/mq/mq.json":
/*!**********************************************!*\
  !*** ./node_modules/arui-feather/mq/mq.json ***!
  \**********************************************/
/*! exports provided: --small, --small-only, --medium, --medium-only, --large, --large-only, --xlarge, --xlarge-only, --xxlarge, --xxlarge-only, --mobile-s, --mobile-m, --mobile-l, --mobile, --tablet-s, --tablet-m, --tablet, --desktop-s, --desktop-m, --desktop-l, --desktop-xl, --desktop, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"--small\":\"screen\",\"--small-only\":\"screen and (max-width: 47.9375em)\",\"--medium\":\"screen and (min-width: 48em)\",\"--medium-only\":\"screen and (min-width: 48em) and (max-width: 63.9375em)\",\"--large\":\"screen and (min-width: 64em)\",\"--large-only\":\"screen and (min-width: 64em) and (max-width: 89.9375em)\",\"--xlarge\":\"screen and (min-width: 90em)\",\"--xlarge-only\":\"screen and (min-width: 90em) and (max-width: 119.9375em)\",\"--xxlarge\":\"screen and (min-width: 120em)\",\"--xxlarge-only\":\"screen and (min-width: 120em) and (max-width: 99999999em)\",\"--mobile-s\":\"(min-width: 320px)\",\"--mobile-m\":\"(min-width: 375px)\",\"--mobile-l\":\"(min-width: 412px)\",\"--mobile\":\"(max-width: 599px)\",\"--tablet-s\":\"(min-width: 600px)\",\"--tablet-m\":\"(min-width: 768px)\",\"--tablet\":\"(min-width: 600px) and (max-width: 1023px)\",\"--desktop-s\":\"(min-width: 1024px)\",\"--desktop-m\":\"(min-width: 1280px)\",\"--desktop-l\":\"(min-width: 1440px)\",\"--desktop-xl\":\"(min-width: 1920px)\",\"--desktop\":\"(min-width: 1024px)\"}");

/***/ }),

/***/ "./node_modules/arui-feather/notification/index.js":
/*!*********************************************************!*\
  !*** ./node_modules/arui-feather/notification/index.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ../icon/ui/close */ "./node_modules/arui-feather/icon/ui/close/index.js");

__webpack_require__(/*! ../icon/ui/error */ "./node_modules/arui-feather/icon/ui/error/index.js");

__webpack_require__(/*! ../icon/ui/fail */ "./node_modules/arui-feather/icon/ui/fail/index.js");

__webpack_require__(/*! ../icon/ui/ok */ "./node_modules/arui-feather/icon/ui/ok/index.js");

__webpack_require__(/*! ../icon-button */ "./node_modules/arui-feather/icon-button/index.js");

__webpack_require__(/*! ./notification.css */ "./node_modules/arui-feather/notification/notification.css");

var _notification = __webpack_require__(/*! ./notification */ "./node_modules/arui-feather/notification/notification.js");

var _notification2 = _interopRequireDefault(_notification);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _notification2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                           * License, v. 2.0. If a copy of the MPL was not distributed with this
                                           * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/notification/notification.css":
/*!*****************************************************************!*\
  !*** ./node_modules/arui-feather/notification/notification.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"notification": "pm0jhTpy-6PMtQLbStq2P",
	"notification_stick-to_left": "_3N2SYV4kSP3ethMAE7Bg2I",
	"notification_stick-to_right": "_1AEQMRkaIDhmSU9LiYxedf",
	"notification_theme_alfa-on-color": "_3M2BkNCnA_mOMcpTfPCoOr",
	"notification_has-closer": "_3-kZd9xp8AiLyzSgeiyeTV",
	"notification__title": "_1Iwlf_uoF_y8UG3azec4bm",
	"notification__content": "_1YD8DjVvs86ISR7yTAn9Af",
	"notification__icon": "_3tZdKChtehop3CWmxeQdv",
	"notification__closer": "_2c2SllPTbgMVUXogZ3seDo",
	"notification_status_ok": "_1k3_frJcFxHpLuBWCBQNjR",
	"notification_status_error": "H3iVSrjQcoMlJ2ZbE8DQ7",
	"notification_status_fail": "kdAsvMg5bcA7MeOdIj6eo",
	"notification_visible": "_29NgFKUMTPzTpVKx_Qsdki"
};

/***/ }),

/***/ "./node_modules/arui-feather/notification/notification.js":
/*!****************************************************************!*\
  !*** ./node_modules/arui-feather/notification/notification.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _dec, _dec2, _class, _desc, _value, _class2, _class3, _temp2; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                   * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                   * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* eslint jsx-a11y/no-static-element-interactions: 0 */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _close = __webpack_require__(/*! ../icon/ui/close */ "./node_modules/arui-feather/icon/ui/close/index.js");

var _close2 = _interopRequireDefault(_close);

var _error = __webpack_require__(/*! ../icon/ui/error */ "./node_modules/arui-feather/icon/ui/error/index.js");

var _error2 = _interopRequireDefault(_error);

var _fail = __webpack_require__(/*! ../icon/ui/fail */ "./node_modules/arui-feather/icon/ui/fail/index.js");

var _fail2 = _interopRequireDefault(_fail);

var _ok = __webpack_require__(/*! ../icon/ui/ok */ "./node_modules/arui-feather/icon/ui/ok/index.js");

var _ok2 = _interopRequireDefault(_ok);

var _iconButton = __webpack_require__(/*! ../icon-button/icon-button */ "./node_modules/arui-feather/icon-button/icon-button.js");

var _iconButton2 = _interopRequireDefault(_iconButton);

var _swipeable = __webpack_require__(/*! ../swipeable */ "./node_modules/arui-feather/swipeable/index.js");

var _swipeable2 = _interopRequireDefault(_swipeable);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _window = __webpack_require__(/*! ../lib/window */ "./node_modules/arui-feather/lib/window.js");

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

/**
 * Компонент всплывающего окна.
 */
var Notification = (_dec = (0, _cn2.default)('notification'), _dec2 = (0, _performance2.default)(), _dec(_class = _dec2(_class = (_class2 = (_temp2 = _class3 = function (_React$Component) {
    _inherits(Notification, _React$Component);

    function Notification() {
        var _temp, _this, _ret;

        _classCallCheck(this, Notification);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.state = {
            hovered: false
        }, _this.closeTimeout = null, _this.clickEventBindTimeout = null, _this.isWindowClickBinded = false, _temp), _possibleConstructorReturn(_this, _ret);
    }

    Notification.prototype.componentDidMount = function componentDidMount() {
        this.startCloseTimer();

        if (this.props.onClickOutside) {
            this.ensureClickEvent();
        }
    };

    Notification.prototype.componentDidUpdate = function componentDidUpdate(prevProps) {
        if (prevProps.onClickOutside !== this.props.onClickOutside) {
            this.ensureClickEvent();
        } else if (prevProps.visible !== this.props.visible) {
            this.ensureClickEvent(!this.props.visible);
        }
    };

    Notification.prototype.componentWillUnmount = function componentWillUnmount() {
        this.stopCloseTimer();

        if (this.props.onClickOutside) {
            this.ensureClickEvent(true);
        }
    };

    Notification.prototype.render = function render(cn) {
        var _this2 = this;

        var ToggledIcon = void 0;

        switch (this.props.status) {
            case 'error':
                ToggledIcon = _error2.default;break;
            case 'fail':
                ToggledIcon = _fail2.default;break;
            case 'ok':
                ToggledIcon = _ok2.default;break;
            default:
                ToggledIcon = _ok2.default;break;
        }

        return _react2.default.createElement(
            _swipeable2.default,
            { onSwipe: this.handleSwipe },
            _react2.default.createElement(
                'div',
                {
                    ref: function ref(root) {
                        _this2.root = root;
                    },
                    className: cn({
                        visible: this.props.visible,
                        status: this.props.status,
                        hovered: this.state.hovered,
                        'has-closer': this.props.hasCloser,
                        'stick-to': this.props.stickTo
                    }),
                    id: this.props.id,
                    style: this.getPosition(),
                    onMouseEnter: this.handleMouseEnter,
                    onMouseLeave: this.handleMouseLeave,
                    onClick: this.handleClick,
                    onKeyDown: this.handleKeyDown,
                    'data-test-id': this.props['data-test-id']
                },
                _react2.default.createElement(
                    'div',
                    { className: cn('icon') },
                    this.props.icon || _react2.default.createElement(ToggledIcon, {
                        colored: this.props.status === 'ok' || this.props.status === 'error',
                        theme: this.props.theme === 'alfa-on-color' ? 'alfa-on-white' : 'alfa-on-color',
                        size: 'm'
                    })
                ),
                this.props.title && _react2.default.createElement(
                    'div',
                    { className: cn('title') },
                    this.props.title
                ),
                this.props.children && _react2.default.createElement(
                    'div',
                    { className: cn('content') },
                    this.props.children
                ),
                this.props.hasCloser && _react2.default.createElement(
                    _iconButton2.default,
                    {
                        className: cn('closer'),
                        size: 'm',
                        onClick: this.handleCloserClick
                    },
                    _react2.default.createElement(_close2.default, {
                        size: 's',
                        theme: this.props.theme === 'alfa-on-color' ? 'alfa-on-white' : 'alfa-on-color'
                    })
                )
            )
        );
    };

    Notification.prototype.handleSwipe = function handleSwipe(direction) {
        if (direction === 'left' || direction === 'right' || direction === 'top') {
            this.handleCloserClick();
        }
    };

    Notification.prototype.handleCloserClick = function handleCloserClick(event) {
        if (this.props.onCloserClick) {
            this.props.onCloserClick(event);
        }
    };

    Notification.prototype.handleKeyDown = function handleKeyDown(event) {
        if (this.props.onKeyDown) {
            this.props.onKeyDown(event);
        }
    };

    Notification.prototype.handleMouseEnter = function handleMouseEnter(event) {
        this.setState({ hovered: true });
        this.stopCloseTimer();

        if (this.props.onMouseEnter) {
            this.props.onMouseEnter(event);
        }
    };

    Notification.prototype.handleMouseLeave = function handleMouseLeave(event) {
        this.setState({ hovered: false });
        this.stopCloseTimer();
        this.startCloseTimer();

        if (this.props.onMouseLeave) {
            this.props.onMouseLeave(event);
        }
    };

    Notification.prototype.handleClick = function handleClick(event) {
        if (this.props.onClick) {
            this.props.onClick(event);
        }
    };

    Notification.prototype.handleWindowClick = function handleWindowClick(event) {
        if (this.props.onClickOutside && this.root && (0, _window.isNodeOutsideElement)(event.target, this.root)) {
            this.props.onClickOutside(event);
        }
    };

    Notification.prototype.getPosition = function getPosition() {
        return { top: this.props.offset };
    };

    Notification.prototype.startCloseTimer = function startCloseTimer() {
        var _this3 = this;

        this.closeTimeout = setTimeout(function () {
            if (_this3.props.onCloseTimeout) {
                _this3.props.onCloseTimeout();
            }
        }, this.props.autoCloseDelay);
    };

    Notification.prototype.stopCloseTimer = function stopCloseTimer() {
        clearTimeout(this.closeTimeout);
        this.closeTimeout = null;
    };

    Notification.prototype.ensureClickEvent = function ensureClickEvent(isDestroy) {
        var _this4 = this;

        var isNeedBindEvent = isDestroy !== undefined ? !isDestroy : this.props.visible;

        // We need timeouts to not to catch the event that causes
        // popup opening (because it propagates to the `window`).
        if (this.clickEventBindTimeout) {
            clearTimeout(this.clickEventBindTimeout);
            this.clickEventBindTimeout = null;
        }

        this.clickEventBindTimeout = setTimeout(function () {
            if (!_this4.isWindowClickBinded && isNeedBindEvent) {
                window.addEventListener('click', _this4.handleWindowClick);
                window.addEventListener('touchend', _this4.handleWindowClick);
                _this4.isWindowClickBinded = true;
            } else if (_this4.isWindowClickBinded && !isNeedBindEvent) {
                window.removeEventListener('click', _this4.handleWindowClick);
                window.removeEventListener('touchend', _this4.handleWindowClick);
                _this4.isWindowClickBinded = false;
            }
        }, 0);
    };

    return Notification;
}(_react2.default.Component), _class3.defaultProps = {
    autoCloseDelay: 5000,
    stickTo: 'right',
    offset: 12,
    hasCloser: true
}, _temp2), (_applyDecoratedDescriptor(_class2.prototype, 'handleSwipe', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleSwipe'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleCloserClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleCloserClick'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleKeyDown', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleKeyDown'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseEnter', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseEnter'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseLeave', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseLeave'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleClick'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleWindowClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleWindowClick'), _class2.prototype)), _class2)) || _class) || _class);
Notification.propTypes =  true ? {
    /** Тип компонента */
    status: _propTypes2.default.oneOf(['error', 'fail', 'ok']),
    /** Управление видимостью компонента */
    visible: _propTypes2.default.bool,
    /** Отступ от верхнего края */
    offset: _propTypes2.default.number,
    /** К какому краю прижат попап */
    stickTo: _propTypes2.default.oneOf(['left', 'right']),
    /** Управляет отображением кнопки закрытия уведомления */
    hasCloser: _propTypes2.default.bool,
    /** Дочерние элементы `Notification` */
    children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]),
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /** Идентификатор компонента в DOM */
    id: _propTypes2.default.string,
    /** Заголовок сообщения */
    title: _propTypes2.default.node,
    /** Замена стандартной иконки */
    icon: _propTypes2.default.node,
    /** Время до закрытия компонента */
    autoCloseDelay: _propTypes2.default.number,
    /** Обработчик события истечения времени до закрытия компонента */
    onCloseTimeout: _propTypes2.default.func,
    /**
     * Обработчик клика по крестику компонента
     * @param {React.MouseEvent} event
     */
    onCloserClick: _propTypes2.default.func,
    /**
     * Обработчик события нажатия на клавишу клавиатуры в момент, когда фокус находится на компоненте
     * @param {React.KeyboardEvent} event
     */
    onKeyDown: _propTypes2.default.func,
    /**
     * Обработчик события наведения курсора на попап
     * @param {React.MouseEvent} event
     */
    onMouseEnter: _propTypes2.default.func,
    /**
     * Обработчик события снятия курсора с попапа
     * @param {React.MouseEvent} event
     */
    onMouseLeave: _propTypes2.default.func,
    /**
     * Обработчик клика вне компонента
     * @param {React.MouseEvent} event
     */
    onClickOutside: _propTypes2.default.func,
    /**
     * Обработчик клика по компоненту
     * @param {React.MouseEvent} event
     */
    onClick: _propTypes2.default.func,
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = Notification;
//# sourceMappingURL=notification.js.map


/***/ }),

/***/ "./node_modules/arui-feather/performance.js":
/*!**************************************************!*\
  !*** ./node_modules/arui-feather/performance.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                                                                                                                                                                                                                               * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                                                                                                                                                                                                                               * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

exports.isEqual = isEqual;
exports.default = performance;

var _react = __webpack_require__(/*! react */ "react");

var hasOwnProperty = Object.prototype.hasOwnProperty;

/**
 * Функции проверки равенства двух объектов.
 *
 * @param {*} objA Первый объект
 * @param {*} objB Второй объект
 * @param {Boolean} [deep=false] Запускать ли глубокую проверку равенства
 * @returns {Boolean}
 */

function isEqual(objA, objB) {
    var deep = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    if (Object.is(objA, objB)) {
        return true;
    }

    if ((typeof objA === 'undefined' ? 'undefined' : _typeof(objA)) !== 'object' || objA === null || (typeof objB === 'undefined' ? 'undefined' : _typeof(objB)) !== 'object' || objB === null) {
        return false;
    }

    if (objA.prototype !== objB.prototype) {
        return false;
    }

    var keysA = Object.keys(objA);
    var keysB = Object.keys(objB);

    if (keysA.length !== keysB.length) {
        return false;
    }

    var bHasOwnProperty = hasOwnProperty.bind(objB);
    while (keysA.length > 0) {
        var key = keysA.pop();

        if (!bHasOwnProperty(key)) {
            return false;
        }

        var a = objA[key];
        var b = objB[key];

        if (!Object.is(a, b)) {
            if (!deep || (typeof a === 'undefined' ? 'undefined' : _typeof(a)) !== 'object' || (typeof b === 'undefined' ? 'undefined' : _typeof(b)) !== 'object' || a === null || b === null) {
                return false;
            }

            // https://github.com/erikras/redux-form/issues/3461
            // https://github.com/erikras/redux-form/pull/3481
            if ((0, _react.isValidElement)(a) || (0, _react.isValidElement)(b)) return false;

            if (!isEqual(a, b, deep)) {
                return false;
            }
        }
    }

    return true;
}

/**
 * Поверхностная проверка равенства props и state компонента.
 *
 * @param {*} nextProps next component props
 * @param {*} nextState next component state
 * @param {*} nextContext next component context
 * @returns {Boolean}
 */
function shallow(nextProps, nextState, nextContext) {
    return !isEqual(this.props, nextProps) || !isEqual(this.state, nextState) || !isEqual(this.context, nextContext);
}

/**
 * Запускает глубокую проверку равенства props и state компонента.
 * Глубокая проверка менее производительна, но позволяет проверять равенство массивов и объектов.
 *
 * @param {*} nextProps next component props
 * @param {*} nextState next component state
 * @param {*} nextContext next component context
 * @returns {Boolean}
 */
function deep(nextProps, nextState, nextContext) {
    return !isEqual(this.props, nextProps, true) || !isEqual(this.state, nextState, true) || !isEqual(this.context, nextContext, true);
}

/**
 * Декоратор для улучшения производительности React компонентов. Работает за счет реализации метода
 * [shouldComponentUpdate](https://facebook.github.io/react/docs/advanced-performance.html#avoiding-reconciling-the-dom)
 *
 * У декоратора есть два режима работы - глубокая и "поверхностная" проверка. В случае, если все props и state
 * компонента состоит только из примитивных значений (`number`, `string`, `null`, `undefined`) стоит использовать
 * поверхностную проверку, которая будет проверять простое равенство значений в `props` и `state`.

 * В случае, если props или state компонентов имеют сложную структуру (массивы, объекты) необходимо использовать
 * глубокую проверку.
 *
 * @param {Boolean} [useDeep=false] Использовать глубокую проверку равенства
 * @returns {Function}
 */
function performance() {
    var useDeep = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

    return function (target) {
        target.prototype.shouldComponentUpdate = useDeep ? deep : shallow;
    };
}
//# sourceMappingURL=performance.js.map


/***/ }),

/***/ "./node_modules/arui-feather/popup-header/index.js":
/*!*********************************************************!*\
  !*** ./node_modules/arui-feather/popup-header/index.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ../icon/ui/close */ "./node_modules/arui-feather/icon/ui/close/index.js");

__webpack_require__(/*! ../icon-button/icon-button */ "./node_modules/arui-feather/icon-button/icon-button.js");

__webpack_require__(/*! ./popup-header.css */ "./node_modules/arui-feather/popup-header/popup-header.css");

var _popupHeader = __webpack_require__(/*! ./popup-header */ "./node_modules/arui-feather/popup-header/popup-header.js");

var _popupHeader2 = _interopRequireDefault(_popupHeader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

exports.default = _popupHeader2.default;
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/popup-header/popup-header.css":
/*!*****************************************************************!*\
  !*** ./node_modules/arui-feather/popup-header/popup-header.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"popup-header": "_3NeBnNnF4_ZjI1w2ZA93dk",
	"popup-header__title": "VRmJi82ORFZ-9ZrsOXVUL",
	"popup-header__closer": "_30u1pcYPtGspfVBpqwq9lg",
	"popup-header_size_s": "_1Ndr-BzJtceHpgCoVwH0us",
	"popup-header_size_m": "_1BJdgAruZSLvkMKhN0V2Gq",
	"popup-header_size_l": "_1-y3RUM8HJes1PQtQM5yuP",
	"popup-header_size_xl": "_2k7scTiTiC9oOWy43xU7-E"
};

/***/ }),

/***/ "./node_modules/arui-feather/popup-header/popup-header.js":
/*!****************************************************************!*\
  !*** ./node_modules/arui-feather/popup-header/popup-header.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _dec, _dec2, _class, _desc, _value, _class2; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                  * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _close = __webpack_require__(/*! ../icon/ui/close */ "./node_modules/arui-feather/icon/ui/close/index.js");

var _close2 = _interopRequireDefault(_close);

var _iconButton = __webpack_require__(/*! ../icon-button/icon-button */ "./node_modules/arui-feather/icon-button/icon-button.js");

var _iconButton2 = _interopRequireDefault(_iconButton);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

/**
 * Заголовок в Popup.
 */
var PopupHeader = (_dec = (0, _cn2.default)('popup-header'), _dec2 = (0, _performance2.default)(), _dec(_class = _dec2(_class = (_class2 = function (_React$Component) {
    _inherits(PopupHeader, _React$Component);

    function PopupHeader() {
        _classCallCheck(this, PopupHeader);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    PopupHeader.prototype.render = function render(cn) {
        return _react2.default.createElement(
            'div',
            {
                className: cn({
                    size: this.props.size
                }),
                id: this.props.id,
                'data-test-id': this.props['data-test-id']
            },
            _react2.default.createElement(
                _iconButton2.default,
                {
                    className: cn('closer'),
                    size: this.props.size,
                    onClick: this.handleCloserClick
                },
                _react2.default.createElement(_close2.default, {
                    size: this.props.size
                })
            ),
            _react2.default.createElement(
                'div',
                { className: cn('title') },
                this.props.title
            )
        );
    };

    PopupHeader.prototype.handleCloserClick = function handleCloserClick(event) {
        if (this.props.onCloserClick) {
            this.props.onCloserClick(event);
        }
    };

    return PopupHeader;
}(_react2.default.Component), (_applyDecoratedDescriptor(_class2.prototype, 'handleCloserClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleCloserClick'), _class2.prototype)), _class2)) || _class) || _class);
PopupHeader.propTypes =  true ? {
    /** Размер компонента */
    size: _propTypes2.default.oneOf(['s', 'm', 'l', 'xl']),
    /** Содержимое заголовка */
    title: _propTypes2.default.node,
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /** Идентификатор компонента в DOM */
    id: _propTypes2.default.string,
    /**
     * Обработчик клика по кнопке закрытия
     * @param {React.MouseEvent} event
     */
    onCloserClick: _propTypes2.default.func,
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = PopupHeader;
//# sourceMappingURL=popup-header.js.map


/***/ }),

/***/ "./node_modules/arui-feather/popup/calc-drawing-params.js":
/*!****************************************************************!*\
  !*** ./node_modules/arui-feather/popup/calc-drawing-params.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.calcTargetDimensions = calcTargetDimensions;
exports.calcFitContainerDimensions = calcFitContainerDimensions;
exports.calcBestDrawingParams = calcBestDrawingParams;

var _vars = __webpack_require__(/*! ../vars */ "./node_modules/arui-feather/vars.js");

var VIEWPORT_ACCURACY_FACTOR = 0.99; /* This Source Code Form is subject to the terms of the Mozilla Public
                                      * License, v. 2.0. If a copy of the MPL was not distributed with this
                                      * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var VIEWPORT_PADDING = 20;

var DEFAULT_DIRECTIONS = ['bottom-left', 'bottom-center', 'bottom-right', 'top-left', 'top-center', 'top-right', 'right-top', 'right-center', 'right-bottom', 'left-top', 'left-center', 'left-bottom'];

/**
 * @typedef {Object} DrawingParams
 * @property {String} direction Направление отрисовки
 * @property {Number} left Положение по горизонтали
 * @property {Number} top Положение по вертикали
 */

/**
 * @typedef {Object} PopupDimension
 * @property {Number} width Ширина попапа
 * @property {Number} height Высота попапа
 * @property {Number} area Площадь попапа
 */

/**
 * @typedef {Object} Rect
 * @property {Number} top
 * @property {Number} left
 * @property {Number} [bottom]
 * @property {Number} [right]
 * @property {Number} [width]
 * @property {Number} [height]
 */

/**
 * @param {String} direction Направление отрисовки
 * @returns {String|Boolean}
 */
function getMainDirection(direction) {
    var deliveryPosition = direction.indexOf('-');
    return deliveryPosition !== -1 && direction.substr(0, deliveryPosition);
}

/**
 * @param {String} direction Направление отрисовки
 * @returns {String|Boolean}
 */
function getSecondaryDirection(direction) {
    var deliveryPosition = direction.indexOf('-');
    return deliveryPosition !== -1 && direction.substr(deliveryPosition + 1);
}

/**
 * Высчитывает координаты блока, который является таргетом поп-апа.
 *
 * @param {PopupHash} popup Набор параметров попапа
 * @returns {Rect}
 */
function calcTargetDimensions(popup) {
    if (!popup.isTargetAnchor && popup.targetPosition) {
        return {
            left: popup.targetPosition.left,
            top: popup.targetPosition.top,
            width: 0,
            height: 0
        };
    }

    var anchor = popup.targetAnchor;
    var anchorRect = anchor.getBoundingClientRect();
    var fitContainer = popup.fitContainer;


    if (fitContainer) {
        var fitContainerRect = fitContainer.getBoundingClientRect();

        return {
            left: anchorRect.left - fitContainerRect.left + fitContainer.scrollLeft,
            top: anchorRect.top - fitContainerRect.top + fitContainer.scrollTop,
            width: anchorRect.width,
            height: anchorRect.height
        };
    }

    return {
        left: anchorRect.left + window.pageXOffset,
        top: anchorRect.top + window.pageYOffset,
        width: anchorRect.width,
        height: anchorRect.height
    };
}

/**
 * Рассчитывает размеры и координаты расположения viewport.
 *
 * @returns {Rect}
 */
function calcViewportDimensions() {
    var winTop = window.pageYOffset;
    var winLeft = window.pageXOffset;
    var winWidth = window.innerWidth;
    var winHeight = window.innerHeight;

    return {
        top: winTop,
        left: winLeft,
        bottom: winTop + winHeight,
        right: winLeft + winWidth
    };
}

/**
 * Высчитывает размеры и координаты расположения контейнера, в котором рендерится Popup.
 *
 * @param {HTMLElement} container контейнер, в котором будет позиционирован Popup
 * @returns {Rect}
 */
function calcContainerDimensions(container) {
    var containerTop = container.scrollTop;
    var containerLeft = container.scrollLeft;
    var containerHeight = container.offsetHeight;
    var containerWidth = container.offsetWidth;

    return {
        top: containerTop,
        left: containerLeft,
        bottom: containerTop + containerHeight,
        right: containerLeft + containerWidth
    };
}

/**
 * Определяет контейнер, в котором рендерится Popup и возвращает его размеры и координаты.
 *
 * @param {PopupHash} popup Набор параметров попапа
 * @returns {Rect}
 */
function calcFitContainerDimensions(popup) {
    var fitContainer = popup.fitContainer;


    return fitContainer ? calcContainerDimensions(fitContainer) : calcViewportDimensions();
}

/**
 * Рассчитывает пересечения Popup и контейнера, в котором он рендерится.
 *
 * @param {Point} position Позиция
 * @param {Rect} fitContainerDimensions Размеры контейнера, в который будет
 * рендерится Popup
 * @param {PopupDimension} popupDimensions Размеры попапа
 * @param {PopupHash} popupOffsetFitContainer Минимально допустимое смещение в
 * пикселях всплывающего окна от края его контейнера
 * @returns {Number}
 */
function calcFitContainerFactor(position, fitContainerDimensions, popupDimensions, popupOffsetFitContainer) {
    var intersectionLeft = Math.max(position.left, fitContainerDimensions.left + popupOffsetFitContainer);

    var intersectionRight = Math.min(position.left + popupDimensions.width, fitContainerDimensions.right - popupOffsetFitContainer);

    var intersectionTop = Math.max(position.top, fitContainerDimensions.top + popupOffsetFitContainer);

    var intersectionBottom = Math.min(position.top + popupDimensions.height, fitContainerDimensions.bottom - popupOffsetFitContainer);

    if (intersectionLeft < intersectionRight && intersectionTop < intersectionBottom) {
        return (intersectionRight - intersectionLeft) * (intersectionBottom - intersectionTop) / popupDimensions.area;
    }

    return 0;
}

/**
 * Возвращает значения для позиционирования Popup в конкретной позиции.
 *
 * @param {String} direction Направление отрисовки
 * @param {Rect} targetDimensions Область отрисовки якорного элемента
 * @param {PopupDimension} popupDimensions Размеры попапа
 * @param {PopupHash} popup Набор параметров попапа
 * @returns {Point}
 */
function calcPos(direction, targetDimensions, popupDimensions, popup) {
    var result = {};
    var mainOffset = popup.offset.main || popup.isHaveTooltip && _vars.POPUP_MAIN_OFFSET || 0;
    var secondaryOffset = popup.offset.second;

    var mainDirection = getMainDirection(direction);
    var secondaryDirection = getSecondaryDirection(direction);

    switch (mainDirection) {
        case 'bottom':
            result.top = targetDimensions.top + targetDimensions.height + mainOffset;
            break;
        case 'top':
            result.top = targetDimensions.top - popupDimensions.height - mainOffset;
            break;
        case 'left':
            result.left = targetDimensions.left - popupDimensions.width - mainOffset;
            break;
        case 'right':
            result.left = targetDimensions.left + targetDimensions.width + mainOffset;
            break;
        default:
            break;
    }

    switch (secondaryDirection) {
        case 'right':
            result.left = targetDimensions.left + targetDimensions.width - popupDimensions.width - secondaryOffset;
            break;
        case 'left':
            result.left = targetDimensions.left + secondaryOffset;
            break;
        case 'bottom':
            result.top = targetDimensions.top + targetDimensions.height - popupDimensions.height - secondaryOffset;
            break;
        case 'top':
            result.top = targetDimensions.top + secondaryOffset;
            break;
        case 'center':
            switch (mainDirection) {
                case 'top':
                case 'bottom':
                    result.left = targetDimensions.left + (targetDimensions.width / 2 - popupDimensions.width / 2);
                    break;
                case 'left':
                case 'right':
                    result.top = targetDimensions.top + (targetDimensions.height / 2 - popupDimensions.height / 2);
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }

    return result;
}

/**
 * Рассчитывает наилучшие значения для позиционирования Popup.
 *
 * @param {PopupHash} popup Набор параметров попапа
 * @param {Rect} targetDimensions Область отрисовки якорного элемента
 * @param {Rect} fitContainerDimensions Размеры вьюпорта
 * @returns {DrawingParams}
 */
function calcBestDrawingParams(popup, targetDimensions, fitContainerDimensions) {
    var popupDimensions = {
        width: popup.width,
        height: popup.height,
        area: popup.width * popup.height
    };
    var directions = popup.directions ? popup.directions : popup.isHeightAvailable && ['top-left'] || DEFAULT_DIRECTIONS;
    var i = 0;
    var direction = '';
    var heightToEdge = 0;
    var height = 0;
    var overflow = false;
    var newPopupDimensions = {};
    var position = {};
    var fitContainerFactor = 0;
    var bestDirection = '';
    var bestPosition = {};
    var bestHeight = 0;
    var bestFitContainerFactor = 0;

    while (directions[i]) {
        direction = directions[i];
        i += 1;

        if (getMainDirection(direction) === 'top') {
            heightToEdge = targetDimensions.top - fitContainerDimensions.top;
        } else if (getMainDirection(direction) === 'bottom') {
            heightToEdge = fitContainerDimensions.bottom - (targetDimensions.top + targetDimensions.height);
        }

        if (popup.isHeightAdaptive) {
            if (popup.contentHeight > heightToEdge) {
                height = heightToEdge;
                overflow = true;
            } else {
                height = popup.contentHeight;
                overflow = false;
            }

            newPopupDimensions = {
                width: popup.width,
                height: height,
                area: popup.width * height
            };
        }

        position = calcPos(direction, targetDimensions, height ? newPopupDimensions : popupDimensions, popup);
        fitContainerFactor = calcFitContainerFactor(position, fitContainerDimensions, popupDimensions, popup.offset.fitContainer);

        if (i === 1 || fitContainerFactor > bestFitContainerFactor || !bestFitContainerFactor && popup.bestDirection) {
            bestFitContainerFactor = fitContainerFactor;
            bestDirection = direction;
            bestPosition = position;
            bestHeight = height;
        }

        if (bestFitContainerFactor > VIEWPORT_ACCURACY_FACTOR) {
            break;
        }
    }

    if (popup.isHeightAdaptive && popup.contentHeight > heightToEdge) {
        if (getMainDirection(bestDirection) === 'top') {
            bestPosition.top += VIEWPORT_PADDING;
            bestHeight -= VIEWPORT_PADDING;
        } else if (getMainDirection(bestDirection) === 'bottom') {
            bestHeight -= VIEWPORT_PADDING;
        }
    }

    return {
        direction: bestDirection,
        left: bestPosition.left,
        top: bestPosition.top,
        height: bestHeight || 'auto',
        overflow: overflow
    };
}
//# sourceMappingURL=calc-drawing-params.js.map


/***/ }),

/***/ "./node_modules/arui-feather/popup/index.js":
/*!**************************************************!*\
  !*** ./node_modules/arui-feather/popup/index.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ../render-in-container */ "./node_modules/arui-feather/render-in-container/index.js");

__webpack_require__(/*! ../resize-sensor */ "./node_modules/arui-feather/resize-sensor/index.js");

__webpack_require__(/*! ./popup.css */ "./node_modules/arui-feather/popup/popup.css");

__webpack_require__(/*! ./popup_theme_alfa-on-white.css */ "./node_modules/arui-feather/popup/popup_theme_alfa-on-white.css");

__webpack_require__(/*! ./popup_theme_alfa-on-color.css */ "./node_modules/arui-feather/popup/popup_theme_alfa-on-color.css");

var _popup = __webpack_require__(/*! ./popup */ "./node_modules/arui-feather/popup/popup.js");

var _popup2 = _interopRequireDefault(_popup);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

exports.default = _popup2.default;
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/popup/popup.css":
/*!***************************************************!*\
  !*** ./node_modules/arui-feather/popup/popup.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"popup": "kEGSwVpcSloDGN9GcWbVa",
	"popup__container": "_18Evag0ZK1WOtLkbkU_jUU",
	"popup_size_s": "_2uoaykjCHTI3aVOIRQpq36",
	"popup_size_m": "_1QwqDrLX4YMd8yn_4EQRB9",
	"popup_size_l": "_3NrBK7VI2eqw99-JEzOK96",
	"popup_size_xl": "_1BCYjYWagtll0Z-JFIejXc",
	"popup_visible": "_3KGuSwEz1qUcGeRkq0bTc3",
	"popup_height_available": "_2EJDSvdDUd3s4h9nGEZ1uA",
	"popup__content": "_3PZedhd1Qxiwta9C9G9ctm",
	"popup_height_adaptive": "_2drC84fj_l4unXgmGN8fHR",
	"popup__gradient": "u5pZrNZkmd-6R-N-wULeu",
	"popup_target_screen": "_1kAi4ruYJj0jGoNOfiA8Iu",
	"popup__gradient_top": "_2TAVRM_tZqC0x8Q5OmTzJe",
	"popup__gradient_bottom": "YdErv_ZP4U6131ekG0e9w",
	"popup__inner": "_2_O2nYf_PPlYiiIj9F_uin",
	"popup_padded": "_19MRBrcIIa1Su8wUIjwiCV",
	"popup_type_tooltip": "_357FBJ487J0Spdpert2wvd",
	"popup_direction_top-left": "_14G-gDkkthRznqq6sxjzWh",
	"popup_direction_top-center": "eww_kMwy5oqXLuBe0NBSR",
	"popup_direction_top-right": "_1HPCMeFdhUrdn5dYnYLker",
	"popup_direction_left-top": "_1cS8EBCa1Za5NtXLhX5jZd",
	"popup_direction_left-center": "U_EAOvH0KEjz3ITxcYheh",
	"popup_direction_left-bottom": "_12gHwhMPt3RdrBjU9OxOuM",
	"popup_direction_right-top": "Paj3u7NIM8xFF_5l1wHxk",
	"popup_direction_right-center": "_3xzk3dbjibxRWfeDzDqYY3",
	"popup_direction_right-bottom": "_3IdN7uaj5gWoNqnIwKWVuv",
	"popup_direction_bottom-left": "_11R1e0jPDZHZs8JCR19XL1",
	"popup_direction_bottom-center": "GlKOZsn2p29rq3XUg9Hf7",
	"popup_direction_bottom-right": "_2IUlsTq1lKbppLmAZLZiRe"
};

/***/ }),

/***/ "./node_modules/arui-feather/popup/popup.js":
/*!**************************************************!*\
  !*** ./node_modules/arui-feather/popup/popup.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _dec, _dec2, _class, _desc, _value, _class2, _class3, _temp2; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                   * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                   * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* eslint-disable max-len */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _lodash = __webpack_require__(/*! lodash.debounce */ "lodash.debounce");

var _lodash2 = _interopRequireDefault(_lodash);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = __webpack_require__(/*! react-dom */ "react-dom");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _resizeSensor = __webpack_require__(/*! ../resize-sensor/resize-sensor */ "./node_modules/arui-feather/resize-sensor/resize-sensor.js");

var _resizeSensor2 = _interopRequireDefault(_resizeSensor);

var _calcDrawingParams = __webpack_require__(/*! ./calc-drawing-params */ "./node_modules/arui-feather/popup/calc-drawing-params.js");

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _propTypes3 = __webpack_require__(/*! ../lib/prop-types */ "./node_modules/arui-feather/lib/prop-types.js");

var _window = __webpack_require__(/*! ../lib/window */ "./node_modules/arui-feather/lib/window.js");

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

/**
 * @typedef {Object} Point
 * @property {Number} left Координата по оси x
 * @property {Number} top Координата по оси y
 */

/**
 * @typedef {Object} PopupOffsetCollection
 * @property {Number} main Смещение в пикселях всплывающего окна относительно основного направления
 * @property {Number} second Смещение в пикселях всплывающего окна относительно второстепенного направления
 * @property {Number} viewport Минимально допустимое смещение в пикселях всплывающего окна от края окна браузера
 */

/**
 * @typedef {Object} PopupHash
 * @property {Array.<String>} directions Список возмножных расположений попапа
 * @property {String} bestDirection Приоритетное расположение
 * @property {Boolean} isHeightAdaptive Подстраивается ли высота попапа под край окна
 * @property {Boolean} isHeightAvailable Занимает ли попап всю возможную высоту
 * @property {Boolean} isTargetAnchor Привязан ли попап к другому элементу
 * @property {Boolean} isHaveTooltip Имеет ли попап тип "tooltip"
 * @property {Number} width Ширина попапа
 * @property {Number} height Высота попапа
 * @property {Number} contentWidth Ширина контента в попапе
 * @property {Number} contentHeight Высота контента в попапе
 * @property {PopupOffsetCollection} offset Список смещений попапа
 * @property {Point} targetPosition Координаты точки привязки попапа
 * @property {HTMLElement} targetAnchor Объект элемента, к которому привязан попап, в DOM дереве
 */

/**
 * Компонент popup'а.
 */
var Popup = (_dec = (0, _cn2.default)('popup'), _dec2 = (0, _performance2.default)(true), _dec(_class = _dec2(_class = (_class2 = (_temp2 = _class3 = function (_React$Component) {
    _inherits(Popup, _React$Component);

    function Popup() {
        var _temp, _this, _ret;

        _classCallCheck(this, Popup);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.state = {
            direction: null,
            hasScrollbar: false,
            receivedContainer: false,
            styles: {
                top: 0,
                left: 0,
                height: 'auto'
            },
            topGradientStyles: {
                width: '100%'
            },
            bottomGradientStyles: {
                width: '100%'
            },
            canUseDOM: false,
            /*
             * Переменная для отложенного вызова функции redraw(),
             * которая будет вызвана после вызова componentDidMount().
             */
            needRedrawAfterMount: false
        }, _this.anchor = null, _this.clickEventBindTimeout = null, _this.domElemPopup = null, _this.domElemPopupInner = null, _this.domElemPopupContent = null, _this.isWindowClickBinded = false, _this.position = null, _this.handleWindowResize = (0, _lodash2.default)(function () {
            if (_this.isPropsToPositionCorrect()) {
                _this.redraw();
            }
        }, 200), _temp), _possibleConstructorReturn(_this, _ret);
    }

    Popup.prototype.componentWillMount = function componentWillMount() {
        if (this.context.isInCustomContainer && this.context.renderContainerElement && this.context.positioningContainerElement) {
            this.setState({
                receivedContainer: true
            });
        }
    };

    Popup.prototype.componentDidMount = function componentDidMount() {
        var _this2 = this;

        if (this.props.onClickOutside) {
            this.ensureClickEvent();
        }

        if (this.inner && (this.props.height === 'adaptive' || this.props.target === 'screen')) {
            this.setGradientStyles();
        }

        window.addEventListener('resize', this.handleWindowResize);

        /* eslint-disable react/no-did-mount-set-state */
        this.setState({
            canUseDOM: true
        }, function () {
            if (_this2.state.needRedrawAfterMount) {
                _this2.redraw();
            }
        });
        /* eslint-enable */
    };

    Popup.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps, nextContext) {
        var _this3 = this;

        if (!this.state.receivedContainer && nextContext.renderContainerElement && nextContext.positioningContainerElement) {
            this.setState({
                receivedContainer: true
            }, function () {
                if (_this3.props.visible) {
                    _this3.redraw();
                }
            });

            return;
        }

        if (nextProps.visible !== this.props.visible) {
            this.redraw();
        }
    };

    Popup.prototype.componentDidUpdate = function componentDidUpdate(prevProps) {
        if (this.props.onClickOutside) {
            if (prevProps.onClickOutside !== this.props.onClickOutside) {
                this.ensureClickEvent();
            } else if (prevProps.visible !== this.props.visible) {
                this.ensureClickEvent(!this.props.visible);
            }
        }
    };

    Popup.prototype.componentWillUnmount = function componentWillUnmount() {
        if (this.props.onClickOutside) {
            this.ensureClickEvent(true);
        }

        // Cancel debouncing to avoid `this.setState()` invocation in unmounted component state
        this.handleWindowResize.cancel();
        window.removeEventListener('resize', this.handleWindowResize);
    };

    Popup.prototype.render = function render(cn) {
        var _this4 = this;

        if (!this.state.canUseDOM || !this.isContainerReady()) {
            return null;
        }

        var template = _react2.default.createElement(
            'div',
            {
                ref: function ref(popup) {
                    _this4.popup = popup;
                },
                'data-for': this.props.for,
                className: cn({
                    direction: this.state.direction,
                    type: this.props.target === 'anchor' && this.props.type === 'tooltip' && this.props.type,
                    target: this.props.target,
                    size: this.props.size,
                    visible: this.props.visible,
                    height: this.props.height,
                    padded: this.props.padded
                }),
                id: this.props.id,
                style: Object.assign({}, this.state.styles, {
                    minWidth: this.getMinWidth(),
                    maxWidth: this.getMaxWidth(),
                    maxHeight: this.getMaxHeight()
                }),
                onMouseEnter: this.handleMouseEnter,
                onMouseLeave: this.handleMouseLeave,
                'data-test-id': this.props['data-test-id']
            },
            _react2.default.createElement(
                'div',
                { className: cn('container') },
                this.props.header && _react2.default.createElement(
                    'div',
                    { className: cn('header') },
                    this.props.header
                ),
                _react2.default.createElement(
                    'div',
                    {
                        ref: function ref(inner) {
                            _this4.inner = inner;
                        },
                        className: cn('inner'),
                        onScroll: this.handleInnerScroll
                    },
                    _react2.default.createElement(
                        'div',
                        {
                            className: cn('content'),
                            ref: function ref(content) {
                                _this4.content = content;
                            }
                        },
                        this.props.children,
                        _react2.default.createElement(_resizeSensor2.default, { onResize: this.handleResize })
                    )
                ),
                this.state.hasScrollbar && _react2.default.createElement(
                    'div',
                    null,
                    _react2.default.createElement('div', { className: cn('gradient', { top: true }), style: this.state.topGradientStyles }),
                    _react2.default.createElement('div', {
                        className: cn('gradient', { bottom: true }),
                        style: this.state.bottomGradientStyles
                    })
                )
            )
        );

        return _reactDom2.default.createPortal(template, this.getRenderContainer());
    };

    Popup.prototype.handleInnerScroll = function handleInnerScroll(event) {
        var _event$target = event.target,
            scrollTop = _event$target.scrollTop,
            offsetHeight = _event$target.offsetHeight,
            scrollHeight = _event$target.scrollHeight;

        var isTopReached = Math.round(scrollTop) === 0;
        var isBottomReached = Math.round(scrollTop) + offsetHeight === scrollHeight;

        if (this.props.height === 'adaptive' || this.props.target === 'screen') {
            var topGradientStyles = {
                width: this.state.topGradientStyles.width
            };
            var bottomGradientStyles = {
                width: this.state.bottomGradientStyles.width
            };

            if (isTopReached) {
                topGradientStyles.height = 0;
            }

            if (isBottomReached) {
                bottomGradientStyles.height = 0;
            }

            this.setState({
                topGradientStyles: topGradientStyles,
                bottomGradientStyles: bottomGradientStyles
            });
        }
    };

    Popup.prototype.handleMouseEnter = function handleMouseEnter(event) {
        if (this.props.onMouseEnter) {
            this.props.onMouseEnter(event);
        }
    };

    Popup.prototype.handleMouseLeave = function handleMouseLeave(event) {
        if (this.props.onMouseLeave) {
            this.props.onMouseLeave(event);
        }
    };

    Popup.prototype.handleWindowClick = function handleWindowClick(event) {
        if (this.props.onClickOutside && !!this.domElemPopup && (0, _window.isNodeOutsideElement)(event.target, this.domElemPopup)) {
            this.props.onClickOutside(event);
        }
    };

    Popup.prototype.handleResize = function handleResize() {
        if (!this.props.visible) {
            return;
        }

        this.redraw();
    };

    /**
     * Задает элемент, к которому будет привязан popup.
     *
     * @public
     * @param {HTMLElement} target Элемент, к которому будет привязан popup
     */


    Popup.prototype.setTarget = function setTarget(target) {
        if (this.anchor === target) {
            return;
        }

        this.anchor = target;
        this.redraw();
    };

    /**
     * Задает положение popup.
     *
     * @public
     * @param {Number} left x-coordinate
     * @param {Number} top y-coordinate
     */


    Popup.prototype.setPosition = function setPosition(left, top) {
        this.position = { left: left, top: top };
        this.redraw();
    };

    /**
     * Возвращает внутренний DOM узел.
     *
     * @public
     * @returns {HTMLElement}
     */


    Popup.prototype.getInnerNode = function getInnerNode() {
        return this.domElemPopupInner;
    };

    /**
     * Возвращает контейнер, в котором отрендерится попап.
     *
     * @returns {HTMLElement}
     */


    Popup.prototype.getRenderContainer = function getRenderContainer() {
        if (!this.context.isInCustomContainer) {
            return document.body;
        }

        return this.context.renderContainerElement;
    };

    /**
     * Возвращает контейнер, внутрь которого надо вписать элемент.
     *
     * @returns {HTMLElement}
     */


    Popup.prototype.getPositioningContainer = function getPositioningContainer() {
        if (!this.context.isInCustomContainer) {
            return null;
        }

        return this.context.positioningContainerElement;
    };

    /**
     * Возвращает `true`, если контейнер, в котором должен быть отрисован
     * `Popup` уже находится в DOM. Для `Popup` без кастомного контейнера
     * роль контейнера выполняет `document.body` и этот для них этот метод
     * всегда вернете `true`.
     *
     * @returns {Boolean}
     */


    Popup.prototype.isContainerReady = function isContainerReady() {
        if (!this.context.isInCustomContainer) {
            return true;
        }

        return this.context.isInCustomContainer && this.state.receivedContainer;
    };

    /**
     * Возвращает `true`, если все необходимые для расчета положения `Popup`
     * внешние props заданы.
     *
     * @returns {Boolean}
     */


    Popup.prototype.isPropsToPositionCorrect = function isPropsToPositionCorrect() {
        return this.props.target === 'anchor' && this.anchor || this.props.target === 'position' && this.position || this.props.target === 'screen';
    };

    Popup.prototype.redraw = function redraw() {
        /*
         * Если функция redraw() была вызвана до componentDidMount,
         * то нужно отложить её вызов до момента,
         * когда this.state.canUseDOM будет равен значению true.
         *
         * Это сделано для того, чтобы redraw() не вызывалась на серверной стороне.
         */
        this.setState({
            needRedrawAfterMount: true
        });

        if (!this.state.canUseDOM || !this.isContainerReady()) {
            return;
        }

        this.setState({
            needRedrawAfterMount: false
        });

        if (!this.isPropsToPositionCorrect()) {
            throw new Error('Cannot show popup without target or position');
        }

        if (!this.domElemPopup) {
            this.domElemPopup = this.popup;
            this.domElemPopupInner = this.inner;
            this.domElemPopupContent = this.content;
        }

        var popupHash = this.getPopupHash();
        var bestDrawingParams = void 0;

        switch (this.props.target) {
            case 'position':
                bestDrawingParams = { top: this.position.top, left: this.position.left };
                break;

            case 'screen':
                bestDrawingParams = {
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    overflow: this.inner.scrollHeight > this.inner.clientHeight
                };
                break;

            case 'anchor':
                bestDrawingParams = (0, _calcDrawingParams.calcBestDrawingParams)(popupHash, (0, _calcDrawingParams.calcTargetDimensions)(popupHash), (0, _calcDrawingParams.calcFitContainerDimensions)(popupHash));
                break;
        }

        this.setState({
            direction: bestDrawingParams.direction,
            hasScrollbar: bestDrawingParams.overflow,
            styles: this.getDrawingCss(bestDrawingParams)
        });

        this.setGradientStyles();
    };

    Popup.prototype.ensureClickEvent = function ensureClickEvent(isDestroy) {
        var _this5 = this;

        var isNeedBindEvent = isDestroy !== undefined ? !isDestroy : this.props.visible;

        // We need timeouts to not to catch the event that causes
        // popup opening (because it propagates to the `window`).
        if (this.clickEventBindTimeout) {
            clearTimeout(this.clickEventBindTimeout);
            this.clickEventBindTimeout = null;
        }

        this.clickEventBindTimeout = setTimeout(function () {
            if (!_this5.isWindowClickBinded && isNeedBindEvent) {
                window.addEventListener('click', _this5.handleWindowClick);
                window.addEventListener('touchend', _this5.handleWindowClick);
                _this5.isWindowClickBinded = true;
            } else if (_this5.isWindowClickBinded && !isNeedBindEvent) {
                window.removeEventListener('click', _this5.handleWindowClick);
                window.removeEventListener('touchend', _this5.handleWindowClick);
                _this5.isWindowClickBinded = false;
            }
        }, 0);
    };

    Popup.prototype.getDrawingCss = function getDrawingCss(drawingParams) {
        return {
            top: drawingParams.top,
            left: drawingParams.left,
            right: drawingParams.right,
            bottom: drawingParams.bottom,
            height: this.props.height === 'adaptive' ? drawingParams.height : 'auto'
        };
    };

    /**
     * @returns {Number}
     */


    Popup.prototype.getMinWidth = function getMinWidth() {
        return this.props.minWidth !== undefined ? this.props.minWidth : 0;
    };

    /**
     * @returns {Number}
     */


    Popup.prototype.getMaxWidth = function getMaxWidth() {
        return this.props.maxWidth !== undefined ? this.props.maxWidth : 'none';
    };

    /**
     * @returns {Number}
     */


    Popup.prototype.getMaxHeight = function getMaxHeight() {
        return this.props.maxHeight !== undefined ? this.props.maxHeight : 'none';
    };

    /**
     * Get collection of popup properties.
     *
     * @returns {PopupHash}
     */


    Popup.prototype.getPopupHash = function getPopupHash() {
        return {
            directions: this.props.directions,
            bestDirection: this.state.direction,
            isHeightAdaptive: this.props.height === 'adaptive',
            isHeightAvailable: this.props.height === 'available',
            isTargetAnchor: this.props.target === 'anchor',
            isHaveTooltip: this.props.type === 'tooltip',
            width: this.domElemPopup.offsetWidth,
            height: this.domElemPopup.offsetHeight,
            contentWidth: this.domElemPopupContent.offsetWidth,
            contentHeight: this.domElemPopupContent.offsetHeight,
            offset: {
                main: this.props.mainOffset,
                second: this.props.secondaryOffset,
                fitContainer: this.props.fitContaiterOffset
            },
            targetPosition: this.position,
            targetAnchor: this.anchor,
            fitContainer: this.getPositioningContainer()
        };
    };

    Popup.prototype.setGradientStyles = function setGradientStyles() {
        var clientWidth = this.inner.clientWidth;


        this.setState({
            topGradientStyles: {
                width: clientWidth,
                height: 0
            },
            bottomGradientStyles: {
                width: clientWidth
            }
        });
    };

    return Popup;
}(_react2.default.Component), _class3.defaultProps = {
    visible: false,
    padded: true,
    secondaryOffset: 0,
    fitContaiterOffset: 0,
    target: 'anchor',
    size: 's'
}, _class3.contextTypes = {
    isInCustomContainer: _propTypes2.default.bool,
    renderContainerElement: _propTypes3.HtmlElement,
    positioningContainerElement: _propTypes3.HtmlElement
}, _temp2), (_applyDecoratedDescriptor(_class2.prototype, 'handleInnerScroll', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleInnerScroll'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseEnter', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseEnter'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleMouseLeave', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseLeave'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleWindowClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleWindowClick'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleResize', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleResize'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'redraw', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'redraw'), _class2.prototype)), _class2)) || _class) || _class);
Popup.propTypes =  true ? {
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /** Идентификатор компонента в DOM */
    id: _propTypes2.default.string,
    /** Дочерние элементы `Popup` */
    children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]),
    /** Тип попапа */
    type: _propTypes2.default.oneOf(['default', 'tooltip']),
    /** Подстраивание высоты попапа под край окна ('adaptive'), занятие попапом всей возможной высоты ('available'), 'default' */
    height: _propTypes2.default.oneOf(['default', 'available', 'adaptive']),
    /** Только для target='anchor', расположение (в порядке приоритета) относительно точки открытия. Первым указывается главное направление, через дефис - второстепенное направление */
    directions: _propTypes2.default.arrayOf(_propTypes2.default.oneOf(['anchor', 'top-left', 'top-center', 'top-right', 'left-top', 'left-center', 'left-bottom', 'right-top', 'right-center', 'right-bottom', 'bottom-left', 'bottom-center', 'bottom-right'])),
    /** Привязка компонента к другому элементу на странице, или его расположение независимо от остальных: 'anchor', 'position', 'screen' */
    target: _propTypes2.default.oneOf(['anchor', 'position', 'screen']),
    /** Только для target='anchor'. Смещение в пикселях всплывающего окна относительно основного направления */
    mainOffset: _propTypes2.default.number,
    /** Только для target='anchor'. Смещение в пикселях всплывающего окна относительно второстепенного направления */
    secondaryOffset: _propTypes2.default.number,
    /** Только для target='anchor'. Минимально допустимое смещение в пикселях всплывающего окна от края его контейнера */
    fitContaiterOffset: _propTypes2.default.number,
    /** Управление видимостью компонента */
    visible: _propTypes2.default.bool,
    /** Управление выставлением модификатора для добавления внутренних отступов в стилях */
    padded: _propTypes2.default.bool,
    /** Элемент закреплённого заголовка для компонента */
    header: _propTypes2.default.node,
    /** Размер компонента */
    size: _propTypes2.default.oneOf(['s', 'm', 'l', 'xl']),
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /**
     * Обработчик события наведения курсора на попап
     * @param {React.MouseEvent} event
     */
    onMouseEnter: _propTypes2.default.func,
    /**
     * Обработчик события снятия курсора с попапа
     * @param {React.MouseEvent} event
     */
    onMouseLeave: _propTypes2.default.func,
    /**
     * Обработчик клика вне компонента
     * @param {React.MouseEvent} event
     */
    onClickOutside: _propTypes2.default.func,
    /** Минимальная ширина попапа */
    minWidth: _propTypes2.default.number,
    /** Максимальная ширина попапа */
    maxWidth: _propTypes2.default.number,
    /** Максимальная высота попапа */
    maxHeight: _propTypes2.default.number,
    /** Указатель на родительский элемент */
    for: _propTypes2.default.string,
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = Popup;
//# sourceMappingURL=popup.js.map


/***/ }),

/***/ "./node_modules/arui-feather/popup/popup_theme_alfa-on-color.css":
/*!***********************************************************************!*\
  !*** ./node_modules/arui-feather/popup/popup_theme_alfa-on-color.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"popup_theme_alfa-on-color": "_1mxrfR9rX7sU1IqoZHxGu7",
	"popup__container": "NqfWw_R_8bCPwIT4S-V0p",
	"popup_height_adaptive": "_2-r1SoV1oAzhr2sK59-pT8",
	"popup__gradient_top": "bK5cMe7Hm7FIAcMl8mJq4",
	"popup_target_screen": "_1veVf9O-mwqFtG8zVE4T4h",
	"popup__gradient_bottom": "_1lRUoktiFuzliyEq15mZo3",
	"popup_type_tooltip": "_1iqCIMhH8qktqQfkXhctBx",
	"popup_direction_top-left": "KfZQ7lwcK4IHxbL5zQl2X",
	"popup_direction_top-center": "_2q0PHtILC43hokLgHOW0OH",
	"popup_direction_top-right": "_9cyqR-pPm580b2baez1F9",
	"popup_direction_left-top": "_2bD-93ufaDmewJ_BMFs0K1",
	"popup_direction_left-center": "_1dGgJGgNc2nBA4O-TuAtRb",
	"popup_direction_left-bottom": "K1ERprvtwqahBlhxiIONW",
	"popup_direction_right-top": "eSVgbtNDEBVeLrp9NeiSj",
	"popup_direction_right-center": "_1yjqovfjD1RJqGTg-wS67a",
	"popup_direction_right-bottom": "_1gMk-R8ZmVjAhVTponPD9U",
	"popup_direction_bottom-left": "IF9MMBePK734T1NaHz24O",
	"popup_direction_bottom-center": "_1m3elB8cRJjHLXCFqG96Dp",
	"popup_direction_bottom-right": "jBfRWvg-S1VJZyVaJQLp2"
};

/***/ }),

/***/ "./node_modules/arui-feather/popup/popup_theme_alfa-on-white.css":
/*!***********************************************************************!*\
  !*** ./node_modules/arui-feather/popup/popup_theme_alfa-on-white.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"popup_theme_alfa-on-white": "_2WE6yaW41ujIt-cd0mSJBv",
	"popup__container": "_2oStcUzYR34DywBlCBashb",
	"popup_height_adaptive": "_2hxn9SQOkbphQphs0QeFkP",
	"popup__gradient_top": "KYbijeQ2F9JEvsMt-zkPf",
	"popup_target_screen": "_2Q443VHPOJqO2F4ti0i5r0",
	"popup__gradient_bottom": "_2fHDEOeqZzkvTwTIPV7Ee5",
	"popup_type_tooltip": "_3-OTntRi0Mla_4Qsb4W5iT",
	"popup_direction_top-left": "_1HseSSXB4AXwCO0fOFMiqw",
	"popup_direction_top-center": "_27XkTGf7Q22y4Z73QJa1Xs",
	"popup_direction_top-right": "_2hpGhA2FZhfS0gHfMUCbB",
	"popup_direction_left-top": "_15vta3HgKF5I9IKw6Ki-mK",
	"popup_direction_left-center": "_2DO-BmbEmP1KeBgzqdpxew",
	"popup_direction_left-bottom": "_3RtAfI4ylWQd9Gv6vOpuud",
	"popup_direction_right-top": "_1DjZTZvlI73HzG1J0bdWZ_",
	"popup_direction_right-center": "_3G97crbzQI-ilN9NuDJtl_",
	"popup_direction_right-bottom": "_232sqTyC5R-vTyl0LszDTv",
	"popup_direction_bottom-left": "_2GgHCSlRpe3oZn910vsH8m",
	"popup_direction_bottom-center": "ztbQJg0DeQOyPVrGt5evB",
	"popup_direction_bottom-right": "_2vsauJASfGoEqfSY7Zsg4a"
};

/***/ }),

/***/ "./node_modules/arui-feather/render-in-container/index.js":
/*!****************************************************************!*\
  !*** ./node_modules/arui-feather/render-in-container/index.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

var _renderInContainer = __webpack_require__(/*! ./render-in-container */ "./node_modules/arui-feather/render-in-container/render-in-container.js");

var _renderInContainer2 = _interopRequireDefault(_renderInContainer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _renderInContainer2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/render-in-container/render-in-container.js":
/*!******************************************************************************!*\
  !*** ./node_modules/arui-feather/render-in-container/render-in-container.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _dec, _class, _desc, _value, _class2; /* This Source Code Form is subject to the terms of the Mozilla Public
                                           * License, v. 2.0. If a copy of the MPL was not distributed with this
                                           * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _deprecatedDecorator = __webpack_require__(/*! deprecated-decorator */ "deprecated-decorator");

var _deprecatedDecorator2 = _interopRequireDefault(_deprecatedDecorator);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "react-dom");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _propTypes3 = __webpack_require__(/*! ../lib/prop-types */ "./node_modules/arui-feather/lib/prop-types.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

/**
 * @deprecated since v11.0.0
 *
 * Компонент, позволяющий визуализировать другие компоненты в произвольном контейнере.
 */
var RenderInContainer = (_dec = (0, _deprecatedDecorator2.default)('ReactDOM v16 with createPortal'), _dec(_class = (_class2 = function (_React$Component) {
    _inherits(RenderInContainer, _React$Component);

    function RenderInContainer() {
        _classCallCheck(this, RenderInContainer);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    RenderInContainer.prototype.componentDidMount = function componentDidMount() {
        this.buildElements();
        this.renderLayer();
    };

    RenderInContainer.prototype.componentDidUpdate = function componentDidUpdate() {
        if (this.container !== this.getContainer()) {
            this.buildElements();
        }
        this.renderLayer();
    };

    RenderInContainer.prototype.componentWillUnmount = function componentWillUnmount() {
        _reactDom2.default.unmountComponentAtNode(this.element);
        this.container.removeChild(this.element);
    };

    RenderInContainer.prototype.render = function render() {
        return false;
    };

    RenderInContainer.prototype.renderLayer = function renderLayer() {
        if (this.props.className) {
            this.element.className = this.props.className.toString();
        }

        _reactDom2.default.unstable_renderSubtreeIntoContainer(this, this.props.children, this.element, this.handleRender);
    };

    RenderInContainer.prototype.handleRender = function handleRender() {
        if (this.props.onRender) {
            this.props.onRender();
        }
    };

    /**
     * Возвращает HTMLElement враппера компонента.
     *
     * @public
     * @returns {HTMLElement}
     */


    RenderInContainer.prototype.getNode = function getNode() {
        return this.element;
    };

    /**
     * Возвращает HTMLElement контейнера, в который отрендерился компонент.
     *
     * @public
     * @returns {HTMLElement}
     */


    RenderInContainer.prototype.getContainer = function getContainer() {
        return this.props.container || document.body;
    };

    RenderInContainer.prototype.buildElements = function buildElements() {
        if (this.element && this.container) {
            _reactDom2.default.unmountComponentAtNode(this.element);
            this.container.removeChild(this.element);
        }

        this.container = this.getContainer();
        this.element = document.createElement('div');

        this.container.appendChild(this.element);
    };

    return RenderInContainer;
}(_react2.default.Component), (_applyDecoratedDescriptor(_class2.prototype, 'handleRender', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleRender'), _class2.prototype)), _class2)) || _class);
RenderInContainer.propTypes =  true ? {
    /** Дочерние элементы контейнера */
    children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]),
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /** Контейнер, в котором будет визуализирован компонент */
    container: _propTypes3.HtmlElement,
    /** Callback на рендер компонента */
    onRender: _propTypes2.default.func
} : undefined;
exports.default = RenderInContainer;
//# sourceMappingURL=render-in-container.js.map


/***/ }),

/***/ "./node_modules/arui-feather/resize-sensor/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/arui-feather/resize-sensor/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

var _resizeSensor = __webpack_require__(/*! ./resize-sensor */ "./node_modules/arui-feather/resize-sensor/resize-sensor.js");

var _resizeSensor2 = _interopRequireDefault(_resizeSensor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _resizeSensor2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                           * License, v. 2.0. If a copy of the MPL was not distributed with this
                                           * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/resize-sensor/resize-sensor.js":
/*!******************************************************************!*\
  !*** ./node_modules/arui-feather/resize-sensor/resize-sensor.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _desc, _value, _class; /* This Source Code Form is subject to the terms of the Mozilla Public
                            * License, v. 2.0. If a copy of the MPL was not distributed with this
                            * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

/**
 * Компонент позволяющий слушать изменения размера родительского элемента.
 * Для использования разместите его в элементе об изменении размера, которого
 * вы хотите знать и добавьте внешний обработчик `onResize`.
 *
 * Важно! Элемент, размер которого вы хотите измерять, должен обладать
 * css свойством `position: relative;`.
 */
var ResizeSensor = (_class = function (_React$Component) {
    _inherits(ResizeSensor, _React$Component);

    function ResizeSensor() {
        _classCallCheck(this, ResizeSensor);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    ResizeSensor.prototype.componentDidMount = function componentDidMount() {
        if (this.iframe.contentWindow) {
            this.iframe.contentWindow.addEventListener('resize', this.handleResize);
        }
    };

    /**
     * @type {HTMLIFrameElement}
     */


    ResizeSensor.prototype.componentWillUnmount = function componentWillUnmount() {
        if (this.iframe.contentWindow) {
            this.iframe.contentWindow.removeEventListener('resize', this.handleResize);
        }
    };

    ResizeSensor.prototype.render = function render() {
        var _this2 = this;

        var iframeStyle = {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            background: 'transparent',
            border: 'none',
            zIndex: -1
        };

        /* eslint-disable jsx-a11y/iframe-has-title */
        return _react2.default.createElement('iframe', {
            ref: function ref(iframe) {
                _this2.iframe = iframe;
            },
            style: iframeStyle,
            tabIndex: '-1'
        });
        /* eslint-enable jsx-a11y/iframe-has-title */
    };

    ResizeSensor.prototype.handleResize = function handleResize() {
        if (this.props.onResize) {
            this.props.onResize();
        }
    };

    return ResizeSensor;
}(_react2.default.Component), (_applyDecoratedDescriptor(_class.prototype, 'handleResize', [_autobind2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'handleResize'), _class.prototype)), _class);
ResizeSensor.propTypes =  true ? {
    /** Callback на изменение размера родителя */
    onResize: _propTypes2.default.func
} : undefined;
exports.default = ResizeSensor;
//# sourceMappingURL=resize-sensor.js.map


/***/ }),

/***/ "./node_modules/arui-feather/select/index.js":
/*!***************************************************!*\
  !*** ./node_modules/arui-feather/select/index.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ../button */ "./node_modules/arui-feather/button/index.js");

__webpack_require__(/*! ../icon/ui/arrow-down */ "./node_modules/arui-feather/icon/ui/arrow-down/index.js");

__webpack_require__(/*! ../icon/ui/arrow-up */ "./node_modules/arui-feather/icon/ui/arrow-up/index.js");

__webpack_require__(/*! ../icon-button */ "./node_modules/arui-feather/icon-button/index.js");

__webpack_require__(/*! ../menu */ "./node_modules/arui-feather/menu/index.js");

__webpack_require__(/*! ../mq */ "./node_modules/arui-feather/mq/index.js");

__webpack_require__(/*! ../popup */ "./node_modules/arui-feather/popup/index.js");

__webpack_require__(/*! ../popup-header */ "./node_modules/arui-feather/popup-header/index.js");

__webpack_require__(/*! ../resize-sensor */ "./node_modules/arui-feather/resize-sensor/index.js");

__webpack_require__(/*! ./select.css */ "./node_modules/arui-feather/select/select.css");

__webpack_require__(/*! ./select_theme_alfa-on-color.css */ "./node_modules/arui-feather/select/select_theme_alfa-on-color.css");

__webpack_require__(/*! ./select_theme_alfa-on-white.css */ "./node_modules/arui-feather/select/select_theme_alfa-on-white.css");

var _select = __webpack_require__(/*! ./select */ "./node_modules/arui-feather/select/select.js");

var _select2 = _interopRequireDefault(_select);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _select2.default; /* This Source Code Form is subject to the terms of the Mozilla Public
                                     * License, v. 2.0. If a copy of the MPL was not distributed with this
                                     * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/select/select.css":
/*!*****************************************************!*\
  !*** ./node_modules/arui-feather/select/select.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"select": "_26UJwrrGwGDpihz7FG44da",
	"select__inner": "_2mzu2hsrkH6TmLn_4D2Hc2",
	"select_width_available": "aeTsVstXkzW_qWHbS-8y0",
	"select__top": "_3YzTHoSRwN2-3pPLrtLp7u",
	"select__sub-wrapper": "_2u22_wNRdPMPE7Xrq4Opx0",
	"select__sub": "_2cmB8J7PxXmocOVLcjaLPC",
	"select_size_s": "Z5OvZK_hSq6oWtZqnQtEc",
	"select_size_m": "_2IwGsbQ_YJ3VMJIHE-QGqz",
	"select_size_l": "_2VaPtFm6SDm-_W8CegG4E0",
	"select_size_xl": "_3clSHCo0dCQDlvea9m-Wf9",
	"select-button": "v0tN9ID_6YUrsBABlZo0E",
	"select__native-control": "_3w-hmW2lFJei7OTSGpyfDt",
	"select-button_size_s": "_2HGxU4yjmO4281XpBCj-6V",
	"select-button_size_m": "_3bFLIVHxqKn_B9nXShmAU1",
	"select-button_size_l": "_1_zvT1oNmptXG2IvG7TLsM",
	"select-button_size_xl": "_3dxHoaYn9V8xa7C3wROQiC",
	"select-button_disabled": "_1Akd5jbTIJZMFY6o--Bjx0",
	"icon-button_size_s": "_3EPTnc_5V9a5OrLaUoApEK",
	"icon-button_size_m": "_3lM7MbSJsgUZALgIVuhGct",
	"icon-button_size_l": "_1xKDL_wRYwIT1fftKXjtya",
	"icon-button_size_xl": "_2jCCHKSyTmU36BobbfHrzm",
	"select__tick": "_2N5gDsBrQwqiEn_Qv_vlep",
	"select_disabled": "_2Gli6KpF_nGBHXN_iJvYGo",
	"select_opened": "fVCkGMi7weBrt9CzYQm3J",
	"select-button_hovered": "_3Uh9hBd2C0ao07x2yxqtrM",
	"select_no-tick": "_1iBFZUjoW8AM4x6fEOQ31C",
	"select__placeholder": "_2-FDKxTUZYueU4e3HL-bfu",
	"select_has-label": "_1c1AgKpFPG32Vys72nFGO-",
	"select_has-placeholder": "_1x3hH3tuuVtCwxiq5P_rXb",
	"select_has-value": "_3lolRSwoy4HiGVSYjbKVGB",
	"select_view_filled": "_-3AB-_877m1E6ANUyYCfL",
	"select-button__content": "_12GQYykJt9MmSlx99be4oe",
	"select_size_s__sub": "gTRbt55o_rCCjTqtVywHy",
	"select_size_m__sub": "_1teyJsAnswi23zpFMIBZHI",
	"select-button__text": "_2eMeMmeKAHEoqLtSDUEn2s",
	"select_view_filled__sub": "_2Hs_aHlodF8OSNUZF52iCA",
	"select_focused": "JxARpXRg7xfE6_ZfH2MUu",
	"select_invalid": "_153mN30MbbS2RZLMFFukUb",
	"select-button_pressed": "_1Abx6fc3mpMl6YUDAh5JCU"
};

/***/ }),

/***/ "./node_modules/arui-feather/select/select.js":
/*!****************************************************!*\
  !*** ./node_modules/arui-feather/select/select.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _dec, _class, _dec2, _dec3, _class2, _desc, _value, _class3, _class4, _temp2; /* This Source Code Form is subject to the terms of the Mozilla Public
                                                                                   * License, v. 2.0. If a copy of the MPL was not distributed with this
                                                                                   * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _reactAddonsCreateFragment = __webpack_require__(/*! react-addons-create-fragment */ "react-addons-create-fragment");

var _reactAddonsCreateFragment2 = _interopRequireDefault(_reactAddonsCreateFragment);

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _button = __webpack_require__(/*! ../button/button */ "./node_modules/arui-feather/button/button.js");

var _button2 = _interopRequireDefault(_button);

var _iconButton = __webpack_require__(/*! ../icon-button/icon-button */ "./node_modules/arui-feather/icon-button/icon-button.js");

var _iconButton2 = _interopRequireDefault(_iconButton);

var _arrowDown = __webpack_require__(/*! ../icon/ui/arrow-down */ "./node_modules/arui-feather/icon/ui/arrow-down/index.js");

var _arrowDown2 = _interopRequireDefault(_arrowDown);

var _arrowUp = __webpack_require__(/*! ../icon/ui/arrow-up */ "./node_modules/arui-feather/icon/ui/arrow-up/index.js");

var _arrowUp2 = _interopRequireDefault(_arrowUp);

var _menu = __webpack_require__(/*! ../menu/menu */ "./node_modules/arui-feather/menu/menu.js");

var _menu2 = _interopRequireDefault(_menu);

var _mq = __webpack_require__(/*! ../mq/mq */ "./node_modules/arui-feather/mq/mq.js");

var _mq2 = _interopRequireDefault(_mq);

var _popup = __webpack_require__(/*! ../popup/popup */ "./node_modules/arui-feather/popup/popup.js");

var _popup2 = _interopRequireDefault(_popup);

var _popupHeader = __webpack_require__(/*! ../popup-header/popup-header */ "./node_modules/arui-feather/popup-header/popup-header.js");

var _popupHeader2 = _interopRequireDefault(_popupHeader);

var _resizeSensor = __webpack_require__(/*! ../resize-sensor/resize-sensor */ "./node_modules/arui-feather/resize-sensor/resize-sensor.js");

var _resizeSensor2 = _interopRequireDefault(_resizeSensor);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _propTypes3 = __webpack_require__(/*! ../lib/prop-types */ "./node_modules/arui-feather/lib/prop-types.js");

var _keyboardCode = __webpack_require__(/*! ../lib/keyboard-code */ "./node_modules/arui-feather/lib/keyboard-code.js");

var _keyboardCode2 = _interopRequireDefault(_keyboardCode);

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

var _scrollTo2 = __webpack_require__(/*! ../lib/scroll-to */ "./node_modules/arui-feather/lib/scroll-to.js");

var _scrollTo3 = _interopRequireDefault(_scrollTo2);

var _vars = __webpack_require__(/*! ../vars */ "./node_modules/arui-feather/vars.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

var DEFAULT_TEXT_FALLBACK = 'Выберите:';

/**
 * Элемент кнопки для выпадающего списка.
 */
var SelectButton = (_dec = (0, _cn2.default)('select-button'), _dec(_class = function (_Button) {
    _inherits(SelectButton, _Button);

    function SelectButton() {
        _classCallCheck(this, SelectButton);

        return _possibleConstructorReturn(this, _Button.apply(this, arguments));
    }

    return SelectButton;
}(_button2.default)) || _class);

/**
 * @typedef {Object} CheckedOption
 * @property {String} value Уникальное значение, которое будет отправлено на сервер, если вариант выбран
 * @property {String} text Текст варианта
 * @property {String} checkedText Текст, который будет отображаться при выборе
 * @property {Icon} icon Иконка варианта
 */

/**
 * Компонент выпадающего списка.
 */

var Select = (_dec2 = (0, _cn2.default)('select', SelectButton, _popup2.default), _dec3 = (0, _performance2.default)(true), _dec2(_class2 = _dec3(_class2 = (_class3 = (_temp2 = _class4 = function (_React$Component) {
    _inherits(Select, _React$Component);

    function Select() {
        var _temp, _this2, _ret;

        _classCallCheck(this, Select);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this2 = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this2), _this2.state = {
            hasGroup: false,
            isMobile: false,
            opened: !!_this2.props.opened,
            popupStyles: {},
            value: _this2.props.value || []
        }, _this2.awaitClosing = false, _temp), _possibleConstructorReturn(_this2, _ret);
    }

    /**
     * @type {HTMLDivElement}
     */


    /**
     * @type {Button}
     */


    /**
     * @type {Popup}
     */


    /**
     * @type {Menu}
     */


    /**
     * @type {Boolean}
     * При открытом меню, нажатие на Esc устанавливает значение этой переменной в true
     * Далее фокус переводится на кнопку. Далее вызывается обработчик handleMenuBlur.
     * В обработчике закрываем попап, если ожидаем закрытия(this.awaitClosing) или фокус за пределами селекта.
     * Это нужно, т.к. в случае в renderPopupOnFocus={true} меню исчезнет быстрее, чем сработает onMenuBlur
     */


    Select.prototype.componentWillMount = function componentWillMount() {
        this.setState({
            hasGroup: this.props.options.some(function (option) {
                return !!(option.type === 'group' && !!option.content);
            })
        });
    };

    Select.prototype.componentDidMount = function componentDidMount() {
        if (this.isAutoSelectRequired()) {
            this.selectFirstOption();
        }

        this.setPopupTarget();
        this.updatePopupStyles();
    };

    Select.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
        this.setPopupTarget();
        this.updatePopupStyles();

        if (this.state.opened && nextProps.disabled) {
            this.toggleOpened();
        }

        this.setState({
            hasGroup: this.props.options.some(function (option) {
                return !!(option.type === 'group' && !!option.content);
            })
        });
    };

    Select.prototype.componentDidUpdate = function componentDidUpdate() {
        if (this.state.opened) {
            this.updatePopupStyles();
        }
    };

    Select.prototype.render = function render(cn, SelectButton, Popup) {
        var _this3 = this;

        var value = this.getValue();

        return _react2.default.createElement(
            'div',
            {
                className: cn({
                    mode: this.props.mode,
                    size: this.props.size,
                    view: this.props.view,
                    width: this.props.width,
                    checked: value.length > 0,
                    disabled: this.props.disabled,
                    'has-label': !!this.props.label,
                    'has-value': !!value.length,
                    'has-placeholder': !!this.props.placeholder,
                    invalid: !!this.props.error,
                    opened: this.getOpened(),
                    'no-tick': this.props.hideTick
                }),
                ref: function ref(root) {
                    _this3.root = root;
                },
                'data-test-id': this.props['data-test-id']
            },
            _react2.default.createElement(
                'span',
                { className: cn('inner') },
                _react2.default.createElement('input', { id: this.props.id, name: this.props.name, type: 'hidden', value: value }),
                !!this.props.label && _react2.default.createElement(
                    'span',
                    { className: cn('top') },
                    this.props.label
                ),
                this.renderButton(cn, SelectButton),
                _react2.default.createElement(
                    _mq2.default,
                    { query: '--small-only', touch: true, onMatchChange: this.handleMqMatchChange },
                    this.props.mobileMenuMode === 'native' && this.renderNativeSelect(cn)
                ),
                (this.props.error || this.props.hint) &&
                // The <div /> wrapper is needed to fix Safari bug of "jumping" element with
                // `display: table-caption`. See: https://github.com/alfa-laboratory/arui-feather/pull/656
                _react2.default.createElement(
                    'div',
                    { className: cn('sub-wrapper') },
                    _react2.default.createElement(
                        'span',
                        { className: cn('sub') },
                        this.props.error || this.props.hint
                    )
                ),
                (!this.state.isMobile || this.state.isMobile && this.props.mobileMenuMode === 'popup') && this.renderPopup(cn, Popup)
            )
        );
    };

    Select.prototype.renderButton = function renderButton(cn, SelectButton) {
        var _this4 = this;

        var tickSize = void 0;
        var ToggledIcon = void 0;
        var opened = this.getOpened();

        switch (opened) {
            case true:
                ToggledIcon = _arrowUp2.default;
                break;
            case false:
                ToggledIcon = _arrowDown2.default;
                break;
        }

        switch (this.props.size) {
            case 's':
            case 'm':
                tickSize = this.props.view === 'filled' ? 'l' : 's';
                break;
            case 'l':
                tickSize = 'm';
                break;
            case 'xl':
                tickSize = 'l';
                break;
        }

        return _react2.default.createElement(
            SelectButton,
            {
                ref: function ref(button) {
                    _this4.button = button;
                },
                size: this.props.size,
                disabled: this.props.disabled,
                focused: this.getOpened(),
                onClick: this.handleButtonClick,
                onFocus: this.handleButtonFocus,
                onBlur: this.handleButtonBlur
            },
            this.renderButtonContent(cn),
            !this.props.hideTick && _react2.default.createElement(
                _iconButton2.default,
                { className: cn('tick'), key: 'addon-icon', size: this.props.size, tag: 'span' },
                _react2.default.createElement(ToggledIcon, { size: tickSize })
            ),
            this.getOpened() && _react2.default.createElement(_resizeSensor2.default, { key: 'addon-sensor', onResize: this.updatePopupStyles })
        );
    };

    Select.prototype.renderNativeSelect = function renderNativeSelect(cn) {
        var _this5 = this;

        var isCheckMode = this.props.mode === 'check';
        var hasEmptyOptGroup = isCheckMode || this.state.hasGroup;
        var hasEmptyOption = !isCheckMode && !this.state.hasGroup;
        var value = this.getValue();

        if (!isCheckMode) {
            value = value.length ? value[0] : '';
        }

        return _react2.default.createElement(
            'select',
            {
                ref: function ref(nativeSelect) {
                    _this5.nativeSelect = nativeSelect;
                },
                className: cn('native-control'),
                disabled: this.props.disabled,
                multiple: isCheckMode && 'multiple',
                value: value,
                onChange: this.handleNativeOptionCheck,
                onClick: this.handleNativeClick,
                onFocus: this.handleNativeFocus,
                onBlur: this.handleNativeBlur
            },
            /*
                  Хак с пустым <optgroup> — для фикса странного поведения select с атрибутом multiple на iOS7+:
                  1. If no option is selected, it selects the first option in the list.
                  2. If one option is selected, it deselects that option.
                  3. If multiple options are selected, it deselects the last option to be tapped.
                  4. If an option previously selected is deselected, it reselects the option.
                  https://discussions.apple.com/message/23745665
                  https://discussions.apple.com/message/24694954
              */
            hasEmptyOptGroup && _react2.default.createElement('optgroup', { disabled: true, label: this.props.nativeOptionPlaceholder }),
            hasEmptyOption && _react2.default.createElement(
                'option',
                { disabled: true, value: '' },
                this.props.nativeOptionPlaceholder
            ),
            this.renderNativeOptionsList(this.props.options)
        );
    };

    Select.prototype.renderPopup = function renderPopup(cn, Popup) {
        var optionsList = this.renderOptionsList(this.props.options);
        var opened = this.getOpened();
        var value = this.getValue();
        var popupIsReady = this.state.popupIsReady;

        var popupIsVisible = this.props.renderPopupOnFocus ? opened && popupIsReady : opened;

        if (!opened && this.props.renderPopupOnFocus) {
            return null;
        }

        return _react2.default.createElement(
            Popup,
            {
                key: 'popup',
                ref: this.setPopupRef,
                'for': this.props.name,
                className: cn('popup'),
                directions: this.props.directions,
                height: 'adaptive',
                padded: false,
                mainOffset: this.props.popupMainOffset,
                secondaryOffset: this.props.popupSecondaryOffset,
                size: this.props.size,
                target: this.state.isMobile ? 'screen' : 'anchor',
                header: this.state.isMobile && this.renderMobileHeader(cn),
                visible: popupIsVisible,
                onClickOutside: this.handleClickOutside,
                minWidth: this.state.popupStyles.minWidth,
                maxWidth: this.state.popupStyles.maxWidth,
                maxHeight: this.props.maxHeight
            },
            _react2.default.createElement(_menu2.default, {
                ref: this.setMenuRef,
                className: cn('menu'),
                size: this.props.size,
                disabled: this.props.disabled,
                mode: this.props.mode,
                groupView: this.props.groupView,
                content: optionsList,
                onItemCheck: this.handleOptionCheck,
                checkedItems: value,
                onFocus: this.handleMenuFocus,
                onBlur: this.handleMenuBlur,
                onHighlightItem: this.handleMenuHighlightItem,
                onKeyDown: this.handleMenuKeyDown
            })
        );
    };

    Select.prototype.renderOptionsList = function renderOptionsList(options) {
        var _this6 = this;

        return options.map(function (option) {
            if (option.type === 'group' && !!option.content) {
                var _content = _this6.renderOptionsList(option.content);

                return {
                    type: 'group',
                    title: option.title,
                    content: _content
                };
            }

            var content = option.description || option.text;

            return {
                props: option.props,
                value: option.value,
                content: (0, _reactAddonsCreateFragment2.default)({ icon: option.icon, content: content })
            };
        });
    };

    Select.prototype.renderNativeOptionsList = function renderNativeOptionsList(options) {
        var _this7 = this;

        var groupKey = 0;

        return options.map(function (option) {
            if (option.type === 'group' && !!option.content) {
                var content = _this7.renderNativeOptionsList(option.content);

                groupKey += 1;

                return _react2.default.createElement(
                    'optgroup',
                    { key: 'group_' + groupKey, label: option.title },
                    content
                );
            }

            return _react2.default.createElement(
                'option',
                { key: option.value, value: option.value },
                option.nativeText || option.text
            );
        });
    };

    Select.prototype.renderButtonContent = function renderButtonContent(cn) {
        var checkedItems = this.getCheckedItems(this.props.options);

        if (this.props.renderButtonContent) {
            return this.props.renderButtonContent(checkedItems);
        }

        var checkedItemsText = checkedItems.map(function (item) {
            return item.checkedText || item.text;
        }).join(', ');
        if (checkedItemsText) {
            return checkedItemsText;
        }
        // Если ничего не выбрано, то рендерим плейсхолдер
        // Если плейсхолдера нет, то рендерим текст лейбла. Но отрендерится он прозрачным - это нужно для того, чтобы
        // лейбл растягивал блок до нужной ширины, т. к. настоящий лейбл позиционируется абсолютно и не влияет на размер
        // Если нет ни плейсхолдера, ни лейбла, то рендерим "Выберите:" для обратной совместимости
        return _react2.default.createElement(
            'span',
            { className: cn('placeholder') },
            this.props.placeholder || this.props.label || DEFAULT_TEXT_FALLBACK
        );
    };

    Select.prototype.renderMobileHeader = function renderMobileHeader(cn) {
        return _react2.default.createElement(_popupHeader2.default, {
            className: cn('mobile-header'),
            size: this.props.size,
            title: this.props.mobileTitle,
            onCloserClick: this.handlePopupCloserClick
        });
    };

    Select.prototype.handleButtonClick = function handleButtonClick(event) {
        if (!this.props.disabled) {
            this.toggleOpened();
        }

        if (this.props.onClick) {
            this.props.onClick(event);
        }
    };

    Select.prototype.handleButtonKeyDown = function handleButtonKeyDown(event) {
        if (!this.props.disabled) {
            if (event.which === _keyboardCode2.default.ENTER || event.which === _keyboardCode2.default.SPACE) {
                this.toggleOpened();
            }
        }

        if (this.props.onKeyDown) {
            this.props.onKeyDown(event);
        }
    };

    Select.prototype.handleButtonFocus = function handleButtonFocus(event) {
        if (this.props.onButtonFocus) {
            this.props.onButtonFocus(this.getRevisedEvent(event));
        }
    };

    Select.prototype.handleButtonBlur = function handleButtonBlur(event) {
        if (this.props.onButtonBlur) {
            this.props.onButtonBlur(this.getRevisedEvent(event));
        }
    };

    Select.prototype.handleMenuFocus = function handleMenuFocus(event) {
        event.target.value = this.getValue();

        if (this.props.onFocus) {
            this.props.onFocus(event);
        }

        if (this.props.onMenuFocus) {
            this.props.onMenuFocus(event);
        }
    };

    Select.prototype.handleMenuBlur = function handleMenuBlur(event) {
        event.target.value = this.getValue();

        if (this.awaitClosing || event.relatedTarget !== this.button.getNode()) {
            this.awaitClosing = false;
            this.setState({
                opened: false
            });
        }

        if (this.props.onBlur) {
            this.props.onBlur(event);
        }

        if (this.props.onMenuBlur) {
            this.props.onMenuBlur(event);
        }
    };

    Select.prototype.handleMenuHighlightItem = function handleMenuHighlightItem(highlightedItem) {
        if (!this.getOpened() && highlightedItem && this.popup) {
            this.popup.getInnerNode().scrollTop = 0;
            this.scrollToHighlightedItem(highlightedItem);
        }
    };

    Select.prototype.handleOptionCheck = function handleOptionCheck(value) {
        var _this8 = this;

        var opened = this.getOpened();

        this.setState({ value: value, opened: this.props.mode === 'check' }, function () {
            // Если у Select-а закрылось выпадающее меню,
            // то возвращаем фокус на кнопку Select
            // после выбора опции.
            var newOpened = _this8.getOpened();
            if (!newOpened && opened !== newOpened) {
                _this8.button.focus();
            }
        });

        if (this.props.onChange) {
            this.props.onChange(value);
        }
    };

    Select.prototype.handleNativeOptionCheck = function handleNativeOptionCheck(event) {
        function getFlattenedPropOptions(options) {
            var result = [];

            options.forEach(function (option) {
                if (option.type === 'group' && !!option.content) {
                    var findInGroup = getFlattenedPropOptions(option.content);
                    result = result.concat(findInGroup);
                } else {
                    result.push(option);
                }
            });

            return result;
        }

        var hasEmptyOption = this.props.mode !== 'check' && !this.state.hasGroup;
        var domOptions = Array.from(event.currentTarget.options).filter(function (option, index) {
            return !(hasEmptyOption && option.disabled && index === 0);
        });
        var flattenedPropOptions = getFlattenedPropOptions(this.props.options);
        var value = domOptions.reduce(function (result, item, index) {
            if (item.selected) {
                result.push(flattenedPropOptions[index].value);
            }
            return result;
        }, []);

        if (this.props.mode === 'radio' || this.props.mode === 'radio-check') {
            this.blur();
        }

        this.setState({ value: value });

        if (this.props.onChange) {
            this.props.onChange(value);
        }
    };

    Select.prototype.handleClickOutside = function handleClickOutside() {
        this.setState({
            opened: false
        });

        if (this.props.onClickOutside) {
            this.props.onClickOutside();
        }
    };

    Select.prototype.handleMenuKeyDown = function handleMenuKeyDown(event, highlightedItem) {
        var opened = this.getOpened();

        switch (event.which) {
            case _keyboardCode2.default.DOWN_ARROW:
            case _keyboardCode2.default.UP_ARROW:
                event.preventDefault();
                this.scrollToHighlightedItem(highlightedItem);
                break;
            case _keyboardCode2.default.ENTER:
            case _keyboardCode2.default.SPACE:
                event.preventDefault();
                this.setState({
                    opened: this.props.mode === 'check' ? true : !opened
                });
                this.focusOnMenu();
                break;
            case _keyboardCode2.default.ESCAPE:
                event.preventDefault();
                this.awaitClosing = true;
                this.button.focus();
                break;
        }

        if (this.props.onKeyDown) {
            this.props.onKeyDown(event);
        }
    };

    Select.prototype.handleNativeFocus = function handleNativeFocus(event) {
        if (!this.props.disabled) {
            this.toggleOpened();
        }

        if (this.props.onFocus) {
            this.props.onFocus(this.getRevisedEvent(event));
        }
    };

    Select.prototype.handleNativeBlur = function handleNativeBlur(event) {
        if (!this.props.disabled) {
            this.toggleOpened();
        }

        if (this.props.onBlur) {
            this.props.onBlur(this.getRevisedEvent(event));
        }
    };

    Select.prototype.handleNativeClick = function handleNativeClick(event) {
        if (this.props.onClick) {
            this.props.onClick(this.getRevisedEvent(event));
        }
    };

    Select.prototype.handleMqMatchChange = function handleMqMatchChange(isMatched) {
        var _this9 = this;

        this.setState({
            isMobile: isMatched
        }, function () {
            _this9.setPopupTarget();
            _this9.updatePopupStyles();
        });
    };

    Select.prototype.handlePopupCloserClick = function handlePopupCloserClick() {
        this.setState({
            opened: false
        });
    };

    Select.prototype.setPopupRef = function setPopupRef(ref) {
        var _this10 = this;

        this.popup = ref;

        if (this.popup) {
            this.popup.setTarget(this.button.getNode());
        }

        if (this.props.renderPopupOnFocus) {
            var popupIsReady = !!this.popup;

            this.setState({
                popupIsReady: popupIsReady
            });

            if (popupIsReady) {
                setTimeout(function () {
                    _this10.focusOnMenu();
                }, 0);
            }
        }
    };

    Select.prototype.setMenuRef = function setMenuRef(menu) {
        this.menu = menu;
    };

    /**
     * Возвращает корневой `HTMLElement` компонента.
     *
     * @public
     * @returns {HTMLElement}
     */


    Select.prototype.getNode = function getNode() {
        return this.root;
    };

    /**
     * Устанавливает фокус на компонент.
     *
     * @public
     */


    Select.prototype.focus = function focus() {
        var _this11 = this;

        if (this.nativeSelect) {
            this.nativeSelect.focus();
        } else {
            this.button.focus();

            this.setState({
                opened: true
            }, function () {
                _this11.focusOnMenu();
            });
        }
    };

    /**
     * Убирает фокус с компонента.
     *
     * @public
     */


    Select.prototype.blur = function blur() {
        if (document.activeElement) {
            document.activeElement.blur();
        }
    };

    /**
     * Скроллит страницу до компонента.
     *
     * @public
     */


    Select.prototype.scrollTo = function scrollTo() {
        var elementRect = this.root.getBoundingClientRect();

        (0, _scrollTo3.default)({
            // eslint-disable-next-line no-mixed-operators
            targetY: elementRect.top + window.pageYOffset - _vars.SCROLL_TO_CORRECTION
        });
    };

    Select.prototype.focusOnMenu = function focusOnMenu() {
        if (!this.menu) {
            return;
        }

        if (this.state.isMobile && this.props.mobileMenuMode === 'popup') return;

        var scrollContainer = this.getScrollContainer();

        var posX = scrollContainer.scrollTop;
        var posY = scrollContainer.scrollLeft;

        this.menu.focus();
        scrollContainer.scrollTop = posX;
        scrollContainer.scrollLeft = posY;
    };

    /**
     * @param {MenuItem} highlightedItem Выбранный в текущий момент пункт меню
     */


    Select.prototype.scrollToHighlightedItem = function scrollToHighlightedItem(highlightedItem) {
        var element = highlightedItem.getNode();
        var container = this.popup.getInnerNode();
        var correction = element.offsetHeight;

        if (container) {
            if (element.offsetTop + correction > container.scrollTop + container.offsetHeight) {
                (0, _scrollTo3.default)({
                    container: container,
                    targetY: element.offsetTop,
                    duration: _vars.SCROLL_TO_NORMAL_DURATION
                });
            } else if (element.offsetTop < container.scrollTop) {
                (0, _scrollTo3.default)({
                    container: container,
                    // eslint-disable-next-line no-mixed-operators
                    targetY: element.offsetTop - container.offsetHeight + correction,
                    duration: _vars.SCROLL_TO_NORMAL_DURATION
                });
            }
        }
    };

    Select.prototype.toggleOpened = function toggleOpened() {
        var _this12 = this;

        var newOpenedState = !this.getOpened();

        this.setState({
            opened: newOpenedState
        }, function () {
            if (newOpenedState) {
                _this12.focusOnMenu();
            }
        });
    };

    Select.prototype.updatePopupStyles = function updatePopupStyles() {
        var buttonWidth = this.button.getNode().getBoundingClientRect().width;
        var popupStyles = { minWidth: buttonWidth };

        if (this.props.equalPopupWidth) {
            popupStyles.maxWidth = buttonWidth;
        }

        this.setState({ popupStyles: popupStyles });
    };

    Select.prototype.setPopupTarget = function setPopupTarget() {
        if (this.popup) {
            this.popup.setTarget(this.button.getNode());
        }
    };

    Select.prototype.getCheckedItems = function getCheckedItems(options) {
        var _this13 = this;

        var value = this.getValue();
        var result = [];

        options.forEach(function (option) {
            if (option.type === 'group' && !!option.content) {
                var findInGroup = _this13.getCheckedItems(option.content);
                result = result.concat(findInGroup);
            } else if (value.indexOf(option.value) !== -1) {
                result.push(option);
            }
        });

        return result;
    };

    /**
     * @returns {Boolean}
     */


    Select.prototype.getOpened = function getOpened() {
        return this.props.opened !== undefined ? this.props.opened : this.state.opened;
    };

    Select.prototype.getRevisedEvent = function getRevisedEvent(event) {
        return Object.assign({}, event, { target: Object.assign({}, event.target, { value: this.getValue() }) });
    };

    /**
     * @returns {Array<String|Number>}
     */


    Select.prototype.getValue = function getValue() {
        return this.props.value || this.state.value;
    };

    /**
     * @returns {HTMLElement}
     */


    Select.prototype.getScrollContainer = function getScrollContainer() {
        return this.context.positioningContainerElement || document.body;
    };

    Select.prototype.isAutoSelectRequired = function isAutoSelectRequired() {
        var _props = this.props,
            mode = _props.mode,
            options = _props.options,
            renderPopupOnFocus = _props.renderPopupOnFocus;


        return renderPopupOnFocus && mode === 'radio' && options.length > 0 && !this.hasCheckedItems();
    };

    Select.prototype.hasCheckedItems = function hasCheckedItems() {
        var options = this.props.options;

        var checkedItems = this.getCheckedItems(options);

        return checkedItems.length > 0;
    };

    Select.prototype.selectFirstOption = function selectFirstOption() {
        var firstOption = this.getFirstOption(this.props.options);
        this.handleOptionCheck([firstOption.value]);
    };

    Select.prototype.getFirstOption = function getFirstOption(options) {
        var firstOption = options[0];

        if (firstOption.type === 'group') {
            return this.getFirstOption(firstOption.content);
        }

        return firstOption;
    };

    return Select;
}(_react2.default.Component), _class4.defaultProps = {
    mode: 'check',
    groupView: 'default',
    disabled: false,
    size: 'm',
    directions: ['bottom-left', 'bottom-right', 'top-left', 'top-right'],
    view: 'default',
    width: 'default',
    equalPopupWidth: false,
    options: [],
    mobileTitle: DEFAULT_TEXT_FALLBACK,
    nativeOptionPlaceholder: DEFAULT_TEXT_FALLBACK,
    mobileMenuMode: 'native',
    renderPopupOnFocus: false
}, _class4.contextTypes = {
    positioningContainerElement: _propTypes3.HtmlElement
}, _temp2), (_applyDecoratedDescriptor(_class3.prototype, 'handleButtonClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleButtonClick'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleButtonKeyDown', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleButtonKeyDown'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleButtonFocus', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleButtonFocus'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleButtonBlur', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleButtonBlur'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleMenuFocus', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleMenuFocus'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleMenuBlur', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleMenuBlur'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleMenuHighlightItem', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleMenuHighlightItem'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleOptionCheck', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleOptionCheck'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleNativeOptionCheck', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleNativeOptionCheck'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleClickOutside', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleClickOutside'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleMenuKeyDown', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleMenuKeyDown'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleNativeFocus', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleNativeFocus'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleNativeBlur', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleNativeBlur'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleNativeClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleNativeClick'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handleMqMatchChange', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handleMqMatchChange'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'handlePopupCloserClick', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'handlePopupCloserClick'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'setPopupRef', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'setPopupRef'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'setMenuRef', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'setMenuRef'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'updatePopupStyles', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'updatePopupStyles'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'setPopupTarget', [_autobind2.default], Object.getOwnPropertyDescriptor(_class3.prototype, 'setPopupTarget'), _class3.prototype)), _class3)) || _class2) || _class2);
Select.propTypes =  true ? {
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /** Тип выпадающего списка */
    mode: _propTypes2.default.oneOf(['check', 'radio', 'radio-check']),
    /** Размещение заголовка групп: обычное или в одну строку с первым элементом группы */
    groupView: _propTypes2.default.oneOf(['default', 'line']),
    /** Тип поля (filled только на белом фоне в размере m) */
    view: _propTypes2.default.oneOf(['default', 'filled']),
    /** Управление возможностью компонента занимать всю ширину родителя */
    width: _propTypes2.default.oneOf(['default', 'available']),
    /** Направления, в которые может открываться попап компонента */
    directions: _propTypes2.default.arrayOf(_propTypes2.default.oneOf(['top-left', 'top-center', 'top-right', 'left-top', 'left-center', 'left-bottom', 'right-top', 'right-center', 'right-bottom', 'bottom-left', 'bottom-center', 'bottom-right'])),
    /** Управление возможностью редактирования значения */
    disabled: _propTypes2.default.bool,
    /** Управление видимостью выпадающего списка */
    opened: _propTypes2.default.bool,
    /** Ширинa выпадающего списка равна ширине кнопки */
    equalPopupWidth: _propTypes2.default.bool,
    /** Список выбранных значений */
    value: _propTypes2.default.arrayOf(_propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number])),
    /** Список вариантов выбора */
    options: _propTypes2.default.arrayOf(_propTypes2.default.shape({
        /** Тип списка вариантов */
        type: _propTypes2.default.oneOf(['item', 'group']),
        /** Уникальное значение, которое будет отправлено на сервер, если вариант выбран */
        value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
        /** Текст варианта */
        text: _propTypes2.default.node,
        /** Текст варианта для нативного режима */
        nativeText: _propTypes2.default.string,
        /** Отображение варианта */
        description: _propTypes2.default.node,
        /** Текст, который будет отображаться при выборе */
        checkedText: _propTypes2.default.string,
        /** Иконка варианта */
        icon: _propTypes2.default.node,
        /** Список вариантов, только для type='group' */
        content: _propTypes2.default.array,
        /** Только для type='item': свойства для компонента [MenuItem](#!/MenuItem) */
        props: _propTypes2.default.object
    })),
    /** Вставляет попап со списком только если элемент активен */
    renderPopupOnFocus: _propTypes2.default.bool,
    /** Размер компонента */
    size: _propTypes2.default.oneOf(['s', 'm', 'l', 'xl']),
    /** Уникальный идентификатор блока */
    id: _propTypes2.default.string,
    /** Уникальное имя блока */
    name: _propTypes2.default.string,
    /** Лейбл для поля */
    label: _propTypes2.default.node,
    /** Подсказка в поле */
    placeholder: _propTypes2.default.string,
    /** Подсказка в качестве неактивного первого варианта выбора для нативного мобильного контрола */
    nativeOptionPlaceholder: _propTypes2.default.string,
    /** Подсказка под полем */
    hint: _propTypes2.default.node,
    /** Отображение ошибки */
    error: _propTypes2.default.node,
    /** Управление нативным режимом на мобильных устройствах */
    mobileMenuMode: _propTypes2.default.oneOf(['native', 'popup']),
    /** Подсказка над меню в мобильном режиме */
    mobileTitle: _propTypes2.default.node,
    /** Смещение в пикселях всплывающего окна относительно основного направления (только на десктопе) */
    popupMainOffset: _propTypes2.default.number,
    /** Смещение в пикселях всплывающего окна относительно второстепенного направления (только на десктопе) */
    popupSecondaryOffset: _propTypes2.default.number,
    /** Скрытие галочки в правой части кнопки */
    hideTick: _propTypes2.default.bool,
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /**
     * Обработчик фокуса на компоненте
     * @param {React.FocusEvent} event
     */
    onFocus: _propTypes2.default.func,
    /**
     * Обработчик потери фокуса компонентом
     * @param {React.FocusEvent} event
     */
    onBlur: _propTypes2.default.func,
    /**
     * Обработчик фокуса на кнопке
     * @param {React.FocusEvent} event
     */
    onButtonFocus: _propTypes2.default.func,
    /**
     * Обработчик потери у кнопки
     * @param {React.FocusEvent} event
     */
    onButtonBlur: _propTypes2.default.func,
    /**
     * Обработчик фокуса на меню
     * @param {React.FocusEvent} event
     */
    onMenuFocus: _propTypes2.default.func,
    /**
     * Обработчик потери фокуса у меню
     * @param {React.FocusEvent} event
     */
    onMenuBlur: _propTypes2.default.func,
    /**
     * Обработчик клика по кнопке компонента
     * @param {React.MouseEvent} event
     */
    onClick: _propTypes2.default.func,
    /**
     * Обработчик клика вне компонента
     * @param {React.MouseEvent} event
     */
    onClickOutside: _propTypes2.default.func,
    /**
     * Обработчик изменения значения
     * @param {Array<string|number>} value
     */
    onChange: _propTypes2.default.func,
    /**
     * Обработчик нажатия на клавишу
     * @param {React.KeyboardEvent} event
     */
    onKeyDown: _propTypes2.default.func,
    /** Кастомный метод рендера содержимого кнопки, принимает на вход: массив элементов типа CheckedOption */
    renderButtonContent: _propTypes2.default.func,
    /** Максимальная высота попапа */
    maxHeight: _propTypes2.default.number,
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = Select;
//# sourceMappingURL=select.js.map


/***/ }),

/***/ "./node_modules/arui-feather/select/select_theme_alfa-on-color.css":
/*!*************************************************************************!*\
  !*** ./node_modules/arui-feather/select/select_theme_alfa-on-color.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"select_theme_alfa-on-color": "_1OCLLCx5RAvo005FQXdruJ",
	"select__top": "_2lki9GXMKxSq-xlKSF0WEI",
	"select__sub": "Ml_Z-thWsXgO22AANhA4A",
	"select__placeholder": "_1YBPS9W1MIe5PFelunm2y8",
	"select-button": "khWzrMqQkZhJVPe946lKE",
	"select-button_hovered": "qZCRuPq_0qedAEejx8ZR",
	"select-button_focused": "_2gYp6Xm7Uct0ukj5V9ks1o",
	"select-button_pressed": "_3u26GuRLZu2j_otHRUDcpc",
	"select-button_disabled": "_3NnG8PT21VM_M8byPUAoDT",
	"select_opened": "_3OJHKBmnmnodBY3jGel4Ja",
	"select_invalid": "_3fCSI9cnS45K0R_qaCYfZz"
};

/***/ }),

/***/ "./node_modules/arui-feather/select/select_theme_alfa-on-white.css":
/*!*************************************************************************!*\
  !*** ./node_modules/arui-feather/select/select_theme_alfa-on-white.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"select_theme_alfa-on-white": "_1wcV98p_ssPUK4f19Ha77h",
	"select__top": "eWnlKb-px-eb3459vy9T7",
	"select__sub": "_25iAxq2Aso8vohz0P1Lpi0",
	"select__placeholder": "_1RpHSAaAZ3kZ7aJ_esxC9J",
	"select-button": "_37VztfzkPI7wlOwwOp36Fi",
	"select-button_hovered": "_3dOFze3VR3zXbW5nE8n1Of",
	"select-button_focused": "xpnx2uv7Hg0EYpQY-leo_",
	"select-button_pressed": "gCWn726lQ53C_KEfH9umV",
	"select-button_disabled": "_31-Ce4DVa0-dtS1u2ChIGx",
	"select_opened": "_3nGolJqu1DvHUCRibFgXqf",
	"select_invalid": "_1iIxScNDYcNPS92E8BSZnH"
};

/***/ }),

/***/ "./node_modules/arui-feather/spin/index.js":
/*!*************************************************!*\
  !*** ./node_modules/arui-feather/spin/index.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

__webpack_require__(/*! ./spin.css */ "./node_modules/arui-feather/spin/spin.css");

var _spin = __webpack_require__(/*! ./spin */ "./node_modules/arui-feather/spin/spin.js");

var _spin2 = _interopRequireDefault(_spin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

exports.default = _spin2.default;
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/spin/spin.css":
/*!*************************************************!*\
  !*** ./node_modules/arui-feather/spin/spin.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"spin": "cTwrQh-w1vCa54UnHTVAX",
	"spin_animation": "_2rV_xgGfgaxtHcx4Xe4oqO",
	"spin_visible": "_2ikqNWaGXKjqW2mOnob0dJ",
	"spin_size_s": "_1LNacnkPw_qXX1saHCZK6D",
	"spin_size_m": "_5kQyiqNN4aYrRPG138OIV",
	"spin_size_l": "Hinjdf-Nq7GJ_VQJGdupJ",
	"spin_size_xl": "_37Vyp-ADJrAhbASoEPrvcH",
	"spin_theme_alfa-on-color": "_3TDhH1HDNDEQVIMjdhOxCT",
	"spin_theme_alfa-on-white": "_2IJUK4gbkXS4-oomkg_7yT"
};

/***/ }),

/***/ "./node_modules/arui-feather/spin/spin.js":
/*!************************************************!*\
  !*** ./node_modules/arui-feather/spin/spin.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _dec, _dec2, _class, _class2, _temp; /* This Source Code Form is subject to the terms of the Mozilla Public
                                          * License, v. 2.0. If a copy of the MPL was not distributed with this
                                          * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _cn = __webpack_require__(/*! ../cn */ "./node_modules/arui-feather/cn.js");

var _cn2 = _interopRequireDefault(_cn);

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

/**
 * Компонент показывающий крутящееся кольцо загрузки.
 */
var Spin = (_dec = (0, _cn2.default)('spin'), _dec2 = (0, _performance2.default)(), _dec(_class = _dec2(_class = (_temp = _class2 = function (_React$Component) {
    _inherits(Spin, _React$Component);

    function Spin() {
        _classCallCheck(this, Spin);

        return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
    }

    Spin.prototype.render = function render(cn) {
        return _react2.default.createElement('span', {
            className: cn({
                size: this.props.size,
                visible: this.props.visible
            }),
            id: this.props.id,
            'data-test-id': this.props['data-test-id']
        });
    };

    return Spin;
}(_react2.default.Component), _class2.defaultProps = {
    visible: false,
    size: 'm'
}, _temp)) || _class) || _class);
Spin.propTypes =  true ? {
    /** Управление видимостью компонента */
    visible: _propTypes2.default.bool,
    /** Размер компонента */
    size: _propTypes2.default.oneOf(['s', 'm', 'l', 'xl']),
    /** Тема компонента */
    theme: _propTypes2.default.oneOf(['alfa-on-color', 'alfa-on-white']),
    /** Дополнительный класс */
    className: _propTypes2.default.string,
    /** Идентификатор компонента в DOM */
    id: _propTypes2.default.string,
    /** Идентификатор для систем автоматизированного тестирования */
    'data-test-id': _propTypes2.default.string
} : undefined;
exports.default = Spin;
//# sourceMappingURL=spin.js.map


/***/ }),

/***/ "./node_modules/arui-feather/swipeable/index.js":
/*!******************************************************!*\
  !*** ./node_modules/arui-feather/swipeable/index.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = undefined;

var _swipeable = __webpack_require__(/*! ./swipeable */ "./node_modules/arui-feather/swipeable/swipeable.js");

var _swipeable2 = _interopRequireDefault(_swipeable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _swipeable2.default;
//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/arui-feather/swipeable/swipeable.js":
/*!**********************************************************!*\
  !*** ./node_modules/arui-feather/swipeable/swipeable.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = exports.getCoordinates = undefined;

var _dec, _class, _desc, _value, _class2, _class3, _temp2;

var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _autobind = __webpack_require__(/*! core-decorators/lib/autobind */ "core-decorators/lib/autobind");

var _autobind2 = _interopRequireDefault(_autobind);

var _performance = __webpack_require__(/*! ../performance */ "./node_modules/arui-feather/performance.js");

var _performance2 = _interopRequireDefault(_performance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

var getCoordinates = exports.getCoordinates = function getCoordinates(_ref) {
    var touches = _ref.touches,
        changedTouches = _ref.changedTouches,
        clientX = _ref.clientX,
        clientY = _ref.clientY;
    return touches && changedTouches ? { clientX: (touches[0] || changedTouches[0]).clientX, clientY: (touches[0] || changedTouches[0]).clientY } : { clientX: clientX, clientY: clientY };
};

var Swipeable = (_dec = (0, _performance2.default)(), _dec(_class = (_class2 = (_temp2 = _class3 = function (_React$Component) {
    _inherits(Swipeable, _React$Component);

    function Swipeable() {
        var _temp, _this, _ret;

        _classCallCheck(this, Swipeable);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.swipeStartX = 0, _this.swipeStartY = 0, _this.deltaX = 0, _this.deltaY = 0, _temp), _possibleConstructorReturn(_this, _ret);
    }

    Swipeable.prototype.componentWillUnmount = function componentWillUnmount() {
        this.removeListeners();
    };

    Swipeable.prototype.render = function render() {
        return _react2.default.cloneElement(this.props.children, {
            onMouseDown: this.handleMouseDown,
            onTouchStart: this.handleTouchStart
        });
    };

    Swipeable.prototype.handleMouseDown = function handleMouseDown(event) {
        if (this.props.children.props.onMouseDown) {
            this.props.children.props.onMouseDown(event);
        }

        this.handleSwipeStart(event);

        document.addEventListener('mouseup', this.handleSwipeEnd);
        document.addEventListener('mouseup', this.removeListeners);
    };

    Swipeable.prototype.handleTouchStart = function handleTouchStart(event) {
        if (event.touches && event.touches.length > 1) return;

        if (this.props.children.props.onTouchStart) {
            this.props.children.props.onTouchStart(event);
        }

        this.handleSwipeStart(event);

        document.addEventListener('touchend', this.handleSwipeEnd);
        document.addEventListener('touchend', this.removeListeners);
        document.addEventListener('touchcancel', this.removeListeners);
    };

    Swipeable.prototype.handleSwipeStart = function handleSwipeStart(event) {
        var _getCoordinates = getCoordinates(event),
            clientX = _getCoordinates.clientX,
            clientY = _getCoordinates.clientY;

        this.swipeStartX = clientX;
        this.swipeStartY = clientY;
    };

    Swipeable.prototype.handleSwipeEnd = function handleSwipeEnd(event) {
        var _props = this.props,
            delta = _props.delta,
            onSwipe = _props.onSwipe;

        var _getCoordinates2 = getCoordinates(event),
            clientX = _getCoordinates2.clientX,
            clientY = _getCoordinates2.clientY;

        this.deltaX = this.swipeStartX - clientX;
        this.deltaY = this.swipeStartY - clientY;

        if (typeof onSwipe === 'function') {
            if (this.deltaX > delta) {
                onSwipe('left');
            } else if (this.deltaX < -delta) {
                onSwipe('right');
            } else if (this.deltaY > delta) {
                onSwipe('top');
            } else if (this.deltaY < -delta) {
                onSwipe('bottom');
            }
        }
    };

    Swipeable.prototype.removeListeners = function removeListeners() {
        document.removeEventListener('mouseup', this.handleSwipeEnd);
        document.removeEventListener('mouseup', this.removeListeners);
        document.removeEventListener('touchend', this.handleSwipeEnd);
        document.removeEventListener('touchend', this.removeListeners);
        document.removeEventListener('touchcancel', this.removeListeners);
        this.swipeStartX = 0;
        this.swipeStartY = 0;
        this.deltaX = 0;
        this.deltaY = 0;
    };

    return Swipeable;
}(_react2.default.Component), _class3.defaultProps = {
    delta: 100
}, _temp2), (_applyDecoratedDescriptor(_class2.prototype, 'handleMouseDown', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleMouseDown'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleTouchStart', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleTouchStart'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleSwipeStart', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleSwipeStart'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'handleSwipeEnd', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'handleSwipeEnd'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'removeListeners', [_autobind2.default], Object.getOwnPropertyDescriptor(_class2.prototype, 'removeListeners'), _class2.prototype)), _class2)) || _class);
exports.default = Swipeable;
Swipeable.propTypes =  true ? {
    /** Число пикселей, на которое нужно сместиться, чтобы запустить функцию по свайпу */
    delta: _propTypes2.default.number,
    /**
     * Функция запускаемая по свайпу.
     * @param {string} direction Направление свайпа. Возможные значение - 'top', 'right', 'bottom', 'left'.
     */
    onSwipe: _propTypes2.default.func.isRequired,
    /** Дочерний компонент представленный в виде единичного элемента */
    children: _propTypes2.default.shape({
        props: _propTypes2.default.shape({
            onMouseDown: _propTypes2.default.func,
            onTouchStart: _propTypes2.default.func
        })
    }).isRequired
} : undefined;
//# sourceMappingURL=swipeable.js.map


/***/ }),

/***/ "./node_modules/arui-feather/vars.js":
/*!*******************************************!*\
  !*** ./node_modules/arui-feather/vars.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* eslint max-len: 0 */

var SCROLL_TO_CORRECTION = exports.SCROLL_TO_CORRECTION = 16;
var SCROLL_TO_DEFAULT_DURATION = exports.SCROLL_TO_DEFAULT_DURATION = 0;
var SCROLL_TO_NORMAL_DURATION = exports.SCROLL_TO_NORMAL_DURATION = 250;
var SCROLL_TO_EASING = exports.SCROLL_TO_EASING = 'easeInOutSine';

var POPUP_MAIN_OFFSET = exports.POPUP_MAIN_OFFSET = 12;

var LOREM_IPSUM = exports.LOREM_IPSUM = ['Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ', 'Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. ', 'Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. ', 'Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus. ', 'Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna. ', 'Etiam maximus velit quis massa lacinia laoreet. Curabitur egestas, massa vel ullamcorper cursus, sem augue faucibus orci, eu mattis erat mi at arcu. In at ligula suscipit, bibendum nisl vel, hendrerit ex. ', 'Quisque sagittis vehicula augue a tristique. Suspendisse quis accumsan quam, placerat ornare sapien. Phasellus consectetur nibh non est molestie malesuada. ', 'Nullam tortor ex, commodo id tristique non, auctor a mi. Donec at feugiat dui, in efficitur ex. Fusce molestie tortor quis leo mattis lobortis. Maecenas luctus risus lorem, id aliquet eros commodo at. ', 'Fusce lacus sapien, euismod gravida purus non, dignissim sollicitudin arcu. Ut facilisis massa eros, vel hendrerit eros luctus at. Sed dictum ligula ex, et viverra mauris varius non. ', 'Sed rhoncus condimentum velit at fringilla. Mauris dignissim posuere vestibulum. Nulla sed velit vel ligula semper varius. Quisque gravida nec massa eu fringilla. ', 'Duis risus ligula, iaculis vel rutrum eget, varius vitae massa. Proin non accumsan purus. Fusce non augue in enim ullamcorper aliquet a quis quam. '];
//# sourceMappingURL=vars.js.map


/***/ }),

/***/ "./pages/App.js":
/*!**********************!*\
  !*** ./pages/App.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _src_components_FormComponent_FormComponent_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./src/components/FormComponent/FormComponent.js */ "./pages/src/components/FormComponent/FormComponent.js");
/* harmony import */ var _Store_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Store.js */ "./pages/Store.js");
var _jsxFileName = "C:\\Users\\Xiaomi\\WebstormProjects\\React-apps\\test-task-test\\my-app\\pages\\App.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;





axios__WEBPACK_IMPORTED_MODULE_3___default.a.defaults.baseURL = ' http://test.clevertec.ru/tt';

function App() {
  return __jsx(react_redux__WEBPACK_IMPORTED_MODULE_2__["Provider"], {
    store: _Store_js__WEBPACK_IMPORTED_MODULE_5__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, __jsx(_src_components_FormComponent_FormComponent_js__WEBPACK_IMPORTED_MODULE_4__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "966911003",
    __self: this
  }, "*.jsx-966911003{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xcWGlhb21pXFxXZWJzdG9ybVByb2plY3RzXFxSZWFjdC1hcHBzXFx0ZXN0LXRhc2stdGVzdFxcbXktYXBwXFxwYWdlc1xcQXBwLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWF3QixBQUc4QiwwRUFDUyw4RUFDSCw2RkFDSSxtR0FFdkIiLCJmaWxlIjoiQzpcXFVzZXJzXFxYaWFvbWlcXFdlYnN0b3JtUHJvamVjdHNcXFJlYWN0LWFwcHNcXHRlc3QtdGFzay10ZXN0XFxteS1hcHBcXHBhZ2VzXFxBcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHtQcm92aWRlcn0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnO1xuXG5pbXBvcnQgRm9ybUNvbXBvbmVudCBmcm9tIFwiLi9zcmMvY29tcG9uZW50cy9Gb3JtQ29tcG9uZW50L0Zvcm1Db21wb25lbnQuanNcIjtcbmltcG9ydCBzdG9yZSBmcm9tIFwiLi9TdG9yZS5qc1wiO1xuXG5heGlvcy5kZWZhdWx0cy5iYXNlVVJMID0gJyBodHRwOi8vdGVzdC5jbGV2ZXJ0ZWMucnUvdHQnO1xuXG5mdW5jdGlvbiBBcHAoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgICAgPFByb3ZpZGVyIHN0b3JlPXtzdG9yZX0+XG4gICAgICAgICAgICA8Rm9ybUNvbXBvbmVudC8+XG4gICAgICAgICAgICA8c3R5bGUganN4PntgXG4gICAgICAgICAgICAgICAgKntcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGB9PC9zdHlsZT5cbiAgICAgICAgPC9Qcm92aWRlcj5cblxuXG4gICAgKTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgQXBwO1xuIl19 */\n/*@ sourceURL=C:\\Users\\Xiaomi\\WebstormProjects\\React-apps\\test-task-test\\my-app\\pages\\App.js */"));
}

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./pages/Store.js":
/*!************************!*\
  !*** ./pages/Store.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-thunk */ "redux-thunk");
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_thunk__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _src_reducers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./src/reducers */ "./pages/src/reducers/index.js");



const initialState = {};
const middleware = [redux_thunk__WEBPACK_IMPORTED_MODULE_1___default.a];
const store = Object(redux__WEBPACK_IMPORTED_MODULE_0__["createStore"])(_src_reducers__WEBPACK_IMPORTED_MODULE_2__["default"], initialState, Object(redux__WEBPACK_IMPORTED_MODULE_0__["applyMiddleware"])(...middleware));
/* harmony default export */ __webpack_exports__["default"] = (store);

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "react-dom");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _App_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./App.js */ "./pages/App.js");
var _jsxFileName = "C:\\Users\\Xiaomi\\WebstormProjects\\React-apps\\test-task-test\\my-app\\pages\\index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


/*function Home() {
    return <div>Welcome to Next.js!</div>
}

export default Home;*/


react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render(__jsx(_App_js__WEBPACK_IMPORTED_MODULE_2__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 12
  },
  __self: undefined
}), document.getElementById('root'));

/***/ }),

/***/ "./pages/src/actions/FormActions/formActions.js":
/*!******************************************************!*\
  !*** ./pages/src/actions/FormActions/formActions.js ***!
  \******************************************************/
/*! exports provided: getFormData, setInputValue, sendFormData, setSendNotificationStatus */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFormData", function() { return getFormData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setInputValue", function() { return setInputValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sendFormData", function() { return sendFormData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setSendNotificationStatus", function() { return setSendNotificationStatus; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _formTypes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./formTypes */ "./pages/src/actions/FormActions/formTypes.js");


const getFormData = () => async dispatch => {
  try {
    // await axios.post('/meta');
    const res = {
      title: 'My Test Form',
      image: 'https://resheto.net/images/mater/GoodMorning/kartinki_s_dobrym_utrom_24.jpeg',
      fields: [{
        title: 'text',
        name: 'text',
        type: 'TEXT',
        values: ''
      }, {
        title: 'numeric',
        name: 'numeric',
        type: 'NUMERIC',
        values: ''
      }, {
        title: 'list',
        name: 'list',
        type: 'LIST',
        values: {
          none: "Не выбрано",
          v1: "Первое значение",
          v2: "Второе значение",
          v3: "Третье значение"
        }
      }]
    };
    dispatch({
      type: _formTypes__WEBPACK_IMPORTED_MODULE_1__["GET_FORM_DATA"],
      payload: res
    });
  } catch (error) {
    console.log(error);
  }
};
const setInputValue = input => dispatch => {
  console.log('input', input);
  dispatch({
    type: _formTypes__WEBPACK_IMPORTED_MODULE_1__["SET_INPUT_VALUE"],
    payload: input
  });
};
const sendFormData = e => async dispatch => {
  try {
    dispatch({
      type: _formTypes__WEBPACK_IMPORTED_MODULE_1__["SET_SENDING_STATUS"],
      payload: true
    }); // const res = await axios.post('/data', null, e);

    setTimeout(() => {
      dispatch({
        type: _formTypes__WEBPACK_IMPORTED_MODULE_1__["SET_SENDING_STATUS"],
        payload: false
      });
    }, 1000);
    setTimeout(() => {
      dispatch({
        type: _formTypes__WEBPACK_IMPORTED_MODULE_1__["SET_SEND_NOTIFICATION_STATUS"],
        payload: true
      });
    }, 1010);
    ;
    /*dispatch({
        type: SEND_FORM_DATA
    });*/

    console.log('target', e);
  } catch (error) {
    console.log('error', error);
  }
};
const setSendNotificationStatus = status => dispatch => {
  dispatch({
    type: _formTypes__WEBPACK_IMPORTED_MODULE_1__["SET_SEND_NOTIFICATION_STATUS"],
    payload: status
  });
};

/***/ }),

/***/ "./pages/src/actions/FormActions/formTypes.js":
/*!****************************************************!*\
  !*** ./pages/src/actions/FormActions/formTypes.js ***!
  \****************************************************/
/*! exports provided: GET_FORM_DATA, SEND_FORM_DATA, SET_INPUT_VALUE, SET_SENDING_STATUS, SET_SEND_NOTIFICATION_STATUS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_FORM_DATA", function() { return GET_FORM_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SEND_FORM_DATA", function() { return SEND_FORM_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_INPUT_VALUE", function() { return SET_INPUT_VALUE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_SENDING_STATUS", function() { return SET_SENDING_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_SEND_NOTIFICATION_STATUS", function() { return SET_SEND_NOTIFICATION_STATUS; });
const GET_FORM_DATA = 'GET_FORM_DATA';
const SEND_FORM_DATA = 'SEND_FORM_DATA';
const SET_INPUT_VALUE = 'SET_INPUT_VALUE';
const SET_SENDING_STATUS = 'SET_SENDING_STATUS';
const SET_SEND_NOTIFICATION_STATUS = 'SET_SEND_NOTIFICATION_STATUS';

/***/ }),

/***/ "./pages/src/components/FormComponent/FormComponent.js":
/*!*************************************************************!*\
  !*** ./pages/src/components/FormComponent/FormComponent.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "@babel/runtime-corejs2/core-js/object/define-property");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-properties */ "@babel/runtime-corejs2/core-js/object/define-properties");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/get-own-property-descriptors */ "@babel/runtime-corejs2/core-js/object/get-own-property-descriptors");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/get-own-property-descriptor */ "@babel/runtime-corejs2/core-js/object/get-own-property-descriptor");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/get-own-property-symbols */ "@babel/runtime-corejs2/core-js/object/get-own-property-symbols");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/keys */ "@babel/runtime-corejs2/core-js/object/keys");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "@babel/runtime-corejs2/helpers/esm/defineProperty");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _actions_FormActions_formActions__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../actions/FormActions/formActions */ "./pages/src/actions/FormActions/formActions.js");
/* harmony import */ var arui_feather_input__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! arui-feather/input */ "./node_modules/arui-feather/input/index.js");
/* harmony import */ var arui_feather_input__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(arui_feather_input__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var arui_feather_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! arui-feather/button */ "./node_modules/arui-feather/button/index.js");
/* harmony import */ var arui_feather_button__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(arui_feather_button__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var arui_feather_form__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! arui-feather/form */ "./node_modules/arui-feather/form/index.js");
/* harmony import */ var arui_feather_form__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(arui_feather_form__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var arui_feather_select__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! arui-feather/select */ "./node_modules/arui-feather/select/index.js");
/* harmony import */ var arui_feather_select__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(arui_feather_select__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var arui_feather_spin__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! arui-feather/spin */ "./node_modules/arui-feather/spin/index.js");
/* harmony import */ var arui_feather_spin__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(arui_feather_spin__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var arui_feather_notification__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! arui-feather/notification */ "./node_modules/arui-feather/notification/index.js");
/* harmony import */ var arui_feather_notification__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(arui_feather_notification__WEBPACK_IMPORTED_MODULE_16__);







var _jsxFileName = "C:\\Users\\Xiaomi\\WebstormProjects\\React-apps\\test-task-test\\my-app\\pages\\src\\components\\FormComponent\\FormComponent.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5___default()(object); if (_babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4___default.a) { var symbols = _babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4___default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3___default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6___default()(target, key, source[key]); }); } else if (_babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2___default.a) { _babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1___default()(target, _babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2___default()(source)); } else { ownKeys(source).forEach(function (key) { _babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(target, key, _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3___default()(source, key)); }); } } return target; }











class FormComponent extends react__WEBPACK_IMPORTED_MODULE_8__["Component"] {
  constructor(...args) {
    super(...args);

    _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6___default()(this, "state", {
      finalForm: {}
    });

    _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6___default()(this, "handleChange", (e, name) => {
      this.setState({
        finalForm: _objectSpread({}, this.state.finalForm, {
          [name]: e
        })
      }); //console.log(name);
      //setInputValue({value: e, name});
    });

    _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6___default()(this, "onSubmit", e => {
      //console.log('hello, my friend');
      this.props.sendFormData(this.state.finalForm);
    });

    _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6___default()(this, "setInputType", (type, values, name) => {
      switch (type) {
        case 'TEXT':
          return __jsx(arui_feather_input__WEBPACK_IMPORTED_MODULE_11___default.a, {
            type: "text",
            name: name,
            onChange: e => this.handleChange(e, name),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 47
            },
            __self: this
          });

        case 'NUMERIC':
          return __jsx(arui_feather_input__WEBPACK_IMPORTED_MODULE_11___default.a, {
            type: "number",
            name: name,
            onChange: e => this.handleChange(e, name),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 49
            },
            __self: this
          });

        case 'LIST':
          const options = [{
            value: values.none,
            text: values.none
          }, {
            value: values.v1,
            text: values.v1
          }, {
            value: values.v2,
            text: values.v2
          }, {
            value: values.v3,
            text: values.v3
          }];
          return __jsx(arui_feather_select__WEBPACK_IMPORTED_MODULE_14___default.a, {
            name: name,
            mode: "radio-check",
            options: options,
            onChange: e => this.handleChange(e[0], name),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 57
            },
            __self: this
          });
      }
    });
  }

  componentDidMount() {
    this.props.getFormData();
  }

  render() {
    const {
      form,
      isFormSending,
      isSentNotification,
      setSendNotificationStatus
    } = this.props; //console.log(this.props);

    return form ? __jsx(arui_feather_form__WEBPACK_IMPORTED_MODULE_13___default.a, {
      onSubmit: this.onSubmit,
      className: "App",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67
      },
      __self: this
    }, __jsx("h2", {
      className: "jsx-3611114273",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68
      },
      __self: this
    }, form.title), __jsx("img", {
      src: form.image,
      alt: "image",
      className: "jsx-3611114273",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69
      },
      __self: this
    }), __jsx("table", {
      border: "1",
      className: "jsx-3611114273",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, __jsx("tbody", {
      className: "jsx-3611114273",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71
      },
      __self: this
    }, form.fields.map((field, index) => __jsx("tr", {
      key: index,
      className: "jsx-3611114273",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 73
      },
      __self: this
    }, __jsx("td", {
      className: "jsx-3611114273",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 74
      },
      __self: this
    }, __jsx("h5", {
      className: "jsx-3611114273",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75
      },
      __self: this
    }, field.title)), __jsx("td", {
      className: "jsx-3611114273",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77
      },
      __self: this
    }, this.setInputType(field.type, field.values, field.name)))))), __jsx(arui_feather_button__WEBPACK_IMPORTED_MODULE_12___default.a, {
      view: "extra",
      type: "submit",
      icon: __jsx(arui_feather_spin__WEBPACK_IMPORTED_MODULE_15___default.a, {
        visible: isFormSending,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81
        },
        __self: this
      }),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }, "Send"), __jsx(arui_feather_notification__WEBPACK_IMPORTED_MODULE_16___default.a, {
      visible: isSentNotification,
      status: "ok",
      offset: 12,
      stickTo: "left",
      title: "Success",
      onCloseTimeout: () => {
        setSendNotificationStatus(false);
      },
      onCloserClick: () => {
        setSendNotificationStatus(false);
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }, "Form has been sent."), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_7___default.a, {
      id: "3611114273",
      __self: this
    }, "h2.jsx-3611114273{margin-bottom:0;padding:0;}img.jsx-3611114273{width:500px;height:350px;margin:20px 0;border-radius:10%;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xcWGlhb21pXFxXZWJzdG9ybVByb2plY3RzXFxSZWFjdC1hcHBzXFx0ZXN0LXRhc2stdGVzdFxcbXktYXBwXFxwYWdlc1xcc3JjXFxjb21wb25lbnRzXFxGb3JtQ29tcG9uZW50XFxGb3JtQ29tcG9uZW50LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWdHNEIsQUFHeUMsQUFJSixZQUNDLElBSkgsU0FLSSxDQUpsQixhQUtzQixrQkFDdEIiLCJmaWxlIjoiQzpcXFVzZXJzXFxYaWFvbWlcXFdlYnN0b3JtUHJvamVjdHNcXFJlYWN0LWFwcHNcXHRlc3QtdGFzay10ZXN0XFxteS1hcHBcXHBhZ2VzXFxzcmNcXGNvbXBvbmVudHNcXEZvcm1Db21wb25lbnRcXEZvcm1Db21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHtDb21wb25lbnR9IGZyb20gJ3JlYWN0JztcclxuXHJcbmltcG9ydCB7Y29ubmVjdH0gZnJvbSAncmVhY3QtcmVkdXgnO1xyXG5cclxuaW1wb3J0IHtcclxuICAgIGdldEZvcm1EYXRhLFxyXG4gICAgc2VuZEZvcm1EYXRhLFxyXG4gICAgc2V0SW5wdXRWYWx1ZSxcclxuICAgIHNldFNlbmROb3RpZmljYXRpb25TdGF0dXNcclxufSBmcm9tIFwiLi4vLi4vYWN0aW9ucy9Gb3JtQWN0aW9ucy9mb3JtQWN0aW9uc1wiO1xyXG5cclxuaW1wb3J0IElucHV0IGZyb20gJ2FydWktZmVhdGhlci9pbnB1dCc7XHJcbmltcG9ydCBCdXR0b24gZnJvbSAnYXJ1aS1mZWF0aGVyL2J1dHRvbic7XHJcbmltcG9ydCBGb3JtIGZyb20gXCJhcnVpLWZlYXRoZXIvZm9ybVwiO1xyXG5pbXBvcnQgU2VsZWN0IGZyb20gXCJhcnVpLWZlYXRoZXIvc2VsZWN0XCI7XHJcblxyXG5pbXBvcnQgU3BpbiBmcm9tIFwiYXJ1aS1mZWF0aGVyL3NwaW5cIjtcclxuaW1wb3J0IE5vdGlmaWNhdGlvbiBmcm9tIFwiYXJ1aS1mZWF0aGVyL25vdGlmaWNhdGlvblwiO1xyXG5cclxuY2xhc3MgRm9ybUNvbXBvbmVudCBleHRlbmRzIENvbXBvbmVudCB7XHJcbiAgICBzdGF0ZSA9IHtcclxuICAgICAgICBmaW5hbEZvcm06IHt9XHJcbiAgICB9O1xyXG5cclxuICAgIGhhbmRsZUNoYW5nZSA9IChlLCBuYW1lKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgICAgIGZpbmFsRm9ybTogey4uLnRoaXMuc3RhdGUuZmluYWxGb3JtLCBbbmFtZV06IGV9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyhuYW1lKTtcclxuICAgICAgICAvL3NldElucHV0VmFsdWUoe3ZhbHVlOiBlLCBuYW1lfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIG9uU3VibWl0ID0gKGUpID0+IHtcclxuXHJcbiAgICAgICAgLy9jb25zb2xlLmxvZygnaGVsbG8sIG15IGZyaWVuZCcpO1xyXG4gICAgICAgIHRoaXMucHJvcHMuc2VuZEZvcm1EYXRhKHRoaXMuc3RhdGUuZmluYWxGb3JtKTtcclxuICAgIH07XHJcblxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcy5nZXRGb3JtRGF0YSgpXHJcbiAgICB9O1xyXG5cclxuICAgIHNldElucHV0VHlwZSA9ICh0eXBlLCB2YWx1ZXMsIG5hbWUpID0+IHtcclxuXHJcbiAgICAgICAgc3dpdGNoICh0eXBlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ1RFWFQnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIDxJbnB1dCB0eXBlPSd0ZXh0JyBuYW1lPXtuYW1lfSBvbkNoYW5nZT17ZSA9PiB0aGlzLmhhbmRsZUNoYW5nZShlLCBuYW1lKX0vPjtcclxuICAgICAgICAgICAgY2FzZSAnTlVNRVJJQyc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gPElucHV0IHR5cGU9J251bWJlcicgbmFtZT17bmFtZX0gb25DaGFuZ2U9e2UgPT4gdGhpcy5oYW5kbGVDaGFuZ2UoZSwgbmFtZSl9Lz47XHJcbiAgICAgICAgICAgIGNhc2UgJ0xJU1QnOlxyXG4gICAgICAgICAgICAgICAgY29uc3Qgb3B0aW9ucyA9IFtcclxuICAgICAgICAgICAgICAgICAgICB7dmFsdWU6IHZhbHVlcy5ub25lLCB0ZXh0OiB2YWx1ZXMubm9uZX0sXHJcbiAgICAgICAgICAgICAgICAgICAge3ZhbHVlOiB2YWx1ZXMudjEsIHRleHQ6IHZhbHVlcy52MX0sXHJcbiAgICAgICAgICAgICAgICAgICAge3ZhbHVlOiB2YWx1ZXMudjIsIHRleHQ6IHZhbHVlcy52Mn0sXHJcbiAgICAgICAgICAgICAgICAgICAge3ZhbHVlOiB2YWx1ZXMudjMsIHRleHQ6IHZhbHVlcy52M31cclxuICAgICAgICAgICAgICAgIF07XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gPFNlbGVjdCBuYW1lPXtuYW1lfSBtb2RlPVwicmFkaW8tY2hlY2tcIiBvcHRpb25zPXtvcHRpb25zfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e2UgPT4gdGhpcy5oYW5kbGVDaGFuZ2UoZVswXSwgbmFtZSl9Lz5cclxuXHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgY29uc3Qge2Zvcm0sIGlzRm9ybVNlbmRpbmcsIGlzU2VudE5vdGlmaWNhdGlvbiwgc2V0U2VuZE5vdGlmaWNhdGlvblN0YXR1c30gPSB0aGlzLnByb3BzO1xyXG4gICAgICAgIC8vY29uc29sZS5sb2codGhpcy5wcm9wcyk7XHJcbiAgICAgICAgcmV0dXJuIGZvcm0gPyAoXHJcbiAgICAgICAgICAgIDxGb3JtIG9uU3VibWl0PXt0aGlzLm9uU3VibWl0fSBjbGFzc05hbWU9XCJBcHBcIj5cclxuICAgICAgICAgICAgICAgIDxoMj57Zm9ybS50aXRsZX08L2gyPlxyXG4gICAgICAgICAgICAgICAgPGltZyBzcmM9e2Zvcm0uaW1hZ2V9IGFsdD0naW1hZ2UnLz5cclxuICAgICAgICAgICAgICAgIDx0YWJsZSBib3JkZXI9JzEnPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0Ym9keT5cclxuICAgICAgICAgICAgICAgICAgICB7Zm9ybS5maWVsZHMubWFwKChmaWVsZCwgaW5kZXgpID0+IChcclxuICAgICAgICAgICAgICAgICAgICAgICAgPHRyIGtleT17aW5kZXh9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoNT57ZmllbGQudGl0bGV9PC9oNT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e3RoaXMuc2V0SW5wdXRUeXBlKGZpZWxkLnR5cGUsIGZpZWxkLnZhbHVlcywgZmllbGQubmFtZSl9PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC90cj4pKX1cclxuICAgICAgICAgICAgICAgICAgICA8L3Rib2R5PlxyXG4gICAgICAgICAgICAgICAgPC90YWJsZT5cclxuICAgICAgICAgICAgICAgIDxCdXR0b24gdmlldz0nZXh0cmEnIHR5cGU9J3N1Ym1pdCcgaWNvbj17PFNwaW4gdmlzaWJsZT17aXNGb3JtU2VuZGluZ30vPn0+U2VuZDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPE5vdGlmaWNhdGlvblxyXG4gICAgICAgICAgICAgICAgICAgIHZpc2libGU9e2lzU2VudE5vdGlmaWNhdGlvbn1cclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM9J29rJ1xyXG4gICAgICAgICAgICAgICAgICAgIG9mZnNldD17MTJ9XHJcbiAgICAgICAgICAgICAgICAgICAgc3RpY2tUbz0nbGVmdCdcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZT0nU3VjY2VzcydcclxuICAgICAgICAgICAgICAgICAgICBvbkNsb3NlVGltZW91dD17KCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRTZW5kTm90aWZpY2F0aW9uU3RhdHVzKGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xvc2VyQ2xpY2s9eygpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2V0U2VuZE5vdGlmaWNhdGlvblN0YXR1cyhmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICBGb3JtIGhhcyBiZWVuIHNlbnQuXHJcbiAgICAgICAgICAgICAgICA8L05vdGlmaWNhdGlvbj5cclxuICAgICAgICAgICAgICAgIDxzdHlsZSBqc3g+e2BcclxuICAgICAgICAgICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogNTAwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMzUwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMjBweCAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYH08L3N0eWxlPlxyXG4gICAgICAgICAgICA8L0Zvcm0+XHJcbiAgICAgICAgKSA6ICg8ZGl2PkxvYWRpbmc8L2Rpdj4pXHJcbiAgICB9O1xyXG59XHJcblxyXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoc3RhdGUpID0+ICh7XHJcbiAgICAvL2NvbnNvbGUubG9nKHN0YXRlKVxyXG4gICAgaXNGb3JtU2VuZGluZzogc3RhdGUuZm9ybVJlZHVjZXJzLmlzRm9ybVNlbmRpbmcsXHJcbiAgICBpc1NlbnROb3RpZmljYXRpb246IHN0YXRlLmZvcm1SZWR1Y2Vycy5pc1NlbnROb3RpZmljYXRpb24sXHJcbiAgICBmb3JtOiBzdGF0ZS5mb3JtUmVkdWNlcnMuZm9ybVxyXG59KTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QoXHJcbiAgICBtYXBTdGF0ZVRvUHJvcHMsXHJcbiAgICB7XHJcbiAgICAgICAgZ2V0Rm9ybURhdGEsXHJcbiAgICAgICAgc2VuZEZvcm1EYXRhLFxyXG4gICAgICAgIHNldElucHV0VmFsdWUsXHJcbiAgICAgICAgc2V0U2VuZE5vdGlmaWNhdGlvblN0YXR1c1xyXG4gICAgfVxyXG4pKEZvcm1Db21wb25lbnQpOyJdfQ== */\n/*@ sourceURL=C:\\Users\\Xiaomi\\WebstormProjects\\React-apps\\test-task-test\\my-app\\pages\\src\\components\\FormComponent\\FormComponent.js */")) : __jsx("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 110
      },
      __self: this
    }, "Loading");
  }

}

const mapStateToProps = state => ({
  //console.log(state)
  isFormSending: state.formReducers.isFormSending,
  isSentNotification: state.formReducers.isSentNotification,
  form: state.formReducers.form
});

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_9__["connect"])(mapStateToProps, {
  getFormData: _actions_FormActions_formActions__WEBPACK_IMPORTED_MODULE_10__["getFormData"],
  sendFormData: _actions_FormActions_formActions__WEBPACK_IMPORTED_MODULE_10__["sendFormData"],
  setInputValue: _actions_FormActions_formActions__WEBPACK_IMPORTED_MODULE_10__["setInputValue"],
  setSendNotificationStatus: _actions_FormActions_formActions__WEBPACK_IMPORTED_MODULE_10__["setSendNotificationStatus"]
})(FormComponent));

/***/ }),

/***/ "./pages/src/reducers/formReducers.js":
/*!********************************************!*\
  !*** ./pages/src/reducers/formReducers.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "@babel/runtime-corejs2/core-js/object/define-property");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-properties */ "@babel/runtime-corejs2/core-js/object/define-properties");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/get-own-property-descriptors */ "@babel/runtime-corejs2/core-js/object/get-own-property-descriptors");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/get-own-property-descriptor */ "@babel/runtime-corejs2/core-js/object/get-own-property-descriptor");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/get-own-property-symbols */ "@babel/runtime-corejs2/core-js/object/get-own-property-symbols");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/keys */ "@babel/runtime-corejs2/core-js/object/keys");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "@babel/runtime-corejs2/helpers/esm/defineProperty");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _actions_FormActions_formTypes__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../actions/FormActions/formTypes */ "./pages/src/actions/FormActions/formTypes.js");








function ownKeys(object, enumerableOnly) { var keys = _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5___default()(object); if (_babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4___default.a) { var symbols = _babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4___default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3___default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6___default()(target, key, source[key]); }); } else if (_babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2___default.a) { _babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1___default()(target, _babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2___default()(source)); } else { ownKeys(source).forEach(function (key) { _babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(target, key, _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3___default()(source, key)); }); } } return target; }


const initialState = {
  form: null,
  isFormSending: false,
  isSentNotification: false
};
/* harmony default export */ __webpack_exports__["default"] = (function (state = initialState, action) {
  const {
    type,
    payload
  } = action;

  switch (type) {
    case _actions_FormActions_formTypes__WEBPACK_IMPORTED_MODULE_7__["GET_FORM_DATA"]:
      return _objectSpread({}, state, {
        form: payload
      });

    case _actions_FormActions_formTypes__WEBPACK_IMPORTED_MODULE_7__["SET_INPUT_VALUE"]:
      return _objectSpread({}, state, {
        form: _objectSpread({}, state.form, {
          [payload.name]: payload.value
        })
      });

    case _actions_FormActions_formTypes__WEBPACK_IMPORTED_MODULE_7__["SET_SENDING_STATUS"]:
      return _objectSpread({}, state, {
        isFormSending: payload
      });

    case _actions_FormActions_formTypes__WEBPACK_IMPORTED_MODULE_7__["SET_SEND_NOTIFICATION_STATUS"]:
      return _objectSpread({}, state, {
        isSentNotification: payload
      });

    default:
      return state;
  }
});

/***/ }),

/***/ "./pages/src/reducers/index.js":
/*!*************************************!*\
  !*** ./pages/src/reducers/index.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _formReducers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./formReducers */ "./pages/src/reducers/formReducers.js");


/* harmony default export */ __webpack_exports__["default"] = (Object(redux__WEBPACK_IMPORTED_MODULE_0__["combineReducers"])({
  formReducers: _formReducers__WEBPACK_IMPORTED_MODULE_1__["default"]
}));

/***/ }),

/***/ 3:
/*!******************************!*\
  !*** multi ./pages/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Xiaomi\WebstormProjects\React-apps\test-task-test\my-app\pages\index.js */"./pages/index.js");


/***/ }),

/***/ "@babel/runtime-corejs2/core-js/object/define-properties":
/*!**************************************************************************!*\
  !*** external "@babel/runtime-corejs2/core-js/object/define-properties" ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime-corejs2/core-js/object/define-properties");

/***/ }),

/***/ "@babel/runtime-corejs2/core-js/object/define-property":
/*!************************************************************************!*\
  !*** external "@babel/runtime-corejs2/core-js/object/define-property" ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime-corejs2/core-js/object/define-property");

/***/ }),

/***/ "@babel/runtime-corejs2/core-js/object/get-own-property-descriptor":
/*!************************************************************************************!*\
  !*** external "@babel/runtime-corejs2/core-js/object/get-own-property-descriptor" ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime-corejs2/core-js/object/get-own-property-descriptor");

/***/ }),

/***/ "@babel/runtime-corejs2/core-js/object/get-own-property-descriptors":
/*!*************************************************************************************!*\
  !*** external "@babel/runtime-corejs2/core-js/object/get-own-property-descriptors" ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime-corejs2/core-js/object/get-own-property-descriptors");

/***/ }),

/***/ "@babel/runtime-corejs2/core-js/object/get-own-property-symbols":
/*!*********************************************************************************!*\
  !*** external "@babel/runtime-corejs2/core-js/object/get-own-property-symbols" ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime-corejs2/core-js/object/get-own-property-symbols");

/***/ }),

/***/ "@babel/runtime-corejs2/core-js/object/keys":
/*!*************************************************************!*\
  !*** external "@babel/runtime-corejs2/core-js/object/keys" ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime-corejs2/core-js/object/keys");

/***/ }),

/***/ "@babel/runtime-corejs2/helpers/esm/defineProperty":
/*!********************************************************************!*\
  !*** external "@babel/runtime-corejs2/helpers/esm/defineProperty" ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime-corejs2/helpers/esm/defineProperty");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "bezier-easing":
/*!********************************!*\
  !*** external "bezier-easing" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("bezier-easing");

/***/ }),

/***/ "cn-decorator":
/*!*******************************!*\
  !*** external "cn-decorator" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("cn-decorator");

/***/ }),

/***/ "core-decorators/lib/autobind":
/*!***********************************************!*\
  !*** external "core-decorators/lib/autobind" ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-decorators/lib/autobind");

/***/ }),

/***/ "deprecated-decorator":
/*!***************************************!*\
  !*** external "deprecated-decorator" ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("deprecated-decorator");

/***/ }),

/***/ "inputmask-core":
/*!*********************************!*\
  !*** external "inputmask-core" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("inputmask-core");

/***/ }),

/***/ "lodash.debounce":
/*!**********************************!*\
  !*** external "lodash.debounce" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("lodash.debounce");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-addons-create-fragment":
/*!***********************************************!*\
  !*** external "react-addons-create-fragment" ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-addons-create-fragment");

/***/ }),

/***/ "react-dom":
/*!****************************!*\
  !*** external "react-dom" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-dom");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "react-style-proptype":
/*!***************************************!*\
  !*** external "react-style-proptype" ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-style-proptype");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "redux-thunk":
/*!******************************!*\
  !*** external "redux-thunk" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-thunk");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map